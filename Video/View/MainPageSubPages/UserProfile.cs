﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.Dao;
using Video.Model;

namespace Video.View.MainPageSubPages
{
    public partial class UserProfile : Form
    {
        private UserBean bean;
        public UserProfile(Model.UserBean bean)
        {
            this.bean = bean;
            InitializeComponent();

            usernameLabel.Text = bean.username;
            usersEmailLabel.Text = bean.email;
            usersBirthDate.Text = bean.birthdate;
        }

        private void updateEmailButton_Click(object sender, EventArgs e)
        {
            if (newEmailTextBox.Text.Equals(String.Empty)) {
                MessageBox.Show("Üres az email mező");
            }
            else {
                Database dao = new Database();
                dao.updateUserEmail(bean, newEmailTextBox.Text);
                usersEmailLabel.Text = bean.email;
            }
            
        }

        private void updatePwButton_Click(object sender, EventArgs e)
        {
            if (oldPasswordTextBox.Text.Equals(String.Empty))
            {
                MessageBox.Show("A jelszó változtatásához meg kell adnod a régi jelszavad!");
            }
            else if (newPasswordTextField.Text.Equals(String.Empty) || renewPasswordTextField.Text.Equals(String.Empty))
            {
                MessageBox.Show("Kétszer kell megadnod az új jelszavadat!");
            }
            else if (newPasswordTextField.Text != renewPasswordTextField.Text)
            {
                MessageBox.Show("A két jelszó nem egyezik");
            }
            else {
                Database pw = new Database();
                string newPassword = newPasswordTextField.Text;
                pw.updateUserPassword(bean, newPassword);
            }
        }
    }
}
