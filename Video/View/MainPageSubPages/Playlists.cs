﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.Dao;
using Video.Model;
using Video.View.MainPageSubPages.Components;

namespace Video.View.MainPageSubPages
{
    public partial class Playlists : Form
    {
        const int SPACER = 5;
        MainPage mainPage;
        List<PlaylistBean> list;
        UserBean user;

        public Playlists(MainPage mainPage, UserBean user)
        {
            this.user = user;
            this.mainPage = mainPage;
            InitializeComponent();
            Database listazz = new Database();
            list = listazz.listPlaylists(user); 
            listPlaylists();
        }
        

        public void listPlaylists()
        {

            int previousVerticalPosition = 0;
            int height = 0;
            foreach (PlaylistBean beans in list)
            {
                PlayListNameForm a = new PlayListNameForm(mainPage, beans, user);
                a.Location = new Point(0, height + previousVerticalPosition + SPACER);
                a.playlist_name.Text = beans.playlistName;
                previousVerticalPosition = a.Location.Y;
                height = a.Height;

                playlists_panel.Controls.Add(a);
            }
        }

        private void playlist_add_button(object sender, EventArgs e)
        {
            if (!add_new_playlist_textbox.Text.Equals(String.Empty))
            {
                PlaylistBean playlistToAdd = new PlaylistBean()
                {
                    playlistName = add_new_playlist_textbox.Text,
                    userId = user.userid,
                };
                Database databaseAccess = new Database();
                databaseAccess.insertPlaylist(playlistToAdd);
                refreshplaylists_panel();
            }
        }

        public void refreshplaylists_panel() {
            Database databaseAccess = new Database();
            list = databaseAccess.listPlaylists(user);
            listPlaylists();
        }

        private void closeFormsExceptThis(string formName)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != formName)
                {
                    Application.OpenForms[i].Close();
                }
            }
        }

        private void MainPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            closeFormsExceptThis("MainPage");
        }

    }
}
