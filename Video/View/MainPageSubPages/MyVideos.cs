﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.Dao;
using Video.Model;
using Video.View.MainPageSubPages.Components;

namespace Video.View.MainPageSubPages
{
    public partial class MyVideos : Form
    {
        const int SPACER = 0;
        List<VideoBean> list;
        UserBean user;
        MainPage main;
        public MyVideos(MainPage main, UserBean user)
        {
            this.user = user;
            this.main = main;
            InitializeComponent();
            Database listazz = new Database();
            list = listazz.getListOfVideosFromVideoTableUploadedByThisUser(user);
            listVideos();
        }

        private void listVideos()
        {
            int previousVerticalPosition = 0;
            int height = 0;
            foreach (VideoBean beans in list)
            {
                SingleVideoForm a = new SingleVideoForm(this, user, beans, main);
                a.Location = new Point(0, height + previousVerticalPosition + SPACER);
                if (this.user == a.user) {
                    a.editButton.Visible = true;
                    a.deleteButton.Visible = true;
                }
                previousVerticalPosition = a.Location.Y;
                height = a.Height;
                main_panel.Controls.Add(a);
            }
        }
    }
}
