﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Video.View.MainPageSubPages
{
    partial class HomePage
    {
        
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.latestVideoPanel3 = new System.Windows.Forms.Panel();
            this.latestVideoPanel2 = new System.Windows.Forms.Panel();
            this.latestVideoPanel1 = new System.Windows.Forms.Panel();
            this.latestVideosLabel = new System.Windows.Forms.Label();
            this.homePagePanel = new System.Windows.Forms.Panel();
            this.homepage_panel = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.mostViewedByCategoryStaticLabel = new System.Windows.Forms.Label();
            this.day = new System.Windows.Forms.Label();
            this.Week = new System.Windows.Forms.Label();
            this.month = new System.Windows.Forms.Label();
            this.most_viewed = new System.Windows.Forms.Label();
            this.most_uploaded_videos = new System.Windows.Forms.Label();
            this.most_comment_users = new System.Windows.Forms.Label();
            this.homePagePanel.SuspendLayout();
            this.homepage_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // latestVideoPanel3
            // 
            this.latestVideoPanel3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.latestVideoPanel3.Location = new System.Drawing.Point(790, -165);
            this.latestVideoPanel3.Name = "latestVideoPanel3";
            this.latestVideoPanel3.Size = new System.Drawing.Size(256, 144);
            this.latestVideoPanel3.TabIndex = 24;
            // 
            // latestVideoPanel2
            // 
            this.latestVideoPanel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.latestVideoPanel2.Location = new System.Drawing.Point(393, -165);
            this.latestVideoPanel2.Name = "latestVideoPanel2";
            this.latestVideoPanel2.Size = new System.Drawing.Size(256, 144);
            this.latestVideoPanel2.TabIndex = 24;
            // 
            // latestVideoPanel1
            // 
            this.latestVideoPanel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.latestVideoPanel1.Location = new System.Drawing.Point(28, -165);
            this.latestVideoPanel1.Name = "latestVideoPanel1";
            this.latestVideoPanel1.Size = new System.Drawing.Size(256, 144);
            this.latestVideoPanel1.TabIndex = 24;
            // 
            // latestVideosLabel
            // 
            this.latestVideosLabel.AutoSize = true;
            this.latestVideosLabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.latestVideosLabel.Location = new System.Drawing.Point(22, -200);
            this.latestVideosLabel.Name = "latestVideosLabel";
            this.latestVideosLabel.Size = new System.Drawing.Size(179, 32);
            this.latestVideosLabel.TabIndex = 15;
            this.latestVideosLabel.Text = "LatestVideos";
            // 
            // homePagePanel
            // 
            this.homePagePanel.AutoSize = true;
            this.homePagePanel.Controls.Add(this.latestVideoPanel3);
            this.homePagePanel.Controls.Add(this.latestVideoPanel2);
            this.homePagePanel.Controls.Add(this.latestVideoPanel1);
            this.homePagePanel.Controls.Add(this.latestVideosLabel);
            this.homePagePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.homePagePanel.Location = new System.Drawing.Point(0, 0);
            this.homePagePanel.Name = "homePagePanel";
            this.homePagePanel.Size = new System.Drawing.Size(1240, 0);
            this.homePagePanel.TabIndex = 3;
            // 
            // homepage_panel
            // 
            this.homepage_panel.AutoScroll = true;
            this.homepage_panel.AutoSize = true;
            this.homepage_panel.Controls.Add(this.label5);
            this.homepage_panel.Controls.Add(this.label4);
            this.homepage_panel.Controls.Add(this.label3);
            this.homepage_panel.Controls.Add(this.label2);
            this.homepage_panel.Controls.Add(this.label1);
            this.homepage_panel.Controls.Add(this.mostViewedByCategoryStaticLabel);
            this.homepage_panel.Controls.Add(this.day);
            this.homepage_panel.Controls.Add(this.Week);
            this.homepage_panel.Controls.Add(this.month);
            this.homepage_panel.Controls.Add(this.most_viewed);
            this.homepage_panel.Controls.Add(this.most_uploaded_videos);
            this.homepage_panel.Controls.Add(this.most_comment_users);
            this.homepage_panel.Location = new System.Drawing.Point(0, 0);
            this.homepage_panel.Name = "homepage_panel";
            this.homepage_panel.Size = new System.Drawing.Size(1240, 7820);
            this.homepage_panel.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(3, 7325);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(417, 26);
            this.label5.TabIndex = 81;
            this.label5.Text = "Recommended Videos by Video Tags:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(3, 7164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(248, 26);
            this.label4.TabIndex = 80;
            this.label4.Text = "Recommended Users:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(3, 2067);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(259, 26);
            this.label3.TabIndex = 79;
            this.label3.Text = "Recommended Videos:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(3, 6795);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(262, 26);
            this.label2.TabIndex = 78;
            this.label2.Text = "Similar Interests Users:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 1823);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(325, 26);
            this.label1.TabIndex = 77;
            this.label1.Text = "Newest Videos by Categories";
            // 
            // mostViewedByCategoryStaticLabel
            // 
            this.mostViewedByCategoryStaticLabel.AutoSize = true;
            this.mostViewedByCategoryStaticLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.mostViewedByCategoryStaticLabel.Location = new System.Drawing.Point(3, 1013);
            this.mostViewedByCategoryStaticLabel.Name = "mostViewedByCategoryStaticLabel";
            this.mostViewedByCategoryStaticLabel.Size = new System.Drawing.Size(520, 26);
            this.mostViewedByCategoryStaticLabel.TabIndex = 76;
            this.mostViewedByCategoryStaticLabel.Text = "Most Viewed Videos In Most Viewed Categories";
            // 
            // day
            // 
            this.day.AutoSize = true;
            this.day.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.day.Location = new System.Drawing.Point(19, 813);
            this.day.Name = "day";
            this.day.Size = new System.Drawing.Size(61, 26);
            this.day.TabIndex = 72;
            this.day.Text = "Day:";
            // 
            // Week
            // 
            this.Week.AutoSize = true;
            this.Week.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Week.Location = new System.Drawing.Point(19, 594);
            this.Week.Name = "Week";
            this.Week.Size = new System.Drawing.Size(79, 26);
            this.Week.TabIndex = 69;
            this.Week.Text = "Week:";
            // 
            // month
            // 
            this.month.AutoSize = true;
            this.month.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.month.Location = new System.Drawing.Point(19, 389);
            this.month.Name = "month";
            this.month.Size = new System.Drawing.Size(84, 26);
            this.month.TabIndex = 68;
            this.month.Text = "Month:";
            // 
            // most_viewed
            // 
            this.most_viewed.AutoSize = true;
            this.most_viewed.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.most_viewed.Location = new System.Drawing.Point(6, 353);
            this.most_viewed.Name = "most_viewed";
            this.most_viewed.Size = new System.Drawing.Size(155, 26);
            this.most_viewed.TabIndex = 66;
            this.most_viewed.Text = "Most Viewed:";
            // 
            // most_uploaded_videos
            // 
            this.most_uploaded_videos.AutoSize = true;
            this.most_uploaded_videos.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.most_uploaded_videos.Location = new System.Drawing.Point(6, 176);
            this.most_uploaded_videos.Name = "most_uploaded_videos";
            this.most_uploaded_videos.Size = new System.Drawing.Size(246, 26);
            this.most_uploaded_videos.TabIndex = 59;
            this.most_uploaded_videos.Text = "Most Uploaded Video:";
            // 
            // most_comment_users
            // 
            this.most_comment_users.AutoSize = true;
            this.most_comment_users.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.most_comment_users.Location = new System.Drawing.Point(6, 6);
            this.most_comment_users.Name = "most_comment_users";
            this.most_comment_users.Size = new System.Drawing.Size(192, 26);
            this.most_comment_users.TabIndex = 58;
            this.most_comment_users.Text = "Most Comments:";
            // 
            // HomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1224, 1045);
            this.Controls.Add(this.homepage_panel);
            this.Controls.Add(this.homePagePanel);
            this.Name = "HomePage";
            this.Text = "HomePage1";
            this.homePagePanel.ResumeLayout(false);
            this.homePagePanel.PerformLayout();
            this.homepage_panel.ResumeLayout(false);
            this.homepage_panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel latestVideoPanel3;
        private System.Windows.Forms.Panel latestVideoPanel2;
        private System.Windows.Forms.Panel latestVideoPanel1;
        private System.Windows.Forms.Label latestVideosLabel;
        private System.Windows.Forms.Panel homePagePanel;
        private System.Windows.Forms.Panel homepage_panel;
        private System.Windows.Forms.Label Week;
        private System.Windows.Forms.Label month;
        private System.Windows.Forms.Label most_viewed;
        private System.Windows.Forms.Label most_uploaded_videos;
        private System.Windows.Forms.Label most_comment_users;
        private System.Windows.Forms.Label day;
        private System.Windows.Forms.Label mostViewedByCategoryStaticLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}