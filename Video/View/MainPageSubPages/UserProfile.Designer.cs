﻿namespace Video.View.MainPageSubPages
{
    partial class UserProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateEmailButton = new System.Windows.Forms.Button();
            this.newEmailTextBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.usersBirthDate = new System.Windows.Forms.Label();
            this.birthDateJustTextLabel = new System.Windows.Forms.Label();
            this.usersEmailLabel = new System.Windows.Forms.Label();
            this.emailJustTextLabel = new System.Windows.Forms.Label();
            this.changeEmailLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.changePasswordStaticLabel = new System.Windows.Forms.Label();
            this.oldPasswordTextBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.updatePwButton = new System.Windows.Forms.Button();
            this.newPasswordTextField = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.renewPasswordTextField = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.SuspendLayout();
            // 
            // updateEmailButton
            // 
            this.updateEmailButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateEmailButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateEmailButton.Location = new System.Drawing.Point(316, 319);
            this.updateEmailButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.updateEmailButton.Name = "updateEmailButton";
            this.updateEmailButton.Size = new System.Drawing.Size(95, 31);
            this.updateEmailButton.TabIndex = 1;
            this.updateEmailButton.Text = "Update";
            this.updateEmailButton.UseVisualStyleBackColor = true;
            this.updateEmailButton.Click += new System.EventHandler(this.updateEmailButton_Click);
            // 
            // newEmailTextBox
            // 
            this.newEmailTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.newEmailTextBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.newEmailTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.newEmailTextBox.HintForeColor = System.Drawing.Color.Empty;
            this.newEmailTextBox.HintText = "YourNewEmail";
            this.newEmailTextBox.isPassword = false;
            this.newEmailTextBox.LineFocusedColor = System.Drawing.Color.Black;
            this.newEmailTextBox.LineIdleColor = System.Drawing.Color.Gray;
            this.newEmailTextBox.LineMouseHoverColor = System.Drawing.Color.Black;
            this.newEmailTextBox.LineThickness = 3;
            this.newEmailTextBox.Location = new System.Drawing.Point(55, 309);
            this.newEmailTextBox.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.newEmailTextBox.Name = "newEmailTextBox";
            this.newEmailTextBox.Size = new System.Drawing.Size(252, 41);
            this.newEmailTextBox.TabIndex = 0;
            this.newEmailTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // usersBirthDate
            // 
            this.usersBirthDate.AutoSize = true;
            this.usersBirthDate.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usersBirthDate.Location = new System.Drawing.Point(263, 165);
            this.usersBirthDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.usersBirthDate.Name = "usersBirthDate";
            this.usersBirthDate.Size = new System.Drawing.Size(149, 33);
            this.usersBirthDate.TabIndex = 3;
            this.usersBirthDate.Text = "1996.12.27";
            // 
            // birthDateJustTextLabel
            // 
            this.birthDateJustTextLabel.AutoSize = true;
            this.birthDateJustTextLabel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birthDateJustTextLabel.Location = new System.Drawing.Point(49, 165);
            this.birthDateJustTextLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.birthDateJustTextLabel.Name = "birthDateJustTextLabel";
            this.birthDateJustTextLabel.Size = new System.Drawing.Size(143, 33);
            this.birthDateJustTextLabel.TabIndex = 4;
            this.birthDateJustTextLabel.Text = "Birthdate:";
            // 
            // usersEmailLabel
            // 
            this.usersEmailLabel.AutoSize = true;
            this.usersEmailLabel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usersEmailLabel.Location = new System.Drawing.Point(263, 110);
            this.usersEmailLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.usersEmailLabel.Name = "usersEmailLabel";
            this.usersEmailLabel.Size = new System.Drawing.Size(315, 33);
            this.usersEmailLabel.TabIndex = 5;
            this.usersEmailLabel.Text = "marcelloo@gmail.com";
            // 
            // emailJustTextLabel
            // 
            this.emailJustTextLabel.AutoSize = true;
            this.emailJustTextLabel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailJustTextLabel.Location = new System.Drawing.Point(49, 110);
            this.emailJustTextLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.emailJustTextLabel.Name = "emailJustTextLabel";
            this.emailJustTextLabel.Size = new System.Drawing.Size(92, 33);
            this.emailJustTextLabel.TabIndex = 6;
            this.emailJustTextLabel.Text = "Email:";
            // 
            // changeEmailLabel
            // 
            this.changeEmailLabel.AutoSize = true;
            this.changeEmailLabel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changeEmailLabel.Location = new System.Drawing.Point(48, 239);
            this.changeEmailLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.changeEmailLabel.Name = "changeEmailLabel";
            this.changeEmailLabel.Size = new System.Drawing.Size(209, 32);
            this.changeEmailLabel.TabIndex = 7;
            this.changeEmailLabel.Text = "Change email:";
            // 
            // usernameLabel
            // 
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameLabel.Location = new System.Drawing.Point(40, 12);
            this.usernameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(183, 40);
            this.usernameLabel.TabIndex = 8;
            this.usernameLabel.Text = "Username";
            // 
            // changePasswordStaticLabel
            // 
            this.changePasswordStaticLabel.AutoSize = true;
            this.changePasswordStaticLabel.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.changePasswordStaticLabel.Location = new System.Drawing.Point(49, 425);
            this.changePasswordStaticLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.changePasswordStaticLabel.Name = "changePasswordStaticLabel";
            this.changePasswordStaticLabel.Size = new System.Drawing.Size(263, 32);
            this.changePasswordStaticLabel.TabIndex = 7;
            this.changePasswordStaticLabel.Text = "Change password:";
            // 
            // oldPasswordTextBox
            // 
            this.oldPasswordTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.oldPasswordTextBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.oldPasswordTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.oldPasswordTextBox.HintForeColor = System.Drawing.Color.Empty;
            this.oldPasswordTextBox.HintText = "Your old password";
            this.oldPasswordTextBox.isPassword = false;
            this.oldPasswordTextBox.LineFocusedColor = System.Drawing.Color.Black;
            this.oldPasswordTextBox.LineIdleColor = System.Drawing.Color.Gray;
            this.oldPasswordTextBox.LineMouseHoverColor = System.Drawing.Color.Black;
            this.oldPasswordTextBox.LineThickness = 3;
            this.oldPasswordTextBox.Location = new System.Drawing.Point(54, 490);
            this.oldPasswordTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.oldPasswordTextBox.Name = "oldPasswordTextBox";
            this.oldPasswordTextBox.Size = new System.Drawing.Size(252, 41);
            this.oldPasswordTextBox.TabIndex = 2;
            this.oldPasswordTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // updatePwButton
            // 
            this.updatePwButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updatePwButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updatePwButton.Location = new System.Drawing.Point(316, 699);
            this.updatePwButton.Margin = new System.Windows.Forms.Padding(4);
            this.updatePwButton.Name = "updatePwButton";
            this.updatePwButton.Size = new System.Drawing.Size(95, 31);
            this.updatePwButton.TabIndex = 5;
            this.updatePwButton.Text = "Update";
            this.updatePwButton.UseVisualStyleBackColor = true;
            this.updatePwButton.Click += new System.EventHandler(this.updatePwButton_Click);
            // 
            // newPasswordTextField
            // 
            this.newPasswordTextField.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.newPasswordTextField.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.newPasswordTextField.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.newPasswordTextField.HintForeColor = System.Drawing.Color.Empty;
            this.newPasswordTextField.HintText = "Your new password";
            this.newPasswordTextField.isPassword = false;
            this.newPasswordTextField.LineFocusedColor = System.Drawing.Color.Black;
            this.newPasswordTextField.LineIdleColor = System.Drawing.Color.Gray;
            this.newPasswordTextField.LineMouseHoverColor = System.Drawing.Color.Black;
            this.newPasswordTextField.LineThickness = 3;
            this.newPasswordTextField.Location = new System.Drawing.Point(54, 591);
            this.newPasswordTextField.Margin = new System.Windows.Forms.Padding(5);
            this.newPasswordTextField.Name = "newPasswordTextField";
            this.newPasswordTextField.Size = new System.Drawing.Size(252, 41);
            this.newPasswordTextField.TabIndex = 3;
            this.newPasswordTextField.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // renewPasswordTextField
            // 
            this.renewPasswordTextField.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.renewPasswordTextField.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.renewPasswordTextField.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.renewPasswordTextField.HintForeColor = System.Drawing.Color.Empty;
            this.renewPasswordTextField.HintText = "Re-new password";
            this.renewPasswordTextField.isPassword = false;
            this.renewPasswordTextField.LineFocusedColor = System.Drawing.Color.Black;
            this.renewPasswordTextField.LineIdleColor = System.Drawing.Color.Gray;
            this.renewPasswordTextField.LineMouseHoverColor = System.Drawing.Color.Black;
            this.renewPasswordTextField.LineThickness = 3;
            this.renewPasswordTextField.Location = new System.Drawing.Point(54, 689);
            this.renewPasswordTextField.Margin = new System.Windows.Forms.Padding(5);
            this.renewPasswordTextField.Name = "renewPasswordTextField";
            this.renewPasswordTextField.Size = new System.Drawing.Size(252, 41);
            this.renewPasswordTextField.TabIndex = 4;
            this.renewPasswordTextField.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // UserProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1632, 986);
            this.Controls.Add(this.updatePwButton);
            this.Controls.Add(this.updateEmailButton);
            this.Controls.Add(this.renewPasswordTextField);
            this.Controls.Add(this.newPasswordTextField);
            this.Controls.Add(this.oldPasswordTextBox);
            this.Controls.Add(this.newEmailTextBox);
            this.Controls.Add(this.usersBirthDate);
            this.Controls.Add(this.birthDateJustTextLabel);
            this.Controls.Add(this.usersEmailLabel);
            this.Controls.Add(this.emailJustTextLabel);
            this.Controls.Add(this.changePasswordStaticLabel);
            this.Controls.Add(this.changeEmailLabel);
            this.Controls.Add(this.usernameLabel);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "UserProfile";
            this.Text = "UserProfile1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button updateEmailButton;
        private Bunifu.Framework.UI.BunifuMaterialTextbox newEmailTextBox;
        private System.Windows.Forms.Label usersBirthDate;
        private System.Windows.Forms.Label birthDateJustTextLabel;
        private System.Windows.Forms.Label usersEmailLabel;
        private System.Windows.Forms.Label emailJustTextLabel;
        private System.Windows.Forms.Label changeEmailLabel;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.Label changePasswordStaticLabel;
        private Bunifu.Framework.UI.BunifuMaterialTextbox oldPasswordTextBox;
        private System.Windows.Forms.Button updatePwButton;
        private Bunifu.Framework.UI.BunifuMaterialTextbox newPasswordTextField;
        private Bunifu.Framework.UI.BunifuMaterialTextbox renewPasswordTextField;
    }
}