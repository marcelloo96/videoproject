﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.Dao;
using Video.Model;
using Video.View.MainPageSubPages.Components;

namespace Video.View.MainPageSubPages
{
    public partial class Playlist_Result : Form
    {
        const int SPACER = 30;
        private MainPage mainPage;
        private List<VideoBean> videoList;
        private UserBean user;
        private PlaylistBean playlist;

        public Playlist_Result(MainPage mainPage, UserBean user, PlaylistBean playlist)
        {
            this.mainPage = mainPage;
            this.playlist = playlist;
            this.user = user;
            InitializeComponent();
            Database listazz = new Database();
            videoList = listazz.listVideosOfPalylist(playlist);
            listVideos();
        }

        private void listVideos()
        {

            int previousVerticalPosition = 0;
            int height = 0;
            foreach (VideoBean beans in videoList)
            {
                SingleVideoForm a = new SingleVideoForm(this, user, beans, mainPage);
                a.Location = new Point(0, height + previousVerticalPosition + SPACER);

                previousVerticalPosition = a.Location.Y;
                height = a.Height;

                playlist_result_panel.Controls.Add(a);
            }

        }
    }
}
