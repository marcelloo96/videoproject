﻿namespace Video.View.MainPageSubPages
{
    partial class Playlists
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.playlists_panel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.add_playlist_buttom = new System.Windows.Forms.Button();
            this.add_new_playlist_textbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.playlists_panel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // playlists_panel
            // 
            this.playlists_panel.AutoScroll = true;
            this.playlists_panel.Controls.Add(this.panel1);
            this.playlists_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playlists_panel.Location = new System.Drawing.Point(0, 0);
            this.playlists_panel.Name = "playlists_panel";
            this.playlists_panel.Size = new System.Drawing.Size(1240, 824);
            this.playlists_panel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.add_playlist_buttom);
            this.panel1.Controls.Add(this.add_new_playlist_textbox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 781);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1240, 43);
            this.panel1.TabIndex = 0;
            // 
            // add_playlist_buttom
            // 
            this.add_playlist_buttom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.add_playlist_buttom.Location = new System.Drawing.Point(1070, 11);
            this.add_playlist_buttom.Name = "add_playlist_buttom";
            this.add_playlist_buttom.Size = new System.Drawing.Size(75, 23);
            this.add_playlist_buttom.TabIndex = 2;
            this.add_playlist_buttom.Text = "Add";
            this.add_playlist_buttom.UseVisualStyleBackColor = true;
            this.add_playlist_buttom.Click += new System.EventHandler(this.playlist_add_button);
            // 
            // add_new_playlist_textbox
            // 
            this.add_new_playlist_textbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.add_new_playlist_textbox.Location = new System.Drawing.Point(677, 11);
            this.add_new_playlist_textbox.Name = "add_new_playlist_textbox";
            this.add_new_playlist_textbox.Size = new System.Drawing.Size(387, 23);
            this.add_new_playlist_textbox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(499, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add New Playlist:";
            // 
            // Playlists
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 824);
            this.Controls.Add(this.playlists_panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Playlists";
            this.Text = "Playlists";
            this.playlists_panel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel playlists_panel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox add_new_playlist_textbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button add_playlist_buttom;
    }
}