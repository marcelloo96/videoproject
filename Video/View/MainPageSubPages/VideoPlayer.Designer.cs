﻿namespace VideoPlayerProject
{
    partial class VideoPlayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VideoPlayer));
            this.descriptionPanel = new System.Windows.Forms.Panel();
            this.sendCommentPanel = new System.Windows.Forms.Panel();
            this.sendCommentButton = new System.Windows.Forms.Button();
            this.sendCommentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.commentPanel = new System.Windows.Forms.Panel();
            this.commentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.volumeTrackBar = new System.Windows.Forms.TrackBar();
            this.seekerTrackBar = new System.Windows.Forms.TrackBar();
            this.muteButton = new Bunifu.Framework.UI.BunifuImageButton();
            this.playPauseButton = new Bunifu.Framework.UI.BunifuImageButton();
            this.userDateLabel = new System.Windows.Forms.Label();
            this.descriptionRichTextBox = new System.Windows.Forms.RichTextBox();
            this.likesLabel = new System.Windows.Forms.Label();
            this.viewsLabel = new System.Windows.Forms.Label();
            this.titleLabel = new System.Windows.Forms.Label();
            this.volumeLabel = new System.Windows.Forms.Label();
            this.playtimeDurationSeparatorLabel = new System.Windows.Forms.Label();
            this.durationLabel = new System.Windows.Forms.Label();
            this.playtimeLabel = new System.Windows.Forms.Label();
            this.mediaPlayerPanel = new System.Windows.Forms.Panel();
            this.commentRefresher = new System.Windows.Forms.Timer(this.components);
            this.likeButton = new System.Windows.Forms.Button();
            this.addToPlaylistButton = new System.Windows.Forms.Button();
            this.playlistsComboBox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.seekerTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.muteButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playPauseButton)).BeginInit();
            this.SuspendLayout();
            // 
            // descriptionPanel
            // 
            this.descriptionPanel.Location = new System.Drawing.Point(12, 660);
            this.descriptionPanel.Name = "descriptionPanel";
            this.descriptionPanel.Size = new System.Drawing.Size(896, 161);
            this.descriptionPanel.TabIndex = 125;
            // 
            // sendCommentPanel
            // 
            this.sendCommentPanel.Location = new System.Drawing.Point(914, 660);
            this.sendCommentPanel.Name = "sendCommentPanel";
            this.sendCommentPanel.Size = new System.Drawing.Size(314, 132);
            this.sendCommentPanel.TabIndex = 124;
            // 
            // sendCommentButton
            // 
            this.sendCommentButton.Location = new System.Drawing.Point(914, 798);
            this.sendCommentButton.Name = "sendCommentButton";
            this.sendCommentButton.Size = new System.Drawing.Size(314, 23);
            this.sendCommentButton.TabIndex = 123;
            this.sendCommentButton.Text = "Send";
            this.sendCommentButton.UseVisualStyleBackColor = true;
            this.sendCommentButton.Click += new System.EventHandler(this.sendCommentButton_Click);
            // 
            // sendCommentRichTextBox
            // 
            this.sendCommentRichTextBox.Location = new System.Drawing.Point(914, 660);
            this.sendCommentRichTextBox.Name = "sendCommentRichTextBox";
            this.sendCommentRichTextBox.Size = new System.Drawing.Size(314, 132);
            this.sendCommentRichTextBox.TabIndex = 122;
            this.sendCommentRichTextBox.Text = "";
            // 
            // commentPanel
            // 
            this.commentPanel.Location = new System.Drawing.Point(914, 5);
            this.commentPanel.Name = "commentPanel";
            this.commentPanel.Size = new System.Drawing.Size(314, 649);
            this.commentPanel.TabIndex = 121;
            // 
            // commentRichTextBox
            // 
            this.commentRichTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.commentRichTextBox.Location = new System.Drawing.Point(914, 5);
            this.commentRichTextBox.Name = "commentRichTextBox";
            this.commentRichTextBox.Size = new System.Drawing.Size(314, 649);
            this.commentRichTextBox.TabIndex = 120;
            this.commentRichTextBox.Text = "";
            // 
            // volumeTrackBar
            // 
            this.volumeTrackBar.Location = new System.Drawing.Point(718, 515);
            this.volumeTrackBar.Maximum = 100;
            this.volumeTrackBar.Name = "volumeTrackBar";
            this.volumeTrackBar.Size = new System.Drawing.Size(140, 45);
            this.volumeTrackBar.TabIndex = 119;
            this.volumeTrackBar.TickFrequency = 5;
            this.volumeTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.volumeTrackBar.Scroll += new System.EventHandler(this.volumeTrackBar_Scroll);
            // 
            // seekerTrackBar
            // 
            this.seekerTrackBar.Location = new System.Drawing.Point(50, 513);
            this.seekerTrackBar.Maximum = 100;
            this.seekerTrackBar.Name = "seekerTrackBar";
            this.seekerTrackBar.Size = new System.Drawing.Size(461, 45);
            this.seekerTrackBar.TabIndex = 118;
            this.seekerTrackBar.TickFrequency = 5;
            this.seekerTrackBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.seekerTrackBar.Scroll += new System.EventHandler(this.seekerTrackBar_Scroll);
            // 
            // muteButton
            // 
            this.muteButton.BackColor = System.Drawing.Color.Transparent;
            this.muteButton.Image = ((System.Drawing.Image)(resources.GetObject("muteButton.Image")));
            this.muteButton.ImageActive = null;
            this.muteButton.Location = new System.Drawing.Point(680, 515);
            this.muteButton.Name = "muteButton";
            this.muteButton.Size = new System.Drawing.Size(32, 32);
            this.muteButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.muteButton.TabIndex = 116;
            this.muteButton.TabStop = false;
            this.muteButton.Zoom = 10;
            this.muteButton.Click += new System.EventHandler(this.muteButton_Click);
            // 
            // playPauseButton
            // 
            this.playPauseButton.BackColor = System.Drawing.Color.Transparent;
            this.playPauseButton.Image = ((System.Drawing.Image)(resources.GetObject("playPauseButton.Image")));
            this.playPauseButton.ImageActive = null;
            this.playPauseButton.Location = new System.Drawing.Point(12, 513);
            this.playPauseButton.Name = "playPauseButton";
            this.playPauseButton.Size = new System.Drawing.Size(32, 32);
            this.playPauseButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.playPauseButton.TabIndex = 115;
            this.playPauseButton.TabStop = false;
            this.playPauseButton.Zoom = 10;
            this.playPauseButton.Click += new System.EventHandler(this.playPauseButton_Click);
            // 
            // userDateLabel
            // 
            this.userDateLabel.AutoSize = true;
            this.userDateLabel.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.userDateLabel.Location = new System.Drawing.Point(8, 600);
            this.userDateLabel.Name = "userDateLabel";
            this.userDateLabel.Size = new System.Drawing.Size(220, 19);
            this.userDateLabel.TabIndex = 114;
            this.userDateLabel.Text = "Uploaded by user at upload date";
            // 
            // descriptionRichTextBox
            // 
            this.descriptionRichTextBox.BackColor = System.Drawing.Color.White;
            this.descriptionRichTextBox.Location = new System.Drawing.Point(12, 660);
            this.descriptionRichTextBox.Name = "descriptionRichTextBox";
            this.descriptionRichTextBox.Size = new System.Drawing.Size(896, 161);
            this.descriptionRichTextBox.TabIndex = 113;
            this.descriptionRichTextBox.Text = "";
            // 
            // likesLabel
            // 
            this.likesLabel.AutoSize = true;
            this.likesLabel.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.likesLabel.Location = new System.Drawing.Point(8, 638);
            this.likesLabel.Name = "likesLabel";
            this.likesLabel.Size = new System.Drawing.Size(49, 19);
            this.likesLabel.TabIndex = 112;
            this.likesLabel.Text = "0 likes";
            // 
            // viewsLabel
            // 
            this.viewsLabel.AutoSize = true;
            this.viewsLabel.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.viewsLabel.Location = new System.Drawing.Point(8, 619);
            this.viewsLabel.Name = "viewsLabel";
            this.viewsLabel.Size = new System.Drawing.Size(57, 19);
            this.viewsLabel.TabIndex = 111;
            this.viewsLabel.Text = "0 views";
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Calibri Light", 24F);
            this.titleLabel.Location = new System.Drawing.Point(5, 561);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(73, 39);
            this.titleLabel.TabIndex = 110;
            this.titleLabel.Text = "Title";
            // 
            // volumeLabel
            // 
            this.volumeLabel.AutoSize = true;
            this.volumeLabel.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.volumeLabel.Location = new System.Drawing.Point(864, 522);
            this.volumeLabel.Name = "volumeLabel";
            this.volumeLabel.Size = new System.Drawing.Size(44, 19);
            this.volumeLabel.TabIndex = 109;
            this.volumeLabel.Text = "100%";
            // 
            // playtimeDurationSeparatorLabel
            // 
            this.playtimeDurationSeparatorLabel.AutoSize = true;
            this.playtimeDurationSeparatorLabel.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.playtimeDurationSeparatorLabel.Location = new System.Drawing.Point(588, 522);
            this.playtimeDurationSeparatorLabel.Name = "playtimeDurationSeparatorLabel";
            this.playtimeDurationSeparatorLabel.Size = new System.Drawing.Size(15, 19);
            this.playtimeDurationSeparatorLabel.TabIndex = 108;
            this.playtimeDurationSeparatorLabel.Text = "/";
            // 
            // durationLabel
            // 
            this.durationLabel.AutoSize = true;
            this.durationLabel.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.durationLabel.Location = new System.Drawing.Point(609, 522);
            this.durationLabel.Name = "durationLabel";
            this.durationLabel.Size = new System.Drawing.Size(65, 19);
            this.durationLabel.TabIndex = 107;
            this.durationLabel.Text = "00:00:00";
            // 
            // playtimeLabel
            // 
            this.playtimeLabel.AutoSize = true;
            this.playtimeLabel.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.playtimeLabel.Location = new System.Drawing.Point(517, 522);
            this.playtimeLabel.Name = "playtimeLabel";
            this.playtimeLabel.Size = new System.Drawing.Size(65, 19);
            this.playtimeLabel.TabIndex = 106;
            this.playtimeLabel.Text = "00:00:00";
            // 
            // mediaPlayerPanel
            // 
            this.mediaPlayerPanel.Location = new System.Drawing.Point(12, 5);
            this.mediaPlayerPanel.Name = "mediaPlayerPanel";
            this.mediaPlayerPanel.Size = new System.Drawing.Size(896, 504);
            this.mediaPlayerPanel.TabIndex = 105;
            // 
            // commentRefresher
            // 
            this.commentRefresher.Tick += new System.EventHandler(this.commentRefresher_Tick);
            // 
            // likeButton
            // 
            this.likeButton.Location = new System.Drawing.Point(876, 561);
            this.likeButton.Name = "likeButton";
            this.likeButton.Size = new System.Drawing.Size(32, 32);
            this.likeButton.TabIndex = 126;
            this.likeButton.Text = "L";
            this.likeButton.UseVisualStyleBackColor = true;
            this.likeButton.Click += new System.EventHandler(this.likeButton_Click);
            // 
            // addToPlaylistButton
            // 
            this.addToPlaylistButton.Location = new System.Drawing.Point(876, 594);
            this.addToPlaylistButton.Name = "addToPlaylistButton";
            this.addToPlaylistButton.Size = new System.Drawing.Size(32, 32);
            this.addToPlaylistButton.TabIndex = 128;
            this.addToPlaylistButton.Text = "P";
            this.addToPlaylistButton.UseVisualStyleBackColor = true;
            this.addToPlaylistButton.Click += new System.EventHandler(this.addToPlaylistButton_Click);
            // 
            // playlistsComboBox
            // 
            this.playlistsComboBox.FormattingEnabled = true;
            this.playlistsComboBox.Location = new System.Drawing.Point(680, 600);
            this.playlistsComboBox.Name = "playlistsComboBox";
            this.playlistsComboBox.Size = new System.Drawing.Size(190, 21);
            this.playlistsComboBox.TabIndex = 129;
            // 
            // VideoPlayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 840);
            this.Controls.Add(this.playlistsComboBox);
            this.Controls.Add(this.addToPlaylistButton);
            this.Controls.Add(this.likeButton);
            this.Controls.Add(this.descriptionPanel);
            this.Controls.Add(this.sendCommentPanel);
            this.Controls.Add(this.sendCommentButton);
            this.Controls.Add(this.sendCommentRichTextBox);
            this.Controls.Add(this.commentPanel);
            this.Controls.Add(this.commentRichTextBox);
            this.Controls.Add(this.volumeTrackBar);
            this.Controls.Add(this.seekerTrackBar);
            this.Controls.Add(this.muteButton);
            this.Controls.Add(this.playPauseButton);
            this.Controls.Add(this.userDateLabel);
            this.Controls.Add(this.descriptionRichTextBox);
            this.Controls.Add(this.likesLabel);
            this.Controls.Add(this.viewsLabel);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.volumeLabel);
            this.Controls.Add(this.playtimeDurationSeparatorLabel);
            this.Controls.Add(this.durationLabel);
            this.Controls.Add(this.playtimeLabel);
            this.Controls.Add(this.mediaPlayerPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VideoPlayer";
            this.Text = "VideoPlayer";
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.seekerTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.muteButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playPauseButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel descriptionPanel;
        private System.Windows.Forms.Panel sendCommentPanel;
        private System.Windows.Forms.Button sendCommentButton;
        private System.Windows.Forms.RichTextBox sendCommentRichTextBox;
        private System.Windows.Forms.Panel commentPanel;
        private System.Windows.Forms.RichTextBox commentRichTextBox;
        private System.Windows.Forms.TrackBar volumeTrackBar;
        private System.Windows.Forms.TrackBar seekerTrackBar;
        private Bunifu.Framework.UI.BunifuImageButton muteButton;
        private Bunifu.Framework.UI.BunifuImageButton playPauseButton;
        private System.Windows.Forms.Label userDateLabel;
        private System.Windows.Forms.RichTextBox descriptionRichTextBox;
        private System.Windows.Forms.Label likesLabel;
        private System.Windows.Forms.Label viewsLabel;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label volumeLabel;
        private System.Windows.Forms.Label playtimeDurationSeparatorLabel;
        private System.Windows.Forms.Label durationLabel;
        private System.Windows.Forms.Label playtimeLabel;
        private System.Windows.Forms.Panel mediaPlayerPanel;
        private System.Windows.Forms.Timer commentRefresher;
        private System.Windows.Forms.Button likeButton;
        private System.Windows.Forms.Button addToPlaylistButton;
        private System.Windows.Forms.ComboBox playlistsComboBox;
    }
}