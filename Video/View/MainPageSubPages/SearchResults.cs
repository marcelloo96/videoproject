﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.Dao;
using Video.Model;
using Video.View.MainPageSubPages.Components;

namespace Video.View.MainPageSubPages
{
    public partial class SearchResults : Form
    {
        const int SPACER = 30;
        private MainPage mainPage;
        private List<VideoBean> resultlist;
        private UserBean user;


        public SearchResults(List<VideoBean> list, UserBean user, MainPage mainPage) 
        {
            this.user = user;
            this.mainPage = mainPage;
            resultlist = list;
            InitializeComponent();
            listVideos();
            if (mainPage == null) {
                MessageBox.Show("searchResult konstruktorban null");
            }
            
        }

        private void listVideos() {
            Database dataAccess = new Database();

            int previousVerticalPosition=0;
            int height = 0;
            foreach (VideoBean videobean in resultlist) {
                SingleVideoForm a = new SingleVideoForm(this,user,videobean,mainPage);
                    
                a.Location = new Point(0,height+previousVerticalPosition+SPACER);
                previousVerticalPosition = a.Location.Y;
                height = a.Height;
                main_panel.Controls.Add(a);
            }
            
    
    
        }
    }
}
