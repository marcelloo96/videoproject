﻿namespace VideoPlayerProject
{
    partial class VideoUploader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.thumbnailLabel = new System.Windows.Forms.Label();
            this.areCommentsEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.isChildrenSafeCheckBox = new System.Windows.Forms.CheckBox();
            this.isPublicCheckBox = new System.Windows.Forms.CheckBox();
            this.labelsLabel = new System.Windows.Forms.Label();
            this.addLabelButton = new System.Windows.Forms.Button();
            this.labelToAddTextBox = new System.Windows.Forms.TextBox();
            this.deleteLabelButton = new System.Windows.Forms.Button();
            this.selectedLabelsListBox = new System.Windows.Forms.ListBox();
            this.categoriesLabel = new System.Windows.Forms.Label();
            this.selectedCategoriesListBox = new System.Windows.Forms.ListBox();
            this.addCategoryButton = new System.Windows.Forms.Button();
            this.categoriesToSelectListBox = new System.Windows.Forms.ListBox();
            this.deleteCategoryButton = new System.Windows.Forms.Button();
            this.descriptionPanel = new System.Windows.Forms.Panel();
            this.descriptionRichTextBox = new System.Windows.Forms.RichTextBox();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.titleLabel = new System.Windows.Forms.Label();
            this.ftpUploadProgressBar = new System.Windows.Forms.ProgressBar();
            this.thumbnailSelectTrackbar = new System.Windows.Forms.TrackBar();
            this.thumbnailPictureBox = new System.Windows.Forms.PictureBox();
            this.videoUploadButton = new System.Windows.Forms.Button();
            this.videoSelectButton = new System.Windows.Forms.Button();
            this.descriptionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.thumbnailSelectTrackbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thumbnailPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // thumbnailLabel
            // 
            this.thumbnailLabel.AutoSize = true;
            this.thumbnailLabel.Location = new System.Drawing.Point(9, 215);
            this.thumbnailLabel.Name = "thumbnailLabel";
            this.thumbnailLabel.Size = new System.Drawing.Size(59, 13);
            this.thumbnailLabel.TabIndex = 139;
            this.thumbnailLabel.Text = "Thumbnail:";
            // 
            // areCommentsEnabledCheckBox
            // 
            this.areCommentsEnabledCheckBox.AutoSize = true;
            this.areCommentsEnabledCheckBox.Location = new System.Drawing.Point(983, 728);
            this.areCommentsEnabledCheckBox.Name = "areCommentsEnabledCheckBox";
            this.areCommentsEnabledCheckBox.Size = new System.Drawing.Size(140, 17);
            this.areCommentsEnabledCheckBox.TabIndex = 138;
            this.areCommentsEnabledCheckBox.Text = "Are comments enabled?";
            this.areCommentsEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // isChildrenSafeCheckBox
            // 
            this.isChildrenSafeCheckBox.AutoSize = true;
            this.isChildrenSafeCheckBox.Location = new System.Drawing.Point(983, 705);
            this.isChildrenSafeCheckBox.Name = "isChildrenSafeCheckBox";
            this.isChildrenSafeCheckBox.Size = new System.Drawing.Size(150, 17);
            this.isChildrenSafeCheckBox.TabIndex = 137;
            this.isChildrenSafeCheckBox.Text = "Is the video children safe?";
            this.isChildrenSafeCheckBox.UseVisualStyleBackColor = true;
            // 
            // isPublicCheckBox
            // 
            this.isPublicCheckBox.AutoSize = true;
            this.isPublicCheckBox.Location = new System.Drawing.Point(983, 682);
            this.isPublicCheckBox.Name = "isPublicCheckBox";
            this.isPublicCheckBox.Size = new System.Drawing.Size(118, 17);
            this.isPublicCheckBox.TabIndex = 136;
            this.isPublicCheckBox.Text = "Is the video public?";
            this.isPublicCheckBox.UseVisualStyleBackColor = true;
            // 
            // labelsLabel
            // 
            this.labelsLabel.AutoSize = true;
            this.labelsLabel.Location = new System.Drawing.Point(914, 451);
            this.labelsLabel.Name = "labelsLabel";
            this.labelsLabel.Size = new System.Drawing.Size(41, 13);
            this.labelsLabel.TabIndex = 135;
            this.labelsLabel.Text = "Labels:";
            // 
            // addLabelButton
            // 
            this.addLabelButton.Location = new System.Drawing.Point(1200, 655);
            this.addLabelButton.Name = "addLabelButton";
            this.addLabelButton.Size = new System.Drawing.Size(27, 20);
            this.addLabelButton.TabIndex = 134;
            this.addLabelButton.Text = "+";
            this.addLabelButton.UseVisualStyleBackColor = true;
            this.addLabelButton.Click += new System.EventHandler(this.addLabelButton_Click);
            // 
            // labelToAddTextBox
            // 
            this.labelToAddTextBox.Location = new System.Drawing.Point(983, 656);
            this.labelToAddTextBox.Name = "labelToAddTextBox";
            this.labelToAddTextBox.Size = new System.Drawing.Size(211, 20);
            this.labelToAddTextBox.TabIndex = 133;
            // 
            // deleteLabelButton
            // 
            this.deleteLabelButton.Location = new System.Drawing.Point(1200, 451);
            this.deleteLabelButton.Name = "deleteLabelButton";
            this.deleteLabelButton.Size = new System.Drawing.Size(27, 198);
            this.deleteLabelButton.TabIndex = 132;
            this.deleteLabelButton.Text = "-";
            this.deleteLabelButton.UseVisualStyleBackColor = true;
            this.deleteLabelButton.Click += new System.EventHandler(this.deleteLabelButton_Click);
            // 
            // selectedLabelsListBox
            // 
            this.selectedLabelsListBox.FormattingEnabled = true;
            this.selectedLabelsListBox.Location = new System.Drawing.Point(983, 451);
            this.selectedLabelsListBox.Name = "selectedLabelsListBox";
            this.selectedLabelsListBox.Size = new System.Drawing.Size(211, 199);
            this.selectedLabelsListBox.TabIndex = 131;
            // 
            // categoriesLabel
            // 
            this.categoriesLabel.AutoSize = true;
            this.categoriesLabel.Location = new System.Drawing.Point(914, 41);
            this.categoriesLabel.Name = "categoriesLabel";
            this.categoriesLabel.Size = new System.Drawing.Size(60, 13);
            this.categoriesLabel.TabIndex = 130;
            this.categoriesLabel.Text = "Categories:";
            // 
            // selectedCategoriesListBox
            // 
            this.selectedCategoriesListBox.FormattingEnabled = true;
            this.selectedCategoriesListBox.Location = new System.Drawing.Point(983, 41);
            this.selectedCategoriesListBox.Name = "selectedCategoriesListBox";
            this.selectedCategoriesListBox.Size = new System.Drawing.Size(211, 199);
            this.selectedCategoriesListBox.TabIndex = 129;
            // 
            // addCategoryButton
            // 
            this.addCategoryButton.Location = new System.Drawing.Point(1200, 246);
            this.addCategoryButton.Name = "addCategoryButton";
            this.addCategoryButton.Size = new System.Drawing.Size(27, 199);
            this.addCategoryButton.TabIndex = 128;
            this.addCategoryButton.Text = "+";
            this.addCategoryButton.UseVisualStyleBackColor = true;
            this.addCategoryButton.Click += new System.EventHandler(this.addCategoryButton_Click);
            // 
            // categoriesToSelectListBox
            // 
            this.categoriesToSelectListBox.FormattingEnabled = true;
            this.categoriesToSelectListBox.Location = new System.Drawing.Point(983, 246);
            this.categoriesToSelectListBox.Name = "categoriesToSelectListBox";
            this.categoriesToSelectListBox.Size = new System.Drawing.Size(211, 199);
            this.categoriesToSelectListBox.TabIndex = 127;
            // 
            // deleteCategoryButton
            // 
            this.deleteCategoryButton.Location = new System.Drawing.Point(1201, 41);
            this.deleteCategoryButton.Name = "deleteCategoryButton";
            this.deleteCategoryButton.Size = new System.Drawing.Size(27, 199);
            this.deleteCategoryButton.TabIndex = 126;
            this.deleteCategoryButton.Text = "-";
            this.deleteCategoryButton.UseVisualStyleBackColor = true;
            this.deleteCategoryButton.Click += new System.EventHandler(this.deleteCategoryButton_Click);
            // 
            // descriptionPanel
            // 
            this.descriptionPanel.Controls.Add(this.descriptionRichTextBox);
            this.descriptionPanel.Location = new System.Drawing.Point(81, 67);
            this.descriptionPanel.Name = "descriptionPanel";
            this.descriptionPanel.Size = new System.Drawing.Size(827, 142);
            this.descriptionPanel.TabIndex = 125;
            // 
            // descriptionRichTextBox
            // 
            this.descriptionRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.descriptionRichTextBox.Name = "descriptionRichTextBox";
            this.descriptionRichTextBox.Size = new System.Drawing.Size(827, 142);
            this.descriptionRichTextBox.TabIndex = 8;
            this.descriptionRichTextBox.Text = "";
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(9, 70);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(63, 13);
            this.descriptionLabel.TabIndex = 124;
            this.descriptionLabel.Text = "Description:";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(81, 41);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(827, 20);
            this.titleTextBox.TabIndex = 123;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Location = new System.Drawing.Point(9, 44);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(30, 13);
            this.titleLabel.TabIndex = 122;
            this.titleLabel.Text = "Title:";
            // 
            // ftpUploadProgressBar
            // 
            this.ftpUploadProgressBar.Location = new System.Drawing.Point(12, 780);
            this.ftpUploadProgressBar.Name = "ftpUploadProgressBar";
            this.ftpUploadProgressBar.Size = new System.Drawing.Size(1215, 23);
            this.ftpUploadProgressBar.TabIndex = 121;
            // 
            // thumbnailSelectTrackbar
            // 
            this.thumbnailSelectTrackbar.Location = new System.Drawing.Point(81, 686);
            this.thumbnailSelectTrackbar.Name = "thumbnailSelectTrackbar";
            this.thumbnailSelectTrackbar.Size = new System.Drawing.Size(827, 45);
            this.thumbnailSelectTrackbar.TabIndex = 120;
            this.thumbnailSelectTrackbar.Scroll += new System.EventHandler(this.thumbnailSelectTrackbar_Scroll);
            // 
            // thumbnailPictureBox
            // 
            this.thumbnailPictureBox.Location = new System.Drawing.Point(81, 215);
            this.thumbnailPictureBox.Name = "thumbnailPictureBox";
            this.thumbnailPictureBox.Size = new System.Drawing.Size(827, 465);
            this.thumbnailPictureBox.TabIndex = 119;
            this.thumbnailPictureBox.TabStop = false;
            // 
            // videoUploadButton
            // 
            this.videoUploadButton.Location = new System.Drawing.Point(12, 751);
            this.videoUploadButton.Name = "videoUploadButton";
            this.videoUploadButton.Size = new System.Drawing.Size(1216, 23);
            this.videoUploadButton.TabIndex = 118;
            this.videoUploadButton.Text = "Upload video";
            this.videoUploadButton.UseVisualStyleBackColor = true;
            this.videoUploadButton.Click += new System.EventHandler(this.videoUploadButton_Click);
            // 
            // videoSelectButton
            // 
            this.videoSelectButton.Location = new System.Drawing.Point(12, 12);
            this.videoSelectButton.Name = "videoSelectButton";
            this.videoSelectButton.Size = new System.Drawing.Size(1215, 23);
            this.videoSelectButton.TabIndex = 117;
            this.videoSelectButton.Text = "Select video";
            this.videoSelectButton.UseVisualStyleBackColor = true;
            this.videoSelectButton.Click += new System.EventHandler(this.videoSelectButton_Click);
            // 
            // VideoUploader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 840);
            this.Controls.Add(this.thumbnailLabel);
            this.Controls.Add(this.areCommentsEnabledCheckBox);
            this.Controls.Add(this.isChildrenSafeCheckBox);
            this.Controls.Add(this.isPublicCheckBox);
            this.Controls.Add(this.labelsLabel);
            this.Controls.Add(this.addLabelButton);
            this.Controls.Add(this.labelToAddTextBox);
            this.Controls.Add(this.deleteLabelButton);
            this.Controls.Add(this.selectedLabelsListBox);
            this.Controls.Add(this.categoriesLabel);
            this.Controls.Add(this.selectedCategoriesListBox);
            this.Controls.Add(this.addCategoryButton);
            this.Controls.Add(this.categoriesToSelectListBox);
            this.Controls.Add(this.deleteCategoryButton);
            this.Controls.Add(this.descriptionPanel);
            this.Controls.Add(this.descriptionLabel);
            this.Controls.Add(this.titleTextBox);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.ftpUploadProgressBar);
            this.Controls.Add(this.thumbnailSelectTrackbar);
            this.Controls.Add(this.thumbnailPictureBox);
            this.Controls.Add(this.videoUploadButton);
            this.Controls.Add(this.videoSelectButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VideoUploader";
            this.Text = "VideoUploader";
            this.descriptionPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.thumbnailSelectTrackbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thumbnailPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label thumbnailLabel;
        private System.Windows.Forms.CheckBox areCommentsEnabledCheckBox;
        private System.Windows.Forms.CheckBox isChildrenSafeCheckBox;
        private System.Windows.Forms.CheckBox isPublicCheckBox;
        private System.Windows.Forms.Label labelsLabel;
        private System.Windows.Forms.Button addLabelButton;
        private System.Windows.Forms.TextBox labelToAddTextBox;
        private System.Windows.Forms.Button deleteLabelButton;
        private System.Windows.Forms.ListBox selectedLabelsListBox;
        private System.Windows.Forms.Label categoriesLabel;
        private System.Windows.Forms.ListBox selectedCategoriesListBox;
        private System.Windows.Forms.Button addCategoryButton;
        private System.Windows.Forms.ListBox categoriesToSelectListBox;
        private System.Windows.Forms.Button deleteCategoryButton;
        private System.Windows.Forms.Panel descriptionPanel;
        private System.Windows.Forms.RichTextBox descriptionRichTextBox;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.ProgressBar ftpUploadProgressBar;
        private System.Windows.Forms.TrackBar thumbnailSelectTrackbar;
        private System.Windows.Forms.PictureBox thumbnailPictureBox;
        private System.Windows.Forms.Button videoUploadButton;
        private System.Windows.Forms.Button videoSelectButton;
    }
}