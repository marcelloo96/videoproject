﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Declarations;
using Declarations.Events;
using Declarations.Media;
using Declarations.Players;
using Implementation;
using System.Drawing;
using System.Net;
using System.IO;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using Video.Model;
using System.Diagnostics;
using Video.HelperMethods;
using Video.Dao;

namespace VideoPlayerProject
{
    public partial class VideoPlayer : Form
    {
        IMediaPlayerFactory mediaPlayerFactory;
        IVideoPlayer mediaPlayerPlayer;
        IMedia mediaPlayerMedia;
        VideoBean video;
        UserBean user;
        Helper helper = new Helper();
        List<CommentBean> listOfComments;
        string username = "videoUploader";
        string password = "";

        public VideoPlayer(VideoBean video, UserBean user)
        {
            InitializeComponent();
            this.video = video;
            this.user = user;
            Debug.WriteLine("Video player created by " + user.username + "!");
            Debug.WriteLine(video.videoURL.ToString());
            Debug.WriteLine(video.videoThumbnailImageURL.ToString());

            Database kommentszekcio = new Database();
            listOfComments = kommentszekcio.listCommentsOfVideo(video,user);

            playlistsComboBox.DataSource = kommentszekcio.listPlaylists(user);
            playlistsComboBox.DisplayMember = nameof(PlaylistBean.playlistName);


            commentRichTextBox.Font = new Font("Calibri Light", 12);
            commentRichTextBox.SelectionProtected = true;
            commentRichTextBox.BorderStyle = BorderStyle.None;
            commentRichTextBox.ReadOnly = true;
            commentRichTextBox.BackColor = SystemColors.Control;
            commentRichTextBox.Dock = DockStyle.Fill;
            commentPanel.BorderStyle = BorderStyle.FixedSingle;
            commentPanel.Controls.Add(commentRichTextBox);


            sendCommentRichTextBox.Font = new Font("Calibri Light", 12);
            sendCommentRichTextBox.SelectionProtected = true;
            sendCommentRichTextBox.BorderStyle = BorderStyle.None;
            sendCommentRichTextBox.ReadOnly = false;
            sendCommentRichTextBox.BackColor = SystemColors.Control;
            sendCommentRichTextBox.Dock = DockStyle.Fill;
            sendCommentPanel.BorderStyle = BorderStyle.FixedSingle;
            sendCommentPanel.Controls.Add(sendCommentRichTextBox);


            descriptionRichTextBox.Font = new Font("Calibri Light", 12);
            descriptionRichTextBox.SelectionProtected = true;
            descriptionRichTextBox.BorderStyle = BorderStyle.None;
            descriptionRichTextBox.ReadOnly = false;
            descriptionRichTextBox.BackColor = SystemColors.Control;
            descriptionRichTextBox.Dock = DockStyle.Fill;
            descriptionPanel.BorderStyle = BorderStyle.None;
            descriptionPanel.Controls.Add(descriptionRichTextBox);

            this.Size = new Size(1240, 840);

            mediaPlayerFactory = new MediaPlayerFactory(true);
            mediaPlayerPlayer = mediaPlayerFactory.CreatePlayer<IVideoPlayer>();

            mediaPlayerMedia = mediaPlayerFactory.CreateMedia<IMedia>(video.videoURL.ToString().Insert(6, username + ":" + password + "@"));
            mediaPlayerPlayer.Open(mediaPlayerMedia);
            mediaPlayerPlayer.Events.PlayerPositionChanged += new EventHandler<MediaPlayerPositionChanged>(Events_PlayerPositionChanged);
            mediaPlayerPlayer.Events.TimeChanged += new EventHandler<MediaPlayerTimeChanged>(Events_TimeChanged);
            mediaPlayerPlayer.Events.MediaEnded += new EventHandler(Events_MediaEnded);
            mediaPlayerPlayer.Events.PlayerStopped += new EventHandler(Events_PlayerStopped);
            mediaPlayerMedia.Events.DurationChanged += new EventHandler<MediaDurationChange>(Events_DurationChanged);
            mediaPlayerPlayer.WindowHandle = mediaPlayerPanel.Handle;

            mediaPlayerPlayer.Volume = 50;
            volumeTrackBar.Value = 50;
            volumeTrackBar.Maximum = 100;
            volumeTrackBar.SmallChange = 5;
            volumeTrackBar.LargeChange = 5;
            volumeTrackBar.TickStyle = TickStyle.None;
            volumeLabel.Font = new Font("Calibri Light", 12);
            volumeLabel.Text = "50%";

            seekerTrackBar.Value = 0;
            seekerTrackBar.Maximum = 100;
            seekerTrackBar.SmallChange = 5;
            seekerTrackBar.LargeChange = 5;
            seekerTrackBar.TickStyle = TickStyle.None;
            playtimeLabel.Font = new Font("Calibri Light", 12);
            playtimeLabel.Text = "00:00:00";
            playtimeDurationSeparatorLabel.Font = new Font("Calibri Light", 12);
            playtimeDurationSeparatorLabel.Text = "/";
            durationLabel.Font = new Font("Calibri Light", 12);
            durationLabel.Text = "00:00:00";

            titleLabel.Font = new Font("Calibri Light", 24);
            titleLabel.Text = video.videoTitle;
            viewsLabel.Font = new Font("Calibri Light", 12);
            viewsLabel.Text = video.videoViewCount.ToString() + " views";
            likesLabel.Font = new Font("Calibri Light", 12);
            likesLabel.Text = video.videoLikeCount.ToString() + " likes";
            userDateLabel.Font = new Font("Calibri Light", 12);
            userDateLabel.Text = "Uploaded by " + user.username + " at " + video.videoUploadDate.ToString();
            descriptionRichTextBox.Text = video.videoDescription;

            commentRefresher.Interval = 1000;
            commentRefresher.Enabled = true;

            mediaPlayerPanel.BackgroundImage = helper.resizeImageWithPreservedAspectRatio(helper.getThumbnailFromFTP(video, "videoUploader", ""), 896, 504);
            forceRefreshCommentRichTextBox();
            UISync.Init(this);
        }



        private class UISync
        {
            private static ISynchronizeInvoke Sync;
            public static void Init(ISynchronizeInvoke sync)
            {
                Sync = sync;
            }
            public static void Execute(Action action)
            {
                Sync.BeginInvoke(action, null);
            }
        }

        private void InitControls()
        {
            mediaPlayerPlayer.Open(mediaPlayerMedia);
            UISync.Execute(() => playtimeLabel.Text = "00:00:00");
            UISync.Execute(() => durationLabel.Text = "00:00:00");
            UISync.Execute(() => seekerTrackBar.Value = 0);
            UISync.Execute(() => playPauseButton.Image = Image.FromFile("play.png"));
        }

        private void playPauseButton_Click(object sender, EventArgs e)
        {
            if (mediaPlayerPlayer.IsPlaying)
            {
                mediaPlayerPlayer.Pause();
                UISync.Execute(() => playPauseButton.Image = Image.FromFile("play.png"));
            }
            else
            {
                mediaPlayerPlayer.Play();
                UISync.Execute(() => playPauseButton.Image = Image.FromFile("pause.png"));
            }

        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            mediaPlayerPlayer.Pause();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            mediaPlayerPlayer.Stop();
        }

        private void muteButton_Click(object sender, EventArgs e)
        {
            int previousVolume = mediaPlayerPlayer.Volume;
            mediaPlayerPlayer.ToggleMute();
            if (mediaPlayerPlayer.Mute)
            {
                UISync.Execute(() => volumeTrackBar.Value = 0);
                UISync.Execute(() => volumeLabel.Text = volumeTrackBar.Value.ToString() + "%");
                UISync.Execute(() => muteButton.Image = Image.FromFile("muted.png"));
            }
            else
            {
                mediaPlayerPlayer.Volume = previousVolume;
                UISync.Execute(() => volumeTrackBar.Value = mediaPlayerPlayer.Volume);
                UISync.Execute(() => volumeLabel.Text = volumeTrackBar.Value.ToString() + "%");
                UISync.Execute(() => muteButton.Image = Image.FromFile("unmuted.png"));
            }
        }

        private void volumeTrackBar_Scroll(object sender, EventArgs e)
        {
            mediaPlayerPlayer.Volume = volumeTrackBar.Value;
            UISync.Execute(() => volumeLabel.Text = volumeTrackBar.Value.ToString() + "%");
        }

        private void seekerTrackBar_Scroll(object sender, EventArgs e)
        {
            mediaPlayerPlayer.Position = (float)seekerTrackBar.Value / 100;
        }

        void Events_PlayerStopped(object sender, EventArgs e)
        {
            UISync.Execute(() => InitControls());
        }

        void Events_MediaEnded(object sender, EventArgs e)
        {
            UISync.Execute(() => InitControls());
        }

        void Events_TimeChanged(object sender, MediaPlayerTimeChanged e)
        {
            UISync.Execute(() => playtimeLabel.Text = TimeSpan.FromMilliseconds(e.NewTime).ToString().Substring(0, 8));
        }

        void Events_PlayerPositionChanged(object sender, MediaPlayerPositionChanged e)
        {
            UISync.Execute(() => seekerTrackBar.Value = (int)(e.NewPosition * 100));
        }

        void Events_DurationChanged(object sender, MediaDurationChange e)
        {
            UISync.Execute(() => durationLabel.Text = TimeSpan.FromMilliseconds(e.NewDuration).ToString().Substring(0, 8));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UISync.Execute(() => InitControls());
        }

        private void VideoPlayer_FormClosing(object sender, FormClosingEventArgs e)
        {
            mediaPlayerMedia.Dispose();
            mediaPlayerPlayer.Dispose();
            mediaPlayerFactory.Dispose();
        }

        private void sendCommentButton_Click(object sender, EventArgs e)
        {
            if (!sendCommentRichTextBox.Text.Equals(String.Empty))
            {
                CommentBean commentToSend = new CommentBean()
                {
                    commentText = sendCommentRichTextBox.Text,
                    commentWriteDate = DateTime.Now,
                    userId = user.userid,
                    videoId = video.videoId
                };
                Database databaseAccess = new Database();
                databaseAccess.insertComment(commentToSend);
                refreshCommentRichTextBox();
            }
        }

        private void refreshCommentRichTextBox()
        {
            var prevLength = listOfComments.Count;
            Database databaseAccess = new Database();
            listOfComments = databaseAccess.listCommentsOfVideo(video, user);
            var actualLenth = listOfComments.Count;
            if (prevLength < actualLenth)
            {
                commentRichTextBox.Text = String.Empty;
                for (int i = listOfComments.Count - 1; i >= 0; i--)
                {
                    var username = databaseAccess.selectUserFromUserTableByUserId(listOfComments[i].userId).username;
                    if (i == 0)
                    {
                        int rtbLength = commentRichTextBox.Text.Length;
                        string publisherAndPublishDate = "By " + username + " at " + " " + listOfComments[i].commentWriteDate.ToString();
                        string textOfComment = listOfComments[i].commentText;
                        commentRichTextBox.AppendText(publisherAndPublishDate + Environment.NewLine + textOfComment);
                        commentRichTextBox.Select(rtbLength, publisherAndPublishDate.Length);
                        commentRichTextBox.SelectionFont = new Font(commentRichTextBox.Font, FontStyle.Bold);
                        commentRichTextBox.DeselectAll();
                    }
                    else
                    {
                        int rtbLength = commentRichTextBox.Text.Length;
                        string publisherAndPublishDate = "By " + username + " at " + " " + listOfComments[i].commentWriteDate.ToString();
                        string textOfComment = listOfComments[i].commentText;
                        commentRichTextBox.AppendText(publisherAndPublishDate + Environment.NewLine + textOfComment + Environment.NewLine + Environment.NewLine);
                        commentRichTextBox.Select(rtbLength, publisherAndPublishDate.Length);
                        commentRichTextBox.SelectionFont = new Font(commentRichTextBox.Font, FontStyle.Bold);
                        commentRichTextBox.DeselectAll();
                    }
                }
            }
        }

        private void forceRefreshCommentRichTextBox()
        {
            commentRichTextBox.Text = String.Empty;
            Database databaseAccess = new Database();
            listOfComments = databaseAccess.listCommentsOfVideo(video, user);
            for (int i = listOfComments.Count - 1; i >= 0; i--)
            {
                var username = databaseAccess.selectUserFromUserTableByUserId(listOfComments[i].userId).username;
                if (i == 0)
                {
                    int rtbLength = commentRichTextBox.Text.Length;
                    string publisherAndPublishDate = "By " + username + " at " + " " + listOfComments[i].commentWriteDate.ToString();
                    string textOfComment = listOfComments[i].commentText;
                    commentRichTextBox.AppendText(publisherAndPublishDate + Environment.NewLine + textOfComment);
                    commentRichTextBox.Select(rtbLength, publisherAndPublishDate.Length);
                    commentRichTextBox.SelectionFont = new Font(commentRichTextBox.Font, FontStyle.Bold);
                    commentRichTextBox.DeselectAll();
                }
                else
                {
                    int rtbLength = commentRichTextBox.Text.Length;
                    string publisherAndPublishDate = "By " + username + " at " + " " + listOfComments[i].commentWriteDate.ToString();
                    string textOfComment = listOfComments[i].commentText;
                    commentRichTextBox.AppendText(publisherAndPublishDate + Environment.NewLine + textOfComment + Environment.NewLine + Environment.NewLine);
                    commentRichTextBox.Select(rtbLength, publisherAndPublishDate.Length);
                    commentRichTextBox.SelectionFont = new Font(commentRichTextBox.Font, FontStyle.Bold);
                    commentRichTextBox.DeselectAll();
                }
            }
        }

        private void commentRefresher_Tick(object sender, EventArgs e)
        {
            refreshCommentRichTextBox();
        }

        private void likeButton_Click(object sender, EventArgs e)
        {
            Database databaseAccess = new Database();
            databaseAccess.incrementLikeCountOfVideo(video, user);
        }

        private void addToPlaylistButton_Click(object sender, EventArgs e)
        {
            if (playlistsComboBox.SelectedIndex != -1)
            {
                Database databaseAccess = new Database();
                var playlist = playlistsComboBox.SelectedItem as PlaylistBean;
                databaseAccess.addVideoToPlaylist(video, playlist);
                MessageBox.Show("Added to playlist!");
            }
        }
    }
}
