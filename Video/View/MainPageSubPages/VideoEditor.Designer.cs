﻿namespace Video.View.MainPageSubPages
{
    partial class VideoEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.areCommentsEnabledCheckBox = new System.Windows.Forms.CheckBox();
            this.isChildrenSafeCheckBox = new System.Windows.Forms.CheckBox();
            this.isPublicCheckBox = new System.Windows.Forms.CheckBox();
            this.labelsLabel = new System.Windows.Forms.Label();
            this.addLabelButton = new System.Windows.Forms.Button();
            this.labelToAddTextBox = new System.Windows.Forms.TextBox();
            this.deleteLabelButton = new System.Windows.Forms.Button();
            this.selectedLabelsListBox = new System.Windows.Forms.ListBox();
            this.categoriesLabel = new System.Windows.Forms.Label();
            this.selectedCategoriesListBox = new System.Windows.Forms.ListBox();
            this.addCategoryButton = new System.Windows.Forms.Button();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.descriptionRichTextBox = new System.Windows.Forms.RichTextBox();
            this.categoriesToSelectListBox = new System.Windows.Forms.ListBox();
            this.deleteCategoryButton = new System.Windows.Forms.Button();
            this.descriptionPanel = new System.Windows.Forms.Panel();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.videoEditButton = new System.Windows.Forms.Button();
            this.descriptionPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // areCommentsEnabledCheckBox
            // 
            this.areCommentsEnabledCheckBox.AutoSize = true;
            this.areCommentsEnabledCheckBox.Location = new System.Drawing.Point(664, 482);
            this.areCommentsEnabledCheckBox.Name = "areCommentsEnabledCheckBox";
            this.areCommentsEnabledCheckBox.Size = new System.Drawing.Size(140, 17);
            this.areCommentsEnabledCheckBox.TabIndex = 161;
            this.areCommentsEnabledCheckBox.Text = "Are comments enabled?";
            this.areCommentsEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // isChildrenSafeCheckBox
            // 
            this.isChildrenSafeCheckBox.AutoSize = true;
            this.isChildrenSafeCheckBox.Location = new System.Drawing.Point(664, 459);
            this.isChildrenSafeCheckBox.Name = "isChildrenSafeCheckBox";
            this.isChildrenSafeCheckBox.Size = new System.Drawing.Size(150, 17);
            this.isChildrenSafeCheckBox.TabIndex = 160;
            this.isChildrenSafeCheckBox.Text = "Is the video children safe?";
            this.isChildrenSafeCheckBox.UseVisualStyleBackColor = true;
            // 
            // isPublicCheckBox
            // 
            this.isPublicCheckBox.AutoSize = true;
            this.isPublicCheckBox.Location = new System.Drawing.Point(664, 436);
            this.isPublicCheckBox.Name = "isPublicCheckBox";
            this.isPublicCheckBox.Size = new System.Drawing.Size(118, 17);
            this.isPublicCheckBox.TabIndex = 159;
            this.isPublicCheckBox.Text = "Is the video public?";
            this.isPublicCheckBox.UseVisualStyleBackColor = true;
            // 
            // labelsLabel
            // 
            this.labelsLabel.AutoSize = true;
            this.labelsLabel.Location = new System.Drawing.Point(595, 205);
            this.labelsLabel.Name = "labelsLabel";
            this.labelsLabel.Size = new System.Drawing.Size(41, 13);
            this.labelsLabel.TabIndex = 158;
            this.labelsLabel.Text = "Labels:";
            // 
            // addLabelButton
            // 
            this.addLabelButton.Location = new System.Drawing.Point(881, 409);
            this.addLabelButton.Name = "addLabelButton";
            this.addLabelButton.Size = new System.Drawing.Size(27, 20);
            this.addLabelButton.TabIndex = 157;
            this.addLabelButton.Text = "+";
            this.addLabelButton.UseVisualStyleBackColor = true;
            this.addLabelButton.Click += new System.EventHandler(this.addLabelButton_Click);
            // 
            // labelToAddTextBox
            // 
            this.labelToAddTextBox.Location = new System.Drawing.Point(664, 410);
            this.labelToAddTextBox.Name = "labelToAddTextBox";
            this.labelToAddTextBox.Size = new System.Drawing.Size(211, 20);
            this.labelToAddTextBox.TabIndex = 156;
            // 
            // deleteLabelButton
            // 
            this.deleteLabelButton.Location = new System.Drawing.Point(881, 205);
            this.deleteLabelButton.Name = "deleteLabelButton";
            this.deleteLabelButton.Size = new System.Drawing.Size(27, 198);
            this.deleteLabelButton.TabIndex = 155;
            this.deleteLabelButton.Text = "-";
            this.deleteLabelButton.UseVisualStyleBackColor = true;
            this.deleteLabelButton.Click += new System.EventHandler(this.deleteLabelButton_Click);
            // 
            // selectedLabelsListBox
            // 
            this.selectedLabelsListBox.FormattingEnabled = true;
            this.selectedLabelsListBox.Location = new System.Drawing.Point(664, 205);
            this.selectedLabelsListBox.Name = "selectedLabelsListBox";
            this.selectedLabelsListBox.Size = new System.Drawing.Size(211, 199);
            this.selectedLabelsListBox.TabIndex = 154;
            // 
            // categoriesLabel
            // 
            this.categoriesLabel.AutoSize = true;
            this.categoriesLabel.Location = new System.Drawing.Point(12, 205);
            this.categoriesLabel.Name = "categoriesLabel";
            this.categoriesLabel.Size = new System.Drawing.Size(60, 13);
            this.categoriesLabel.TabIndex = 153;
            this.categoriesLabel.Text = "Categories:";
            // 
            // selectedCategoriesListBox
            // 
            this.selectedCategoriesListBox.FormattingEnabled = true;
            this.selectedCategoriesListBox.Location = new System.Drawing.Point(81, 205);
            this.selectedCategoriesListBox.Name = "selectedCategoriesListBox";
            this.selectedCategoriesListBox.Size = new System.Drawing.Size(211, 199);
            this.selectedCategoriesListBox.TabIndex = 152;
            // 
            // addCategoryButton
            // 
            this.addCategoryButton.Location = new System.Drawing.Point(298, 410);
            this.addCategoryButton.Name = "addCategoryButton";
            this.addCategoryButton.Size = new System.Drawing.Size(27, 199);
            this.addCategoryButton.TabIndex = 151;
            this.addCategoryButton.Text = "+";
            this.addCategoryButton.UseVisualStyleBackColor = true;
            this.addCategoryButton.Click += new System.EventHandler(this.addCategoryButton_Click);
            // 
            // descriptionRichTextBox
            // 
            this.descriptionRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.descriptionRichTextBox.Name = "descriptionRichTextBox";
            this.descriptionRichTextBox.Size = new System.Drawing.Size(827, 142);
            this.descriptionRichTextBox.TabIndex = 8;
            this.descriptionRichTextBox.Text = "";
            // 
            // categoriesToSelectListBox
            // 
            this.categoriesToSelectListBox.FormattingEnabled = true;
            this.categoriesToSelectListBox.Location = new System.Drawing.Point(81, 410);
            this.categoriesToSelectListBox.Name = "categoriesToSelectListBox";
            this.categoriesToSelectListBox.Size = new System.Drawing.Size(211, 199);
            this.categoriesToSelectListBox.TabIndex = 150;
            // 
            // deleteCategoryButton
            // 
            this.deleteCategoryButton.Location = new System.Drawing.Point(299, 205);
            this.deleteCategoryButton.Name = "deleteCategoryButton";
            this.deleteCategoryButton.Size = new System.Drawing.Size(27, 199);
            this.deleteCategoryButton.TabIndex = 149;
            this.deleteCategoryButton.Text = "-";
            this.deleteCategoryButton.UseVisualStyleBackColor = true;
            this.deleteCategoryButton.Click += new System.EventHandler(this.deleteCategoryButton_Click);
            // 
            // descriptionPanel
            // 
            this.descriptionPanel.Controls.Add(this.descriptionRichTextBox);
            this.descriptionPanel.Location = new System.Drawing.Point(81, 57);
            this.descriptionPanel.Name = "descriptionPanel";
            this.descriptionPanel.Size = new System.Drawing.Size(827, 142);
            this.descriptionPanel.TabIndex = 148;
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(9, 60);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(63, 13);
            this.descriptionLabel.TabIndex = 147;
            this.descriptionLabel.Text = "Description:";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(81, 31);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(827, 20);
            this.titleTextBox.TabIndex = 146;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Location = new System.Drawing.Point(9, 34);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(30, 13);
            this.titleLabel.TabIndex = 145;
            this.titleLabel.Text = "Title:";
            // 
            // videoEditButton
            // 
            this.videoEditButton.Location = new System.Drawing.Point(12, 615);
            this.videoEditButton.Name = "videoEditButton";
            this.videoEditButton.Size = new System.Drawing.Size(896, 23);
            this.videoEditButton.TabIndex = 141;
            this.videoEditButton.Text = "Edit video";
            this.videoEditButton.UseVisualStyleBackColor = true;
            this.videoEditButton.Click += new System.EventHandler(this.videoEditButton_Click);
            // 
            // VideoEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1240, 840);
            this.Controls.Add(this.areCommentsEnabledCheckBox);
            this.Controls.Add(this.isChildrenSafeCheckBox);
            this.Controls.Add(this.isPublicCheckBox);
            this.Controls.Add(this.labelsLabel);
            this.Controls.Add(this.addLabelButton);
            this.Controls.Add(this.labelToAddTextBox);
            this.Controls.Add(this.deleteLabelButton);
            this.Controls.Add(this.selectedLabelsListBox);
            this.Controls.Add(this.categoriesLabel);
            this.Controls.Add(this.selectedCategoriesListBox);
            this.Controls.Add(this.addCategoryButton);
            this.Controls.Add(this.categoriesToSelectListBox);
            this.Controls.Add(this.deleteCategoryButton);
            this.Controls.Add(this.descriptionPanel);
            this.Controls.Add(this.descriptionLabel);
            this.Controls.Add(this.titleTextBox);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.videoEditButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VideoEditor";
            this.Text = "VideoEditor";
            this.descriptionPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox areCommentsEnabledCheckBox;
        private System.Windows.Forms.CheckBox isChildrenSafeCheckBox;
        private System.Windows.Forms.CheckBox isPublicCheckBox;
        private System.Windows.Forms.Label labelsLabel;
        private System.Windows.Forms.Button addLabelButton;
        private System.Windows.Forms.TextBox labelToAddTextBox;
        private System.Windows.Forms.Button deleteLabelButton;
        private System.Windows.Forms.ListBox selectedLabelsListBox;
        private System.Windows.Forms.Label categoriesLabel;
        private System.Windows.Forms.ListBox selectedCategoriesListBox;
        private System.Windows.Forms.Button addCategoryButton;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.RichTextBox descriptionRichTextBox;
        private System.Windows.Forms.ListBox categoriesToSelectListBox;
        private System.Windows.Forms.Button deleteCategoryButton;
        private System.Windows.Forms.Panel descriptionPanel;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Button videoEditButton;
    }
}