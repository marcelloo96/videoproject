﻿using NReco.VideoInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.Dao;
using Video.HelperMethods;
using Video.Model;

namespace Video.View.MainPageSubPages
{
    public partial class VideoEditor : Form
    {
        Helper helper = new Helper();
        Database databaseAccess = new Database();
        Image thumbnailImage;
        VideoBean video;
        public VideoEditor(VideoBean video)
        {
            this.video = video;
            InitializeComponent();

            titleTextBox.Text = video.videoTitle;
            descriptionRichTextBox.Text = video.videoDescription;

            categoriesToSelectListBox.DataSource = databaseAccess.listCategories();
            categoriesToSelectListBox.DisplayMember = nameof(CategoryBean.categoryName);
            categoriesToSelectListBox.ValueMember = nameof(CategoryBean.categoryId);


            foreach (var actualElement in databaseAccess.listCategoriesOfVideo(video))
            {
                selectedCategoriesListBox.Items.Add(actualElement);
            }

            selectedCategoriesListBox.DisplayMember = nameof(CategoryBean.categoryName);
            selectedCategoriesListBox.ValueMember = nameof(CategoryBean.categoryId);

            foreach (var actualElement in databaseAccess.listLabelsOfVideo(video))
            {
                selectedLabelsListBox.Items.Add(actualElement);
            }
        }

        private void deleteCategoryButton_Click(object sender, EventArgs e)
        {
            if (selectedCategoriesListBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a category first.");
            }
            else
            {
                selectedCategoriesListBox.Items.Remove(selectedCategoriesListBox.SelectedItem);
            }
        }

        private void addCategoryButton_Click(object sender, EventArgs e)
        {
            if (categoriesToSelectListBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a category first.");
            }
            else
            {
                bool isAlreadySelected = false;
                var category = categoriesToSelectListBox.SelectedItem as CategoryBean;
                foreach (CategoryBean actualElement in selectedCategoriesListBox.Items)
                {
                    if (actualElement.categoryId == category.categoryId)
                    {
                        isAlreadySelected = true;
                    }
                }
                if (isAlreadySelected)
                {
                    MessageBox.Show("This category is already assigned to that video!");
                }
                else
                {
                    selectedCategoriesListBox.Items.Add(category);
                }
            }
        }

        private void deleteLabelButton_Click(object sender, EventArgs e)
        {
            if (selectedLabelsListBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a label first.");
            }
            else
            {
                selectedLabelsListBox.Items.Remove(selectedLabelsListBox.SelectedItem);
            }
        }

        private void addLabelButton_Click(object sender, EventArgs e)
        {
            if (labelToAddTextBox.Text == String.Empty)
            {
                MessageBox.Show("Please write a label first.");
            }
            else
            {
                bool isAlreadyAdded = false;
                foreach (var actualElement in selectedLabelsListBox.Items)
                {
                    if (actualElement.Equals(labelToAddTextBox.Text))
                    {
                        isAlreadyAdded = true;
                    }
                }
                if (isAlreadyAdded)
                {
                    MessageBox.Show("This label is already assigned to that video!");
                }
                else
                {
                    selectedLabelsListBox.Items.Add(labelToAddTextBox.Text);
                }
            }
        }

        private void videoEditButton_Click(object sender, EventArgs e)
        {
            if (titleTextBox.Text.Equals(String.Empty) || descriptionRichTextBox.Text.Equals(String.Empty) || selectedCategoriesListBox.Items.Count == 0 || selectedLabelsListBox.Items.Count == 0)
            {
                MessageBox.Show("Please fill all input fields!");
            }
            else
            {
                video.videoTitle = titleTextBox.Text;
                video.videoDescription = descriptionRichTextBox.Text;
                video.isPublic = isPublicCheckBox.Checked == true ? 1 : 0;
                video.isChildrenSafe = isChildrenSafeCheckBox.Checked == true ? 1 : 0;
                video.areCommentsEnabled = areCommentsEnabledCheckBox.Checked == true ? 1 : 0;
                databaseAccess.updateVideoFromVideoTable(video);
                databaseAccess.deleteCategoriesFromVideo(video);
                databaseAccess.deleteLabelsFromVideo(video);
                foreach (CategoryBean actualElement in selectedCategoriesListBox.Items)
                {
                    databaseAccess.insertCategoryToVideo(actualElement, video);
                }
                foreach (var actualElement in selectedLabelsListBox.Items)
                {
                    databaseAccess.insertLabelAndAddItToVideo(actualElement.ToString(), video);
                }
            }
        }
    }
}
