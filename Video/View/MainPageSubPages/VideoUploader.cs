﻿using NReco.VideoConverter;
using NReco.VideoInfo;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Windows.Forms;
using Video.Dao;
using Video.HelperMethods;
using Video.Model;

namespace VideoPlayerProject
{
    public partial class VideoUploader : Form
    {
        Helper helper = new Helper();
        Database databaseAccess = new Database();
        string videoUploadPath;

        Image thumbnailImage;
        int thumbnailImageWidth = 896;
        int thumbnailImageHeight = 504;

        string ftpPath = "ftp://127.0.0.1";
        string ftpUsername = "videoUploader";
        string ftpPassword = "";

        int userId;

        public VideoUploader(UserBean user)
        {
            InitializeComponent();

            userId = user.userid;
            thumbnailSelectTrackbar.Minimum = 0;
            thumbnailSelectTrackbar.Maximum = 1;
            thumbnailSelectTrackbar.Value = 0;
            thumbnailSelectTrackbar.SmallChange = 0;
            thumbnailSelectTrackbar.LargeChange = 0;
            thumbnailSelectTrackbar.TickStyle = TickStyle.None;
            thumbnailSelectTrackbar.TickFrequency = 1;

            descriptionRichTextBox.BorderStyle = BorderStyle.None;
            descriptionPanel.BorderStyle = BorderStyle.FixedSingle;
            descriptionPanel.Controls.Add(descriptionRichTextBox);
            descriptionRichTextBox.Dock = DockStyle.Fill;

            categoriesToSelectListBox.DataSource = databaseAccess.listCategories();
            categoriesToSelectListBox.DisplayMember = nameof(CategoryBean.categoryName);
            categoriesToSelectListBox.ValueMember = nameof(CategoryBean.categoryId);
            selectedCategoriesListBox.DisplayMember = nameof(CategoryBean.categoryName);
            selectedCategoriesListBox.ValueMember = nameof(CategoryBean.categoryId);

            backgroundWorker.WorkerReportsProgress = true;
        }

        private void videoSelectButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog videoUploadFileDialog = new OpenFileDialog();
            videoUploadFileDialog.InitialDirectory = "c:\\";
            videoUploadFileDialog.Filter = "All files (*.*)|*.*";
            videoUploadFileDialog.RestoreDirectory = true;
            if (videoUploadFileDialog.ShowDialog() == DialogResult.OK)
            {
                videoUploadPath = videoUploadFileDialog.FileName;
                FFProbe ffProbe = new FFProbe();
                thumbnailSelectTrackbar.Maximum = (int)ffProbe.GetMediaInfo(videoUploadPath).Duration.TotalSeconds;
                thumbnailImage = helper.getThumbnailFromVideo(videoUploadPath, 0, thumbnailImageWidth, thumbnailImageHeight);
                thumbnailPictureBox.Image = thumbnailImage;
            }
        }

        private void videoUploadButton_Click(object sender, EventArgs e)
        {
           
            if (videoUploadPath == null || thumbnailImage == null ||titleTextBox.Text.Equals(String.Empty) || descriptionRichTextBox.Text.Equals(String.Empty) || selectedCategoriesListBox.Items.Count == 0 || selectedLabelsListBox.Items.Count == 0)
            {
                MessageBox.Show("Please fill all input fields!");
            }
            else
            {
                this.Enabled = false;
                int videoInsertId = databaseAccess.getNextUnusedIdOfVideoTable();
                VideoBean video = new VideoBean()
                {
                    videoId = videoInsertId,
                    videoURL = new Uri(helper.uploadFileToFTP(videoUploadPath, ftpPath, "video" + videoInsertId.ToString(), ftpUsername, ftpPassword, backgroundWorker)),
                    videoThumbnailImageURL = new Uri(helper.uploadStreamToFTP(helper.imageToStream(thumbnailImage), ftpPath, "thumbnail" + videoInsertId.ToString(), ftpUsername, ftpPassword, backgroundWorker)),
                    videoTitle = titleTextBox.Text,
                    videoDescription = descriptionRichTextBox.Text,
                    videoUploadDate = DateTime.Now,
                    videoViewCount = 0,
                    videoLikeCount = 0,
                    isPublic = isPublicCheckBox.Checked == true ? 1 : 0,
                    isChildrenSafe = isChildrenSafeCheckBox.Checked == true ? 1 : 0,
                    areCommentsEnabled = areCommentsEnabledCheckBox.Checked == true ? 1 : 0,
                    userID = userId
                };
                databaseAccess.insertVideoIntoVideoTable(video);
                foreach (CategoryBean actualElement in selectedCategoriesListBox.Items)
                {
                    databaseAccess.insertCategoryToVideo(actualElement, video);
                }
                foreach (var actualElement in selectedLabelsListBox.Items)
                {
                    databaseAccess.insertLabelAndAddItToVideo(actualElement.ToString(), video);
                }
                this.Enabled = true;
                MessageBox.Show("Video successfully uploaded!");
            }
        }

        private void thumbnailSelectTrackbar_Scroll(object sender, EventArgs e)
        {
            thumbnailImage = helper.getThumbnailFromVideo(videoUploadPath, thumbnailSelectTrackbar.Value, thumbnailImageWidth, thumbnailImageHeight);
            thumbnailPictureBox.Image = thumbnailImage;
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ftpUploadProgressBar.Value = e.ProgressPercentage;
        }

        private void addCategoryButton_Click(object sender, EventArgs e)
        {
            if (categoriesToSelectListBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a category first.");
            }
            else
            {
                bool isAlreadySelected = false;
                var category = categoriesToSelectListBox.SelectedItem as CategoryBean;
                foreach (CategoryBean actualElement in selectedCategoriesListBox.Items){
                    if (actualElement.categoryId == category.categoryId)
                    {
                        isAlreadySelected = true;
                    }
                }
                if (isAlreadySelected)
                {
                    MessageBox.Show("This category is already assigned to that video!");
                }
                else
                {
                    selectedCategoriesListBox.Items.Add(category);
                }
            }
        }

        private void deleteCategoryButton_Click(object sender, EventArgs e)
        {
            if (selectedCategoriesListBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a category first.");
            }
            else
            {
                selectedCategoriesListBox.Items.Remove(selectedCategoriesListBox.SelectedItem);
            }
        }

        private void addLabelButton_Click(object sender, EventArgs e)
        {
            if (labelToAddTextBox.Text == String.Empty)
            {
                MessageBox.Show("Please write a label first.");
            }
            else
            {
                bool isAlreadyAdded = false;
                foreach (var actualElement in selectedLabelsListBox.Items)
                {
                    if (actualElement.Equals(labelToAddTextBox.Text))
                    {
                        isAlreadyAdded = true;
                    }
                }
                if (isAlreadyAdded)
                {
                    MessageBox.Show("This label is already assigned to that video!");
                }
                else
                {
                    selectedLabelsListBox.Items.Add(labelToAddTextBox.Text);
                }
            }
        }

        private void deleteLabelButton_Click(object sender, EventArgs e)
        {
            if (selectedLabelsListBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a label first.");
            }
            else
            {
                selectedLabelsListBox.Items.Remove(selectedLabelsListBox.SelectedItem);
            }
        }
    }
}