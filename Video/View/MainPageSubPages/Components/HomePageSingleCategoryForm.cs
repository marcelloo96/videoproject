﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.HelperMethods;
using Video.Model;
using System.Diagnostics;

namespace Video.View.MainPageSubPages.Components
{
    public partial class HomePageSingleCategoryForm : UserControl
    {
        Helper helper = new Helper();
        Form bezarando;
        UserBean user;
        VideoBean video;
        MainPage main;

        public HomePageSingleCategoryForm(Form bezarando, UserBean user, VideoBean video, MainPage main)
        {
            this.bezarando = bezarando;
            this.user = user;
            this.video = video;
            this.main = main;
            InitializeComponent();
        }

        private void videoThumbnail_Click(object sender, EventArgs e)
        {
            if (bezarando == null)
            {
                Debug.WriteLine("bezarando null");
            }
            if (video == null)
            {
                Debug.WriteLine("video null");
            }
            if (user == null)
            {
                Debug.WriteLine("user null");
            }
            helper.openVideo(bezarando, main.middleContentArea, video, user);
        }
    }
}
