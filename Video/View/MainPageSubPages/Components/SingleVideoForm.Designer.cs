﻿namespace Video.View.MainPageSubPages.Components
{
    partial class SingleVideoForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.videoThumbnail = new System.Windows.Forms.Panel();
            this.videoTitle = new System.Windows.Forms.Label();
            this.uploaderLabel = new System.Windows.Forms.Label();
            this.viewsLabel = new System.Windows.Forms.Label();
            this.likesLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.videoDescription = new System.Windows.Forms.RichTextBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // videoThumbnail
            // 
            this.videoThumbnail.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.videoThumbnail.Location = new System.Drawing.Point(20, 20);
            this.videoThumbnail.Name = "videoThumbnail";
            this.videoThumbnail.Size = new System.Drawing.Size(256, 144);
            this.videoThumbnail.TabIndex = 0;
            this.videoThumbnail.Click += new System.EventHandler(this.videoThumbnail_Click);
            // 
            // videoTitle
            // 
            this.videoTitle.AutoSize = true;
            this.videoTitle.Font = new System.Drawing.Font("Calibri Light", 24F);
            this.videoTitle.Location = new System.Drawing.Point(302, 23);
            this.videoTitle.Name = "videoTitle";
            this.videoTitle.Size = new System.Drawing.Size(73, 39);
            this.videoTitle.TabIndex = 1;
            this.videoTitle.Text = "Title";
            // 
            // uploaderLabel
            // 
            this.uploaderLabel.AutoSize = true;
            this.uploaderLabel.Font = new System.Drawing.Font("Calibri Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.uploaderLabel.Location = new System.Drawing.Point(305, 62);
            this.uploaderLabel.Name = "uploaderLabel";
            this.uploaderLabel.Size = new System.Drawing.Size(69, 19);
            this.uploaderLabel.TabIndex = 1;
            this.uploaderLabel.Text = "Uploader";
            // 
            // viewsLabel
            // 
            this.viewsLabel.AutoSize = true;
            this.viewsLabel.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.viewsLabel.Location = new System.Drawing.Point(305, 81);
            this.viewsLabel.Name = "viewsLabel";
            this.viewsLabel.Size = new System.Drawing.Size(41, 19);
            this.viewsLabel.TabIndex = 1;
            this.viewsLabel.Text = "2000";
            // 
            // likesLabel
            // 
            this.likesLabel.AutoSize = true;
            this.likesLabel.Font = new System.Drawing.Font("Calibri Light", 12F);
            this.likesLabel.Location = new System.Drawing.Point(305, 100);
            this.likesLabel.Name = "likesLabel";
            this.likesLabel.Size = new System.Drawing.Size(41, 19);
            this.likesLabel.TabIndex = 2;
            this.likesLabel.Text = "2000";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 164);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1240, 20);
            this.panel1.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(20, 184);
            this.panel2.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(1220, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(20, 184);
            this.panel3.TabIndex = 5;
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1240, 20);
            this.panel4.TabIndex = 6;
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(276, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(20, 184);
            this.panel5.TabIndex = 7;
            // 
            // videoDescription
            // 
            this.videoDescription.Location = new System.Drawing.Point(307, 122);
            this.videoDescription.Name = "videoDescription";
            this.videoDescription.Size = new System.Drawing.Size(916, 42);
            this.videoDescription.TabIndex = 8;
            this.videoDescription.Text = "";
            // 
            // deleteButton
            // 
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteButton.Location = new System.Drawing.Point(1139, 20);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 9;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Visible = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // editButton
            // 
            this.editButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editButton.Location = new System.Drawing.Point(1058, 20);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(75, 23);
            this.editButton.TabIndex = 10;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Visible = false;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // SingleVideoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.videoDescription);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.videoThumbnail);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.likesLabel);
            this.Controls.Add(this.viewsLabel);
            this.Controls.Add(this.uploaderLabel);
            this.Controls.Add(this.videoTitle);
            this.Name = "SingleVideoForm";
            this.Size = new System.Drawing.Size(1240, 184);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Panel videoThumbnail;
        public System.Windows.Forms.Label videoTitle;
        public System.Windows.Forms.Label uploaderLabel;
        public System.Windows.Forms.Label viewsLabel;
        public System.Windows.Forms.Label likesLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RichTextBox videoDescription;
        public System.Windows.Forms.Button deleteButton;
        public System.Windows.Forms.Button editButton;
    }
}
