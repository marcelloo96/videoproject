﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.HelperMethods;
using Video.Model;

namespace Video.View.MainPageSubPages.Components
{
    public partial class HomePageSingleVideoForm : UserControl
    {
        Helper helper = new Helper();
        Form bezarando;
        UserBean user;
        VideoBean video;
        MainPage main;
        public HomePageSingleVideoForm(Form bezarando, UserBean user, VideoBean video, MainPage main)
        {
            this.bezarando = bezarando;
            this.user = user;
            this.video = video;
            this.main = main;
            InitializeComponent();
        }

        private void videoThumbnail_Click(object sender, EventArgs e)
        {
            helper.openVideo(bezarando, main.middleContentArea, video, user);
        }

    }
}
