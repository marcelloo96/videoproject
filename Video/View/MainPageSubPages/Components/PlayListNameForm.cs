﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.View.MainPageSubPages;
using Video.Model;
using Video.Dao;

namespace Video.View.MainPageSubPages.Components
{
    public partial class PlayListNameForm : UserControl
    {
        MainPage mainPage;
        List<VideoBean> videoList;
        UserBean user;
        PlaylistBean playlist;
        List<PlaylistBean> list;
        const int SPACER = 5;

        public PlayListNameForm(MainPage mainPage, PlaylistBean playlist, UserBean user)
        {
            this.playlist = playlist;
            this.mainPage = mainPage;
            this.user = user;
            InitializeComponent();
            
        }

        private void playlist_delete_button_click(object sender, EventArgs e)
        {

            Database dao = new Database();
            dao.deletePlaylist(playlist);
            closeFormsExceptThis("MainPage");
            playlist_panel.Controls.Clear();
            refreshplaylists_panel();
            /*closeFormsExceptThis("MainPage");
            playlist_panel.Controls.Clear();
            Playlists playlists = new Playlists(mainPage,user);
            playlists.FormBorderStyle = FormBorderStyle.None;
            playlists.TopLevel = false;
            playlists.AutoScroll = true;
            playlist_panel.Controls.Add(playlists);
            playlists.Show();
            playlists.BringToFront();*/

        }

        private void playlist_click(object sender, EventArgs e)
        {
            closeFormsExceptThis("MainPage");
            playlist_panel.Controls.Clear();
            Playlist_Result playlistres = new Playlist_Result(mainPage, user, playlist);
            playlistres.FormBorderStyle = FormBorderStyle.None;
            playlistres.TopLevel = false;
            playlistres.AutoScroll = true;
            playlist_panel.Controls.Add(playlistres);
            playlistres.Show();
            playlistres.BringToFront();

        }

        private void closeFormsExceptThis(string formName)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != formName)
                {
                    Application.OpenForms[i].Close();
                }
            }
        }

        private void refreshplaylists_panel()
        {
            Playlists pl = new Playlists(mainPage, user);
            Database databaseAccess = new Database();
            list = databaseAccess.listPlaylists(user);
            pl.listPlaylists();

        }
    }
}
