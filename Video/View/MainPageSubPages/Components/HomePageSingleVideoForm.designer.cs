﻿namespace Video.View.MainPageSubPages.Components
{
    partial class HomePageSingleVideoForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.video_thumbnail = new System.Windows.Forms.PictureBox();
            this.video_title = new System.Windows.Forms.RichTextBox();
            this.views = new System.Windows.Forms.Label();
            this.view_count = new System.Windows.Forms.Label();
            this.like_count = new System.Windows.Forms.Label();
            this.likes = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.video_thumbnail)).BeginInit();
            this.SuspendLayout();
            // 
            // video_thumbnail
            // 
            this.video_thumbnail.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.video_thumbnail.Location = new System.Drawing.Point(3, 3);
            this.video_thumbnail.Name = "video_thumbnail";
            this.video_thumbnail.Size = new System.Drawing.Size(192, 108);
            this.video_thumbnail.TabIndex = 0;
            this.video_thumbnail.TabStop = false;
            this.video_thumbnail.Click += new System.EventHandler(this.videoThumbnail_Click);
            // 
            // video_title
            // 
            this.video_title.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.video_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.video_title.Location = new System.Drawing.Point(3, 106);
            this.video_title.Name = "video_title";
            this.video_title.ReadOnly = true;
            this.video_title.Size = new System.Drawing.Size(192, 45);
            this.video_title.TabIndex = 1;
            this.video_title.Text = "";
            // 
            // views
            // 
            this.views.AutoSize = true;
            this.views.Location = new System.Drawing.Point(49, 154);
            this.views.Name = "views";
            this.views.Size = new System.Drawing.Size(30, 13);
            this.views.TabIndex = 2;
            this.views.Text = "View";
            // 
            // view_count
            // 
            this.view_count.AutoSize = true;
            this.view_count.Location = new System.Drawing.Point(0, 154);
            this.view_count.Name = "view_count";
            this.view_count.Size = new System.Drawing.Size(31, 13);
            this.view_count.TabIndex = 3;
            this.view_count.Text = "2500";
            // 
            // like_count
            // 
            this.like_count.AutoSize = true;
            this.like_count.Location = new System.Drawing.Point(101, 154);
            this.like_count.Name = "like_count";
            this.like_count.Size = new System.Drawing.Size(31, 13);
            this.like_count.TabIndex = 4;
            this.like_count.Text = "2500";
            // 
            // likes
            // 
            this.likes.AutoSize = true;
            this.likes.Location = new System.Drawing.Point(151, 154);
            this.likes.Name = "likes";
            this.likes.Size = new System.Drawing.Size(27, 13);
            this.likes.TabIndex = 5;
            this.likes.Text = "Like";
            // 
            // HomePageSingleVideoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.likes);
            this.Controls.Add(this.like_count);
            this.Controls.Add(this.view_count);
            this.Controls.Add(this.views);
            this.Controls.Add(this.video_title);
            this.Controls.Add(this.video_thumbnail);
            this.Name = "HomePageSingleVideoForm";
            this.Size = new System.Drawing.Size(200, 180);
            ((System.ComponentModel.ISupportInitialize)(this.video_thumbnail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label views;
        private System.Windows.Forms.Label likes;
        public System.Windows.Forms.PictureBox video_thumbnail;
        public System.Windows.Forms.RichTextBox video_title;
        public System.Windows.Forms.Label view_count;
        public System.Windows.Forms.Label like_count;
    }
}
