﻿namespace Video.View.MainPageSubPages.Components
{
    partial class HomePageSingleUserForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomePageSingleUserForm));
            this.profile_picture = new System.Windows.Forms.PictureBox();
            this.username = new System.Windows.Forms.Label();
            this.count = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.profile_picture)).BeginInit();
            this.SuspendLayout();
            // 
            // profile_picture
            // 
            this.profile_picture.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.profile_picture.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("profile_picture.BackgroundImage")));
            this.profile_picture.Location = new System.Drawing.Point(4, 4);
            this.profile_picture.Name = "profile_picture";
            this.profile_picture.Size = new System.Drawing.Size(80, 80);
            this.profile_picture.TabIndex = 0;
            this.profile_picture.TabStop = false;
            // 
            // username
            // 
            this.username.AutoSize = true;
            this.username.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.username.Location = new System.Drawing.Point(3, 87);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(0, 20);
            this.username.TabIndex = 1;
            // 
            // count
            // 
            this.count.AutoSize = true;
            this.count.Location = new System.Drawing.Point(25, 107);
            this.count.Name = "count";
            this.count.Size = new System.Drawing.Size(31, 13);
            this.count.TabIndex = 2;
            this.count.Text = "5841";
            this.count.Visible = false;
            // 
            // HomePageSingleUserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.count);
            this.Controls.Add(this.username);
            this.Controls.Add(this.profile_picture);
            this.Name = "HomePageSingleUserForm";
            this.Size = new System.Drawing.Size(87, 124);
            ((System.ComponentModel.ISupportInitialize)(this.profile_picture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Label username;
        public System.Windows.Forms.PictureBox profile_picture;
        public System.Windows.Forms.Label count;
    }
}
