﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Video.Model;
using Video.HelperMethods;
using Video.Dao;

namespace Video.View.MainPageSubPages.Components
{
    public partial class SingleVideoForm: UserControl
    {
        Helper helper = new Helper();
        MainPage main;
        public VideoBean video;
        public UserBean user;
        Form  bezarando;
        public SingleVideoForm(Form bezarando, UserBean user, VideoBean video, MainPage main)
        {
            InitializeComponent();
            this.bezarando = bezarando;
            this.main = main;
            this.video = video;
            this.user = user;
            videoThumbnail.BackgroundImage = helper.resizeImageWithPreservedAspectRatio(helper.getThumbnailFromFTP(video, "videoUploader", ""), 256, 144);
            videoTitle.Font = new Font("Calibri Light", 24);
            videoTitle.Text = video.videoTitle;
            uploaderLabel.Font = new Font("Calibri Light", 12);
            uploaderLabel.Text = "Uploaded by " + user.username;
            viewsLabel.Font = new Font("Calibri Light", 12);
            viewsLabel.Text = video.videoViewCount.ToString() + " views";
            likesLabel.Font = new Font("Calibri Light", 12);
            likesLabel.Text = video.videoLikeCount.ToString() + " likes";
            videoDescription.Font = new Font("Calibri Light", 12);
            videoDescription.Text = video.videoDescription;
            videoDescription.Enabled = false;
            videoDescription.SelectAll();
            videoDescription.SelectionColor = Color.Black;
            videoDescription.SelectionProtected = true;
            videoDescription.BorderStyle = BorderStyle.None;
            videoDescription.ReadOnly = true;
            videoDescription.BackColor = SystemColors.Control;
            videoDescription.ScrollBars = RichTextBoxScrollBars.None;
        }

        private void videoThumbnail_Click(object sender, EventArgs e)
        {
            if (bezarando == null) {
                MessageBox.Show("bezarando null");
            } else if (main == null)
            {
                MessageBox.Show("main null");
            }else if (video == null)
            {
                MessageBox.Show("video null");
            }else if (user == null)
            {
                MessageBox.Show("user null");
            }
            helper.openVideo(bezarando, main.middleContentArea, video, user);
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            Database databaseaccess = new Database();
            
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            //closeFormsExceptThis("MainPage");
            main.middleContentArea.Controls.Clear();
            VideoEditor videoEditor = new VideoEditor(video);
            videoEditor.FormBorderStyle = FormBorderStyle.None;
            videoEditor.TopLevel = false;
            videoEditor.AutoScroll = true;
            main.middleContentArea.Controls.Add(videoEditor);
            videoEditor.Show();
        }
    }
}
