﻿namespace Video.View.MainPageSubPages.Components
{
    partial class PlayListNameForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.playlist_panel = new System.Windows.Forms.Panel();
            this.playlist_delete = new System.Windows.Forms.Button();
            this.playlist_name = new System.Windows.Forms.Label();
            this.playlist_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // playlist_panel
            // 
            this.playlist_panel.Controls.Add(this.playlist_delete);
            this.playlist_panel.Controls.Add(this.playlist_name);
            this.playlist_panel.Location = new System.Drawing.Point(0, 0);
            this.playlist_panel.Name = "playlist_panel";
            this.playlist_panel.Size = new System.Drawing.Size(1240, 45);
            this.playlist_panel.TabIndex = 0;
            this.playlist_panel.Click += new System.EventHandler(this.playlist_click);
            // 
            // playlist_delete
            // 
            this.playlist_delete.Location = new System.Drawing.Point(1152, 13);
            this.playlist_delete.Name = "playlist_delete";
            this.playlist_delete.Size = new System.Drawing.Size(75, 23);
            this.playlist_delete.TabIndex = 63;
            this.playlist_delete.Text = "Delete";
            this.playlist_delete.UseVisualStyleBackColor = true;
            this.playlist_delete.Click += new System.EventHandler(this.playlist_delete_button_click);
            // 
            // playlist_name
            // 
            this.playlist_name.AutoSize = true;
            this.playlist_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.playlist_name.Location = new System.Drawing.Point(3, 0);
            this.playlist_name.Name = "playlist_name";
            this.playlist_name.Size = new System.Drawing.Size(167, 39);
            this.playlist_name.TabIndex = 60;
            this.playlist_name.Text = "Laza Buli";
            this.playlist_name.Click += new System.EventHandler(this.playlist_click);
            // 
            // PlayListNameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.playlist_panel);
            this.Name = "PlayListNameForm";
            this.Size = new System.Drawing.Size(1240, 45);
            this.playlist_panel.ResumeLayout(false);
            this.playlist_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel playlist_panel;
        private System.Windows.Forms.Button playlist_delete;
        public System.Windows.Forms.Label playlist_name;
    }
}
