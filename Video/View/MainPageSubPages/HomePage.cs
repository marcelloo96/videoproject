﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.Dao;
using Video.HelperMethods;
using Video.Model;
using Video.Model.Beans.Helper;
using Video.View.MainPageSubPages.Components;

namespace Video.View.MainPageSubPages
{
    public partial class HomePage : Form
    {

        const int SPACER = 5;
        List<VideoBean> list;
        UserBean user;
        MainPage main;
        VideoBean video;

        public HomePage(UserBean user, MainPage main)
        {
            this.main = main;
            this.user = user;
            
            InitializeComponent();
            listMostCommentsUsers();
            listMostUploadUsers();
            listMostViewedVideosMonth();
            listMostViewedVideosWeek();
            listMostViewedVideosDay();
            listMostViewedCategories();
            
            //listMostViewedVideosByCategories();
            listNewestVideosByCategories();
            //listSimilarInterestUsers();
            //listRecommendedVideos();
            //listRecommendedUsers();
            //listRecommendedVideosByLabel(user);
        }

        public static Image ResizeImage(Image imgToResize, Size destinationSize)
        {
            var originalWidth = imgToResize.Width;
            var originalHeight = imgToResize.Height;
            var hRatio = (float)originalHeight / destinationSize.Height;
            var wRatio = (float)originalWidth / destinationSize.Width;
            var ratio = Math.Min(hRatio, wRatio);
            var hScale = Convert.ToInt32(destinationSize.Height * ratio);
            var wScale = Convert.ToInt32(destinationSize.Width * ratio);
            var startX = (originalWidth - wScale) / 2;
            var startY = (originalHeight - hScale) / 2;
            var sourceRectangle = new Rectangle(startX, startY, wScale, hScale);
            var bitmap = new Bitmap(destinationSize.Width, destinationSize.Height);
            var destinationRectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(imgToResize, destinationRectangle, sourceRectangle, GraphicsUnit.Pixel);
            }
            return bitmap;
        }

        private void listMostCommentsUsers()
        {
            const int SPACER = 5;
            Database listazz = new Database();
            List<HomePageSingleUserForm> list = listazz.listMostCommentedusers();

            int previousHorizontalPosition = 25;
            int width = 0;
            foreach (HomePageSingleUserForm form in list)
            {
                form.Location = new Point(width + previousHorizontalPosition + SPACER, 35);

                previousHorizontalPosition = form.Location.X;
                width = form.Width;

                homepage_panel.Controls.Add(form);
            }
        }

        private void listMostUploadUsers()
        {
            const int SPACER = 5;
            Database listazz = new Database();
            List<HomePageSingleUserForm> list = listazz.getListOfUsersWithMostUploadedVideos();

            int previousHorizontalPosition = 25;
            int width = 0;
            foreach (HomePageSingleUserForm form in list)
            {
                form.Location = new Point(width + previousHorizontalPosition + SPACER, 205);

                previousHorizontalPosition = form.Location.X;
                width = form.Width;

                homepage_panel.Controls.Add(form);
            }
        }


        private void listMostViewedVideosMonth()
        {
            Helper helper = new Helper();
            const int SPACER = 0;
            Database listazz = new Database();
            List<VideoBean> list = listazz.listMostViewedVideosOfMonth();

            int previousHorizontalPosition = 25;
            int width = 0;
            foreach (VideoBean video in list)
            {
                HomePageSingleVideoForm a = new HomePageSingleVideoForm(this,user,video,main);
                a.video_title.Text = video.videoTitle;
                a.view_count.Text = video.videoViewCount.ToString();
                a.like_count.Text = video.videoLikeCount.ToString();

                a.video_thumbnail.Image = helper.resizeImageWithPreservedAspectRatio(helper.getThumbnailFromFTP(video, "videoUploader", ""), 192, 108);
                a.Location = new Point(width + previousHorizontalPosition + SPACER, 420);

                previousHorizontalPosition = a.Location.X;
                width = a.Width;

                homepage_panel.Controls.Add(a);
            }
        }


        private void listMostViewedVideosWeek()
        {
            Helper helper = new Helper();
            const int SPACER = 0;
            Database listazz = new Database();
            List<VideoBean> list = listazz.listMostViewedVideosOfWeek();

            int previousHorizontalPosition = 25;
            int width = 0;
            foreach (VideoBean video in list)
            {
                HomePageSingleVideoForm a = new HomePageSingleVideoForm(this, user, video, main);
                a.video_title.Text = video.videoTitle;
                a.view_count.Text = video.videoViewCount.ToString();
                a.like_count.Text = video.videoLikeCount.ToString();

                a.video_thumbnail.Image = helper.resizeImageWithPreservedAspectRatio(helper.getThumbnailFromFTP(video, "videoUploader", ""), 192, 108);

                a.Location = new Point(width + previousHorizontalPosition + SPACER, 625);

                previousHorizontalPosition = a.Location.X;
                width = a.Width;

                homepage_panel.Controls.Add(a);
            }
        }


        private void listMostViewedVideosDay()
        {
            Helper helper = new Helper();

            const int SPACER = 0;
            Database listazz = new Database();
            List<VideoBean> list = listazz.listMostViewedVideosOfDay();

            int previousHorizontalPosition = 25;
            int width = 0;
            foreach (VideoBean video in list)
            {
                HomePageSingleVideoForm a = new HomePageSingleVideoForm(this, user, video, main);
                a.video_title.Text = video.videoTitle;
                a.view_count.Text = video.videoViewCount.ToString();
                a.like_count.Text = video.videoLikeCount.ToString();

                a.video_thumbnail.Image = helper.resizeImageWithPreservedAspectRatio(helper.getThumbnailFromFTP(video, "videoUploader", ""), 192, 108);
                a.Location = new Point(width + previousHorizontalPosition + SPACER, 840);

                previousHorizontalPosition = a.Location.X;
                width = a.Width;

                homepage_panel.Controls.Add(a);
            }
        }


        private void listMostViewedCategories()
        {
            const int SPACER = 5;
            const int panelHeight = 220;
           
            Helper helper = new Helper();
            Database listazz = new Database();
            List<CategoryLabel> categoryList = listazz.listMostViewedCategories();

            MostViewedVideosOfCategories mostViewedVideos = listazz.listMostViewedVideosByCategories();
            int previousVerticalPosition = 850 ;

            foreach (CategoryLabel category in categoryList)
            {
                category.Location = new Point(0, previousVerticalPosition + SPACER+ panelHeight);

                previousVerticalPosition = category.Location.Y;
                homepage_panel.Controls.Add(category);

            }

            int previousHorizontalPosition = 25;
            int width = 0;
            List<VideoBean> videoList = mostViewedVideos.cat1;
           
            foreach (VideoBean video in videoList)                  //First CAtegory
             {
                HomePageSingleVideoForm a = new HomePageSingleVideoForm(this, user, video, main);
                a.video_title.Text = video.videoTitle;
                a.view_count.Text = video.videoViewCount.ToString();
                a.like_count.Text = video.videoLikeCount.ToString();

                a.video_thumbnail.Image = helper.resizeImageWithPreservedAspectRatio(helper.getThumbnailFromFTP(video, "videoUploader", ""), 192, 108);
                a.Location = new Point(width + previousHorizontalPosition + SPACER, 1120);

                previousHorizontalPosition = a.Location.X;
                width = a.Width;

                homepage_panel.Controls.Add(a);

            }


            previousHorizontalPosition = 25;
            width = 0;
            videoList = mostViewedVideos.cat2;
            int RowHeigth = 225;

            foreach (VideoBean video in videoList)                  //Second CAtegory
            {
                HomePageSingleVideoForm a = new HomePageSingleVideoForm(this, user, video, main);
                a.video_title.Text = video.videoTitle;
                a.view_count.Text = video.videoViewCount.ToString();
                a.like_count.Text = video.videoLikeCount.ToString();

                a.video_thumbnail.Image = helper.resizeImageWithPreservedAspectRatio(helper.getThumbnailFromFTP(video, "videoUploader", ""), 192, 108);
                a.Location = new Point(width + previousHorizontalPosition + SPACER, 1120 + RowHeigth);

                previousHorizontalPosition = a.Location.X;
                width = a.Width;

                homepage_panel.Controls.Add(a);

            }

            previousHorizontalPosition = 25;
            width = 0;
            videoList = mostViewedVideos.cat3;
            RowHeigth = 225;

            foreach (VideoBean video in videoList)                  //Third CAtegory
            {
                HomePageSingleVideoForm a = new HomePageSingleVideoForm(this, user, video, main);
                a.video_title.Text = video.videoTitle;
                a.view_count.Text = video.videoViewCount.ToString();
                a.like_count.Text = video.videoLikeCount.ToString();

                a.video_thumbnail.Image = helper.resizeImageWithPreservedAspectRatio(helper.getThumbnailFromFTP(video, "videoUploader", ""), 192, 108);
                a.Location = new Point(width + previousHorizontalPosition + SPACER, 1120 + 2 * RowHeigth);

                previousHorizontalPosition = a.Location.X;
                width = a.Width;

                homepage_panel.Controls.Add(a);

            }
        }


        private void listMostViewedVideosByCategories()
        {
            const int SPACER = 0;

            Database listazz = new Database();
            List<HomePageSingleCategoryForm> list = listazz.listMostViewedVideoByCategories();


            int previousHorizontalPosition = 25;
            int previousVerticalPosition = 1150;
            int width = 0;
            int height = 0;
            int counter = 0;

            foreach (HomePageSingleCategoryForm forms in list)
                {
                    forms.Location = new Point(width + previousHorizontalPosition + SPACER, height + previousVerticalPosition + SPACER);

                    previousHorizontalPosition = forms.Location.X;
                    width = forms.Width;

                    homepage_panel.Controls.Add(forms);

                if (counter == 5)
                {
                    previousHorizontalPosition = 25;
                    previousVerticalPosition = forms.Location.Y;
                    height = forms.Height;
                    width = 0;
                    counter = 0;
                }
                else {
                    counter++;
                }

                    
            }
            
        }


        private void listNewestVideosByCategories()
        {
            const int SPACER = 0;


            Database listazz = new Database();
            List<HomePageSingleCategoryForm> list = listazz.listNewestVideosByCategories();


            int previousHorizontalPosition = 25;
            int width = 0;


            foreach (HomePageSingleCategoryForm form in list)
            {
                form.Location = new Point(width + previousHorizontalPosition + SPACER, 1850);

                previousHorizontalPosition = form.Location.X;
                width = form.Width;

                homepage_panel.Controls.Add(form);
            }

        }


        private void listSimilarInterestUsers()
        {
            const int SPACER = 5;

            List<UserBean> list = new List<UserBean>();

            for (int i = 0; i < 13; i++)
            {
                UserBean user = new UserBean("Matthew", "gsfdgfdgfdg", "szabomati@gmail.com", "2012");
                list.Add(user);
            }

            int previousHorizontalPosition = 25;
            int width = 0;
            foreach (UserBean beans in list)
            {
                HomePageSingleUserForm a = new HomePageSingleUserForm();
                a.username.Text = beans.username;

                //Ide jön egy lekérdezés a user kommentjeiről
                //meg a képét valahogy be kell neki adni
                /* WebClient wc = new WebClient();
                 byte[] bytes = wc.DownloadData(user.videoThumbnailImage.ToString());
                 MemoryStream ms = new MemoryStream(bytes);
                 System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
                 a.profile_picture.Image = img;
                 a.profile_picture.Image = ResizeImage(img, new Size(80, 80));*/

                a.Location = new Point(width + previousHorizontalPosition + SPACER, 6825);

                previousHorizontalPosition = a.Location.X;
                width = a.Width;

                homepage_panel.Controls.Add(a);
            }
        }


        private void listRecommendedVideos()
        {
            const int SPACER = 0;

            List<VideoBean> list = new List<VideoBean>();

            for (int i = 0; i < 6; i++)
            {
                VideoBean user = new VideoBean()
                {
                    videoTitle = "Csipkerózsika és a farkas",
                    videoLikeCount = 5641,
                    videoViewCount = 7458
                };
                list.Add(user);
            }

            int previousHorizontalPosition = 25;
            int width = 0;
            foreach (VideoBean video in list)
            {
                HomePageSingleVideoForm a = new HomePageSingleVideoForm(this, user, video, main);
                a.video_title.Text = video.videoTitle;
                a.view_count.Text = video.videoViewCount.ToString();
                a.like_count.Text = video.videoLikeCount.ToString();


                //Ide jön egy lekérdezés az ajánlott videókról, annak like és megtekintéseiről
                //meg a képét valahogy be kell neki adni
                /* WebClient wc = new WebClient();
                 byte[] bytes = wc.DownloadData(user.videoThumbnailImage.ToString());
                 MemoryStream ms = new MemoryStream(bytes);
                 System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
                 a.profile_picture.Image = img;
                 a.profile_picture.Image = ResizeImage(img, new Size(80, 80));*/

                a.Location = new Point(width + previousHorizontalPosition + SPACER, 6975);

                previousHorizontalPosition = a.Location.X;
                width = a.Width;

                homepage_panel.Controls.Add(a);
            }
        }


        private void listRecommendedUsers()
        {
            const int SPACER = 5;

            List<UserBean> list = new List<UserBean>();

            for (int i = 0; i < 13; i++)
            {
                UserBean user = new UserBean("Matthew", "gsfdgfdgfdg", "szabomati@gmail.com", "2012");
                list.Add(user);
            }

            int previousHorizontalPosition = 25;
            int width = 0;
            foreach (UserBean beans in list)
            {
                HomePageSingleUserForm a = new HomePageSingleUserForm();
                a.username.Text = beans.username;

                //Ide jön egy lekérdezés az ajánlott felhasználókról
                //meg a képét valahogy be kell neki adni
                /* WebClient wc = new WebClient();
                 byte[] bytes = wc.DownloadData(user.videoThumbnailImage.ToString());
                 MemoryStream ms = new MemoryStream(bytes);
                 System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
                 a.profile_picture.Image = img;
                 a.profile_picture.Image = ResizeImage(img, new Size(80, 80));*/

                a.Location = new Point(width + previousHorizontalPosition + SPACER, 7195);

                previousHorizontalPosition = a.Location.X;
                width = a.Width;

                homepage_panel.Controls.Add(a);
            }
        }


        private void listRecommendedVideosByLabel(UserBean user2)
        {
            Helper helper = new Helper();
            Database listazz = new Database();
            VideoBean videoBean = listazz.getSimilarVideoThatiLiked(user2);

            int previousHorizontalPosition = 25;
            int width = 0;
            

             HomePageSingleVideoForm a = new HomePageSingleVideoForm(this, user, videoBean, main);
             a.video_title.Text = videoBean.videoTitle;
             a.view_count.Text = videoBean.videoViewCount.ToString();
             a.like_count.Text = videoBean.videoLikeCount.ToString();
             a.video_thumbnail.Image = helper.resizeImageWithPreservedAspectRatio(helper.getThumbnailFromFTP(videoBean, "videoUploader", ""), 192, 108);
             a.Location = new Point(width + previousHorizontalPosition + SPACER, 1200);

             previousHorizontalPosition = a.Location.X;
             width = a.Width;

             homepage_panel.Controls.Add(a);
            
        }
    }
}
