﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace Video.View
{
    public partial class FavouritesPage : UserControl
    {
        public FavouritesPage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string oradb = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=4000)) (CONNECT_DATA = (SID = kabinet))); User Id = h662200; Password = h662200; ";

            using (OracleConnection conn = new OracleConnection(oradb)) {
                    conn.Open();

                #region Működő Beszúrás Skeleton
                /*
                 *MŰKÖDŐ BESZÚRÁS
                OracleCommand command = conn.CreateCommand();
                command.CommandText = "INSERT INTO CATEGORYTABLE (categoryId, categoryName) VALUES (82, 'Menomano')";
                command.ExecuteNonQuery();
                */
                #endregion
                #region Működő Lekérdezés Skeleton    
                /*
                     * MŰKÖDŐ LEKÉRDEZÉS
                    OracleCommand command = conn.CreateCommand();
                    command.CommandText = "SELECT * FROM CATEGORYTABLE";
                    OracleDataReader reader = command.ExecuteReader();
                    reader.Read();
                    OracleString oraclestring1 = reader.GetOracleString(1);
                    MessageBox.Show(oraclestring1.ToString());
                    */
                #endregion
                #region Működő paraméter nélküli Oracle Function Skeleton
                /*
                OracleCommand command = conn.CreateCommand();
                command.Connection = conn;

                command.CommandText = "getLastInsertedUserID";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("lastInsertedUserID", OracleDbType.Decimal);
                command.Parameters["lastInsertedUserID"].Direction = ParameterDirection.ReturnValue;

                command.ExecuteNonQuery();

                var returnedValue = Convert.ToString(command.Parameters["lastInsertedUserID"].Value);

                MessageBox.Show(returnedValue);
                command.Connection.Close();

                */
                #endregion
                #region Nem működő paraméteres Oracle Function Skeleton
                String searchablePerson = "Felipe Infante";
                OracleCommand command = conn.CreateCommand();

                command.Connection = conn;
                command.CommandText = "isUsernameAlreadyExists";
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("username", searchablePerson);
                command.Parameters.Add("cnt", OracleDbType.Decimal);
                command.Parameters["cnt"].Direction = ParameterDirection.ReturnValue;

                command.ExecuteNonQuery();

                String returnedValue = Convert.ToString(command.Parameters["cnt"].Value);

                MessageBox.Show(returnedValue);

                #endregion

                //label1.Text = oraclestring1.ToString();






            }


        }
    }
}
