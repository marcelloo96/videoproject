﻿namespace Video
{
    public partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.content_panel = new System.Windows.Forms.Panel();
            this.middleContentArea = new System.Windows.Forms.Panel();
            this.left_docked_panel = new System.Windows.Forms.Panel();
            this.selectorPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LogoutButton = new Bunifu.Framework.UI.BunifuImageButton();
            this.uploadVideoButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.myProfileButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.PlaylistButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.MyVideosButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.HomeButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.favouritesButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.search_panel = new System.Windows.Forms.Panel();
            this.TopPanelForSearchBar = new System.Windows.Forms.Panel();
            this.currentUserLabel = new System.Windows.Forms.Label();
            this.searchbox_background = new System.Windows.Forms.Panel();
            this.searchbox_text = new System.Windows.Forms.TextBox();
            this.searchbutton = new Bunifu.Framework.UI.BunifuImageButton();
            this.TopPanelForYTLogo = new System.Windows.Forms.Panel();
            this.youtube_logo_panel = new System.Windows.Forms.Panel();
            this.TopPanelForWindowButtons = new System.Windows.Forms.Panel();
            this.red_button = new Bunifu.Framework.UI.BunifuImageButton();
            this.green_button = new Bunifu.Framework.UI.BunifuImageButton();
            this.yellow_button = new Bunifu.Framework.UI.BunifuImageButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.DraggableSearchPanel = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.DraggableButtonPanel = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.DraggableYTPanel = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editVideoTestButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.content_panel.SuspendLayout();
            this.left_docked_panel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LogoutButton)).BeginInit();
            this.search_panel.SuspendLayout();
            this.TopPanelForSearchBar.SuspendLayout();
            this.searchbox_background.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchbutton)).BeginInit();
            this.TopPanelForYTLogo.SuspendLayout();
            this.TopPanelForWindowButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.red_button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.green_button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yellow_button)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.Location = new System.Drawing.Point(391, 182);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 100);
            this.panel2.TabIndex = 1;
            // 
            // content_panel
            // 
            this.content_panel.Controls.Add(this.middleContentArea);
            this.content_panel.Controls.Add(this.left_docked_panel);
            this.content_panel.Controls.Add(this.search_panel);
            this.content_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.content_panel.Location = new System.Drawing.Point(0, 0);
            this.content_panel.Name = "content_panel";
            this.content_panel.Size = new System.Drawing.Size(1440, 884);
            this.content_panel.TabIndex = 2;
            // 
            // middleContentArea
            // 
            this.middleContentArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.middleContentArea.Location = new System.Drawing.Point(200, 60);
            this.middleContentArea.Name = "middleContentArea";
            this.middleContentArea.Size = new System.Drawing.Size(1240, 824);
            this.middleContentArea.TabIndex = 2;
            // 
            // left_docked_panel
            // 
            this.left_docked_panel.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.left_docked_panel.Controls.Add(this.editVideoTestButton);
            this.left_docked_panel.Controls.Add(this.selectorPanel);
            this.left_docked_panel.Controls.Add(this.panel1);
            this.left_docked_panel.Controls.Add(this.uploadVideoButton);
            this.left_docked_panel.Controls.Add(this.myProfileButton);
            this.left_docked_panel.Controls.Add(this.PlaylistButton);
            this.left_docked_panel.Controls.Add(this.MyVideosButton);
            this.left_docked_panel.Controls.Add(this.HomeButton);
            this.left_docked_panel.Controls.Add(this.favouritesButton);
            this.left_docked_panel.Dock = System.Windows.Forms.DockStyle.Left;
            this.left_docked_panel.Location = new System.Drawing.Point(0, 60);
            this.left_docked_panel.Name = "left_docked_panel";
            this.left_docked_panel.Size = new System.Drawing.Size(200, 824);
            this.left_docked_panel.TabIndex = 1;
            // 
            // selectorPanel
            // 
            this.selectorPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.selectorPanel.Location = new System.Drawing.Point(0, 49);
            this.selectorPanel.Name = "selectorPanel";
            this.selectorPanel.Size = new System.Drawing.Size(20, 56);
            this.selectorPanel.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LogoutButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 724);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 1;
            // 
            // LogoutButton
            // 
            this.LogoutButton.BackColor = System.Drawing.Color.Transparent;
            this.LogoutButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.LogoutButton.Image = ((System.Drawing.Image)(resources.GetObject("LogoutButton.Image")));
            this.LogoutButton.ImageActive = null;
            this.LogoutButton.InitialImage = null;
            this.LogoutButton.Location = new System.Drawing.Point(75, 58);
            this.LogoutButton.Name = "LogoutButton";
            this.LogoutButton.Size = new System.Drawing.Size(46, 30);
            this.LogoutButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LogoutButton.TabIndex = 0;
            this.LogoutButton.TabStop = false;
            this.LogoutButton.Zoom = 10;
            this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // uploadVideoButton
            // 
            this.uploadVideoButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.uploadVideoButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.uploadVideoButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.uploadVideoButton.BorderRadius = 0;
            this.uploadVideoButton.ButtonText = "Upload Video";
            this.uploadVideoButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uploadVideoButton.DisabledColor = System.Drawing.Color.Gray;
            this.uploadVideoButton.Iconcolor = System.Drawing.Color.Transparent;
            this.uploadVideoButton.Iconimage = null;
            this.uploadVideoButton.Iconimage_right = null;
            this.uploadVideoButton.Iconimage_right_Selected = null;
            this.uploadVideoButton.Iconimage_Selected = null;
            this.uploadVideoButton.IconMarginLeft = 0;
            this.uploadVideoButton.IconMarginRight = 0;
            this.uploadVideoButton.IconRightVisible = true;
            this.uploadVideoButton.IconRightZoom = 0D;
            this.uploadVideoButton.IconVisible = true;
            this.uploadVideoButton.IconZoom = 90D;
            this.uploadVideoButton.IsTab = false;
            this.uploadVideoButton.Location = new System.Drawing.Point(0, 114);
            this.uploadVideoButton.Margin = new System.Windows.Forms.Padding(0);
            this.uploadVideoButton.Name = "uploadVideoButton";
            this.uploadVideoButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.uploadVideoButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.uploadVideoButton.OnHoverTextColor = System.Drawing.Color.White;
            this.uploadVideoButton.selected = false;
            this.uploadVideoButton.Size = new System.Drawing.Size(202, 57);
            this.uploadVideoButton.TabIndex = 0;
            this.uploadVideoButton.Text = "Upload Video";
            this.uploadVideoButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.uploadVideoButton.Textcolor = System.Drawing.Color.White;
            this.uploadVideoButton.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uploadVideoButton.Click += new System.EventHandler(this.uploadVideoButton_Click);
            // 
            // myProfileButton
            // 
            this.myProfileButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.myProfileButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.myProfileButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.myProfileButton.BorderRadius = 0;
            this.myProfileButton.ButtonText = "My Profile";
            this.myProfileButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.myProfileButton.DisabledColor = System.Drawing.Color.Gray;
            this.myProfileButton.Iconcolor = System.Drawing.Color.Transparent;
            this.myProfileButton.Iconimage = null;
            this.myProfileButton.Iconimage_right = null;
            this.myProfileButton.Iconimage_right_Selected = null;
            this.myProfileButton.Iconimage_Selected = null;
            this.myProfileButton.IconMarginLeft = 0;
            this.myProfileButton.IconMarginRight = 0;
            this.myProfileButton.IconRightVisible = true;
            this.myProfileButton.IconRightZoom = 0D;
            this.myProfileButton.IconVisible = true;
            this.myProfileButton.IconZoom = 90D;
            this.myProfileButton.IsTab = false;
            this.myProfileButton.Location = new System.Drawing.Point(0, 179);
            this.myProfileButton.Margin = new System.Windows.Forms.Padding(0);
            this.myProfileButton.Name = "myProfileButton";
            this.myProfileButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.myProfileButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.myProfileButton.OnHoverTextColor = System.Drawing.Color.White;
            this.myProfileButton.selected = false;
            this.myProfileButton.Size = new System.Drawing.Size(202, 57);
            this.myProfileButton.TabIndex = 0;
            this.myProfileButton.Text = "My Profile";
            this.myProfileButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.myProfileButton.Textcolor = System.Drawing.Color.White;
            this.myProfileButton.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.myProfileButton.Click += new System.EventHandler(this.myProfileButton_Click);
            // 
            // PlaylistButton
            // 
            this.PlaylistButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.PlaylistButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.PlaylistButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PlaylistButton.BorderRadius = 0;
            this.PlaylistButton.ButtonText = "Playlists";
            this.PlaylistButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PlaylistButton.DisabledColor = System.Drawing.Color.Gray;
            this.PlaylistButton.Iconcolor = System.Drawing.Color.Transparent;
            this.PlaylistButton.Iconimage = null;
            this.PlaylistButton.Iconimage_right = null;
            this.PlaylistButton.Iconimage_right_Selected = null;
            this.PlaylistButton.Iconimage_Selected = null;
            this.PlaylistButton.IconMarginLeft = 0;
            this.PlaylistButton.IconMarginRight = 0;
            this.PlaylistButton.IconRightVisible = true;
            this.PlaylistButton.IconRightZoom = 0D;
            this.PlaylistButton.IconVisible = true;
            this.PlaylistButton.IconZoom = 90D;
            this.PlaylistButton.IsTab = false;
            this.PlaylistButton.Location = new System.Drawing.Point(0, 309);
            this.PlaylistButton.Margin = new System.Windows.Forms.Padding(0);
            this.PlaylistButton.Name = "PlaylistButton";
            this.PlaylistButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.PlaylistButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.PlaylistButton.OnHoverTextColor = System.Drawing.Color.White;
            this.PlaylistButton.selected = false;
            this.PlaylistButton.Size = new System.Drawing.Size(202, 57);
            this.PlaylistButton.TabIndex = 0;
            this.PlaylistButton.Text = "Playlists";
            this.PlaylistButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PlaylistButton.Textcolor = System.Drawing.Color.White;
            this.PlaylistButton.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaylistButton.Click += new System.EventHandler(this.PlaylistButton_Click);
            // 
            // MyVideosButton
            // 
            this.MyVideosButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.MyVideosButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.MyVideosButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MyVideosButton.BorderRadius = 0;
            this.MyVideosButton.ButtonText = "My Videos";
            this.MyVideosButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MyVideosButton.DisabledColor = System.Drawing.Color.Gray;
            this.MyVideosButton.Iconcolor = System.Drawing.Color.Transparent;
            this.MyVideosButton.Iconimage = null;
            this.MyVideosButton.Iconimage_right = null;
            this.MyVideosButton.Iconimage_right_Selected = null;
            this.MyVideosButton.Iconimage_Selected = null;
            this.MyVideosButton.IconMarginLeft = 0;
            this.MyVideosButton.IconMarginRight = 0;
            this.MyVideosButton.IconRightVisible = true;
            this.MyVideosButton.IconRightZoom = 0D;
            this.MyVideosButton.IconVisible = true;
            this.MyVideosButton.IconZoom = 90D;
            this.MyVideosButton.IsTab = false;
            this.MyVideosButton.Location = new System.Drawing.Point(0, 244);
            this.MyVideosButton.Margin = new System.Windows.Forms.Padding(0);
            this.MyVideosButton.Name = "MyVideosButton";
            this.MyVideosButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.MyVideosButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.MyVideosButton.OnHoverTextColor = System.Drawing.Color.White;
            this.MyVideosButton.selected = false;
            this.MyVideosButton.Size = new System.Drawing.Size(202, 57);
            this.MyVideosButton.TabIndex = 0;
            this.MyVideosButton.Text = "My Videos";
            this.MyVideosButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.MyVideosButton.Textcolor = System.Drawing.Color.White;
            this.MyVideosButton.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MyVideosButton.Click += new System.EventHandler(this.MyVideosButton_Click);
            // 
            // HomeButton
            // 
            this.HomeButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.HomeButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.HomeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HomeButton.BorderRadius = 0;
            this.HomeButton.ButtonText = "Home";
            this.HomeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.HomeButton.DisabledColor = System.Drawing.Color.Gray;
            this.HomeButton.Iconcolor = System.Drawing.Color.Transparent;
            this.HomeButton.Iconimage = null;
            this.HomeButton.Iconimage_right = null;
            this.HomeButton.Iconimage_right_Selected = null;
            this.HomeButton.Iconimage_Selected = null;
            this.HomeButton.IconMarginLeft = 0;
            this.HomeButton.IconMarginRight = 0;
            this.HomeButton.IconRightVisible = true;
            this.HomeButton.IconRightZoom = 0D;
            this.HomeButton.IconVisible = true;
            this.HomeButton.IconZoom = 90D;
            this.HomeButton.IsTab = false;
            this.HomeButton.Location = new System.Drawing.Point(0, 49);
            this.HomeButton.Margin = new System.Windows.Forms.Padding(0);
            this.HomeButton.Name = "HomeButton";
            this.HomeButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.HomeButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.HomeButton.OnHoverTextColor = System.Drawing.Color.White;
            this.HomeButton.selected = false;
            this.HomeButton.Size = new System.Drawing.Size(202, 57);
            this.HomeButton.TabIndex = 0;
            this.HomeButton.Text = "Home";
            this.HomeButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.HomeButton.Textcolor = System.Drawing.Color.White;
            this.HomeButton.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HomeButton.Click += new System.EventHandler(this.HomeButton_Click);
            // 
            // favouritesButton
            // 
            this.favouritesButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.favouritesButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.favouritesButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.favouritesButton.BorderRadius = 0;
            this.favouritesButton.ButtonText = "Favourites";
            this.favouritesButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.favouritesButton.DisabledColor = System.Drawing.Color.Gray;
            this.favouritesButton.Iconcolor = System.Drawing.Color.Transparent;
            this.favouritesButton.Iconimage = null;
            this.favouritesButton.Iconimage_right = null;
            this.favouritesButton.Iconimage_right_Selected = null;
            this.favouritesButton.Iconimage_Selected = null;
            this.favouritesButton.IconMarginLeft = 0;
            this.favouritesButton.IconMarginRight = 0;
            this.favouritesButton.IconRightVisible = true;
            this.favouritesButton.IconRightZoom = 0D;
            this.favouritesButton.IconVisible = true;
            this.favouritesButton.IconZoom = 90D;
            this.favouritesButton.IsTab = false;
            this.favouritesButton.Location = new System.Drawing.Point(0, 379);
            this.favouritesButton.Margin = new System.Windows.Forms.Padding(0);
            this.favouritesButton.Name = "favouritesButton";
            this.favouritesButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.favouritesButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.favouritesButton.OnHoverTextColor = System.Drawing.Color.White;
            this.favouritesButton.selected = false;
            this.favouritesButton.Size = new System.Drawing.Size(202, 57);
            this.favouritesButton.TabIndex = 0;
            this.favouritesButton.Text = "Favourites";
            this.favouritesButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.favouritesButton.Textcolor = System.Drawing.Color.White;
            this.favouritesButton.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.favouritesButton.Click += new System.EventHandler(this.favouritesButton_Click);
            // 
            // search_panel
            // 
            this.search_panel.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.search_panel.Controls.Add(this.TopPanelForSearchBar);
            this.search_panel.Controls.Add(this.TopPanelForYTLogo);
            this.search_panel.Controls.Add(this.TopPanelForWindowButtons);
            this.search_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.search_panel.Location = new System.Drawing.Point(0, 0);
            this.search_panel.Name = "search_panel";
            this.search_panel.Size = new System.Drawing.Size(1440, 60);
            this.search_panel.TabIndex = 0;
            // 
            // TopPanelForSearchBar
            // 
            this.TopPanelForSearchBar.Controls.Add(this.currentUserLabel);
            this.TopPanelForSearchBar.Controls.Add(this.searchbox_background);
            this.TopPanelForSearchBar.Controls.Add(this.searchbutton);
            this.TopPanelForSearchBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TopPanelForSearchBar.Location = new System.Drawing.Point(197, 0);
            this.TopPanelForSearchBar.Name = "TopPanelForSearchBar";
            this.TopPanelForSearchBar.Size = new System.Drawing.Size(1122, 60);
            this.TopPanelForSearchBar.TabIndex = 5;
            this.TopPanelForSearchBar.Paint += new System.Windows.Forms.PaintEventHandler(this.TopPanelForSearchBar_Paint);
            this.TopPanelForSearchBar.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TopPanelForSearchBar_MouseDoubleClick);
            // 
            // currentUserLabel
            // 
            this.currentUserLabel.AutoSize = true;
            this.currentUserLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.currentUserLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.currentUserLabel.Location = new System.Drawing.Point(990, 22);
            this.currentUserLabel.Name = "currentUserLabel";
            this.currentUserLabel.Size = new System.Drawing.Size(55, 21);
            this.currentUserLabel.TabIndex = 3;
            this.currentUserLabel.Text = "Visitor";
            // 
            // searchbox_background
            // 
            this.searchbox_background.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchbox_background.BackgroundImage")));
            this.searchbox_background.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.searchbox_background.Controls.Add(this.searchbox_text);
            this.searchbox_background.Location = new System.Drawing.Point(4, 16);
            this.searchbox_background.Name = "searchbox_background";
            this.searchbox_background.Size = new System.Drawing.Size(317, 27);
            this.searchbox_background.TabIndex = 2;
            this.searchbox_background.Paint += new System.Windows.Forms.PaintEventHandler(this.searchbox_background_Paint);
            // 
            // searchbox_text
            // 
            this.searchbox_text.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.searchbox_text.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchbox_text.Location = new System.Drawing.Point(12, 6);
            this.searchbox_text.Name = "searchbox_text";
            this.searchbox_text.Size = new System.Drawing.Size(300, 16);
            this.searchbox_text.TabIndex = 2;
            this.searchbox_text.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // searchbutton
            // 
            this.searchbutton.BackColor = System.Drawing.Color.Transparent;
            this.searchbutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchbutton.BackgroundImage")));
            this.searchbutton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.searchbutton.Image = ((System.Drawing.Image)(resources.GetObject("searchbutton.Image")));
            this.searchbutton.ImageActive = null;
            this.searchbutton.InitialImage = null;
            this.searchbutton.Location = new System.Drawing.Point(325, 16);
            this.searchbutton.Name = "searchbutton";
            this.searchbutton.Size = new System.Drawing.Size(29, 29);
            this.searchbutton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.searchbutton.TabIndex = 0;
            this.searchbutton.TabStop = false;
            this.searchbutton.Zoom = 10;
            this.searchbutton.Click += new System.EventHandler(this.searchbutton_Click);
            // 
            // TopPanelForYTLogo
            // 
            this.TopPanelForYTLogo.Controls.Add(this.youtube_logo_panel);
            this.TopPanelForYTLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.TopPanelForYTLogo.Location = new System.Drawing.Point(0, 0);
            this.TopPanelForYTLogo.Name = "TopPanelForYTLogo";
            this.TopPanelForYTLogo.Size = new System.Drawing.Size(197, 60);
            this.TopPanelForYTLogo.TabIndex = 4;
            this.TopPanelForYTLogo.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TopPanelForYTLogo_MouseDoubleClick);
            // 
            // youtube_logo_panel
            // 
            this.youtube_logo_panel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("youtube_logo_panel.BackgroundImage")));
            this.youtube_logo_panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.youtube_logo_panel.Location = new System.Drawing.Point(36, 12);
            this.youtube_logo_panel.Name = "youtube_logo_panel";
            this.youtube_logo_panel.Size = new System.Drawing.Size(101, 43);
            this.youtube_logo_panel.TabIndex = 1;
            // 
            // TopPanelForWindowButtons
            // 
            this.TopPanelForWindowButtons.Controls.Add(this.red_button);
            this.TopPanelForWindowButtons.Controls.Add(this.green_button);
            this.TopPanelForWindowButtons.Controls.Add(this.yellow_button);
            this.TopPanelForWindowButtons.Dock = System.Windows.Forms.DockStyle.Right;
            this.TopPanelForWindowButtons.Location = new System.Drawing.Point(1319, 0);
            this.TopPanelForWindowButtons.Name = "TopPanelForWindowButtons";
            this.TopPanelForWindowButtons.Size = new System.Drawing.Size(121, 60);
            this.TopPanelForWindowButtons.TabIndex = 3;
            this.TopPanelForWindowButtons.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TopPanelForWindowButtons_MouseDoubleClick);
            // 
            // red_button
            // 
            this.red_button.BackColor = System.Drawing.Color.Transparent;
            this.red_button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("red_button.BackgroundImage")));
            this.red_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.red_button.Image = ((System.Drawing.Image)(resources.GetObject("red_button.Image")));
            this.red_button.ImageActive = null;
            this.red_button.InitialImage = null;
            this.red_button.Location = new System.Drawing.Point(79, 23);
            this.red_button.Name = "red_button";
            this.red_button.Size = new System.Drawing.Size(20, 20);
            this.red_button.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.red_button.TabIndex = 0;
            this.red_button.TabStop = false;
            this.red_button.Zoom = 10;
            this.red_button.Click += new System.EventHandler(this.red_button_Click);
            // 
            // green_button
            // 
            this.green_button.BackColor = System.Drawing.Color.Transparent;
            this.green_button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("green_button.BackgroundImage")));
            this.green_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.green_button.Image = ((System.Drawing.Image)(resources.GetObject("green_button.Image")));
            this.green_button.ImageActive = null;
            this.green_button.InitialImage = null;
            this.green_button.Location = new System.Drawing.Point(27, 23);
            this.green_button.Name = "green_button";
            this.green_button.Size = new System.Drawing.Size(20, 20);
            this.green_button.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.green_button.TabIndex = 0;
            this.green_button.TabStop = false;
            this.green_button.Zoom = 10;
            this.green_button.Click += new System.EventHandler(this.green_button_Click);
            // 
            // yellow_button
            // 
            this.yellow_button.BackColor = System.Drawing.Color.Transparent;
            this.yellow_button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("yellow_button.BackgroundImage")));
            this.yellow_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.yellow_button.Image = ((System.Drawing.Image)(resources.GetObject("yellow_button.Image")));
            this.yellow_button.ImageActive = null;
            this.yellow_button.InitialImage = null;
            this.yellow_button.Location = new System.Drawing.Point(53, 23);
            this.yellow_button.Name = "yellow_button";
            this.yellow_button.Size = new System.Drawing.Size(20, 20);
            this.yellow_button.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.yellow_button.TabIndex = 0;
            this.yellow_button.TabStop = false;
            this.yellow_button.Zoom = 10;
            this.yellow_button.Click += new System.EventHandler(this.yellow_button_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // DraggableSearchPanel
            // 
            this.DraggableSearchPanel.Fixed = true;
            this.DraggableSearchPanel.Horizontal = true;
            this.DraggableSearchPanel.TargetControl = this.TopPanelForSearchBar;
            this.DraggableSearchPanel.Vertical = true;
            // 
            // DraggableButtonPanel
            // 
            this.DraggableButtonPanel.Fixed = true;
            this.DraggableButtonPanel.Horizontal = true;
            this.DraggableButtonPanel.TargetControl = this.TopPanelForWindowButtons;
            this.DraggableButtonPanel.Vertical = true;
            // 
            // DraggableYTPanel
            // 
            this.DraggableYTPanel.Fixed = true;
            this.DraggableYTPanel.Horizontal = true;
            this.DraggableYTPanel.TargetControl = this.TopPanelForYTLogo;
            this.DraggableYTPanel.Vertical = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // editVideoTestButton
            // 
            this.editVideoTestButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.editVideoTestButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.editVideoTestButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.editVideoTestButton.BorderRadius = 0;
            this.editVideoTestButton.ButtonText = "TESTEDITVIDEO";
            this.editVideoTestButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.editVideoTestButton.DisabledColor = System.Drawing.Color.Gray;
            this.editVideoTestButton.Iconcolor = System.Drawing.Color.Transparent;
            this.editVideoTestButton.Iconimage = null;
            this.editVideoTestButton.Iconimage_right = null;
            this.editVideoTestButton.Iconimage_right_Selected = null;
            this.editVideoTestButton.Iconimage_Selected = null;
            this.editVideoTestButton.IconMarginLeft = 0;
            this.editVideoTestButton.IconMarginRight = 0;
            this.editVideoTestButton.IconRightVisible = true;
            this.editVideoTestButton.IconRightZoom = 0D;
            this.editVideoTestButton.IconVisible = true;
            this.editVideoTestButton.IconZoom = 90D;
            this.editVideoTestButton.IsTab = false;
            this.editVideoTestButton.Location = new System.Drawing.Point(0, 582);
            this.editVideoTestButton.Margin = new System.Windows.Forms.Padding(0);
            this.editVideoTestButton.Name = "editVideoTestButton";
            this.editVideoTestButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(43)))));
            this.editVideoTestButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.editVideoTestButton.OnHoverTextColor = System.Drawing.Color.White;
            this.editVideoTestButton.selected = false;
            this.editVideoTestButton.Size = new System.Drawing.Size(202, 57);
            this.editVideoTestButton.TabIndex = 3;
            this.editVideoTestButton.Text = "TESTEDITVIDEO";
            this.editVideoTestButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.editVideoTestButton.Textcolor = System.Drawing.Color.White;
            this.editVideoTestButton.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editVideoTestButton.Click += new System.EventHandler(this.editVideoTestButton_Click);
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1440, 884);
            this.Controls.Add(this.content_panel);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainPage";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainPage_FormClosing);
            this.content_panel.ResumeLayout(false);
            this.left_docked_panel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LogoutButton)).EndInit();
            this.search_panel.ResumeLayout(false);
            this.TopPanelForSearchBar.ResumeLayout(false);
            this.TopPanelForSearchBar.PerformLayout();
            this.searchbox_background.ResumeLayout(false);
            this.searchbox_background.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchbutton)).EndInit();
            this.TopPanelForYTLogo.ResumeLayout(false);
            this.TopPanelForWindowButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.red_button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.green_button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yellow_button)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel content_panel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel search_panel;
        private Bunifu.Framework.UI.BunifuImageButton searchbutton;
        private Bunifu.Framework.UI.BunifuImageButton green_button;
        private Bunifu.Framework.UI.BunifuImageButton yellow_button;
        private Bunifu.Framework.UI.BunifuImageButton red_button;
        private System.Windows.Forms.ImageList imageList1;
        private Bunifu.Framework.UI.BunifuDragControl DraggableSearchPanel;
        private System.Windows.Forms.Panel left_docked_panel;
        private System.Windows.Forms.Panel youtube_logo_panel;
        private Bunifu.Framework.UI.BunifuFlatButton PlaylistButton;
        private Bunifu.Framework.UI.BunifuFlatButton MyVideosButton;
        private Bunifu.Framework.UI.BunifuFlatButton HomeButton;
        private Bunifu.Framework.UI.BunifuImageButton LogoutButton;
        private System.Windows.Forms.Panel searchbox_background;
        private System.Windows.Forms.TextBox searchbox_text;
        private System.Windows.Forms.Panel TopPanelForWindowButtons;
        private System.Windows.Forms.Panel TopPanelForYTLogo;
        private System.Windows.Forms.Panel TopPanelForSearchBar;
        private Bunifu.Framework.UI.BunifuDragControl DraggableButtonPanel;
        private Bunifu.Framework.UI.BunifuDragControl DraggableYTPanel;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private Bunifu.Framework.UI.BunifuFlatButton favouritesButton;
        private Bunifu.Framework.UI.BunifuFlatButton myProfileButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel selectorPanel;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private Bunifu.Framework.UI.BunifuFlatButton uploadVideoButton;
        public System.Windows.Forms.Panel middleContentArea;
        private System.Windows.Forms.Label currentUserLabel;
        private Bunifu.Framework.UI.BunifuFlatButton editVideoTestButton;
    }
}

