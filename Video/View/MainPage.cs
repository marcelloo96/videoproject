﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.Dao;
using Video.Model;
using Video.View;
using Video.View.MainPageSubPages;
using VideoPlayerProject;

namespace Video
{
    public partial class MainPage : Form
    {
        private UserBean user;
        

        public MainPage(UserBean bean) {
            this.user = bean;
            InitializeComponent();
            selectorPanel.Height = myProfileButton.Height;

            if (bean.userid == 0)
            {
                /*The user bean is not logged in*/
                myProfileButton.Visible = false;
                PlaylistButton.Visible = false;
                favouritesButton.Visible = false;
                MyVideosButton.Visible = false;
                uploadVideoButton.Visible = false;
            }
            else {
                currentUserLabel.Text = bean.username;
            }

            selectorPanel.Visible = true;
            selectorPanel.Height = HomeButton.Height;
            selectorPanel.Top = HomeButton.Top;
            HomePage home = new HomePage(user, this);
            home.FormBorderStyle = FormBorderStyle.None;
            home.TopLevel = false;
            home.AutoScroll = true;
            middleContentArea.Controls.Add(home);
            home.Show();
            home.BringToFront();
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void red_button_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void searchbox_background_Paint(object sender, PaintEventArgs e)
        {

        }

        private void searchbutton_Click(object sender, EventArgs e)
        {
            selectorPanel.Visible = false;
            Database dao = new Database();
            SearchResults sresult = new SearchResults(dao.getVideosBySearch(searchbox_text.Text), user, this);
            sresult.FormBorderStyle = FormBorderStyle.None;
            sresult.TopLevel = false;
            middleContentArea.Controls.Add(sresult);
            sresult.Show();
            sresult.BringToFront();
        }

        private void yellow_button_Click(object sender, EventArgs e)
        {
            
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void green_button_Click(object sender, EventArgs e)
        {

            this.WindowState = FormWindowState.Minimized;
        }

        private void TopPanelForSearchBar_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void TopPanelForWindowButtons_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void TopPanelForYTLogo_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void HomeButton_Click(object sender, EventArgs e)
        {
            selectorPanel.Visible = true;
            selectorPanel.Height = HomeButton.Height;
            selectorPanel.Top = HomeButton.Top;
            HomePage home = new HomePage(user, this);
            home.FormBorderStyle = FormBorderStyle.None;
            home.TopLevel = false;
            home.AutoScroll = true;
            middleContentArea.Controls.Add(home);
            home.Show();
            home.BringToFront();
        }

        private void myProfileButton_Click(object sender, EventArgs e)
        {
            selectorPanel.Visible = true;
            selectorPanel.Height = myProfileButton.Height;
            selectorPanel.Top = myProfileButton.Top;
            UserProfile profile = new UserProfile(user);
            profile.FormBorderStyle = FormBorderStyle.None;
            profile.TopLevel = false;
            middleContentArea.Controls.Add(profile);
            profile.Show();
            profile.AutoScroll = true;
            profile.BringToFront();
        }

        private void favouritesButton_Click(object sender, EventArgs e)
        {
            selectorPanel.Visible = true;
            selectorPanel.Height = favouritesButton.Height;
            selectorPanel.Top = favouritesButton.Top;
            //favouritesPage.BringToFront();
        }

        private void MyVideosButton_Click(object sender, EventArgs e)
        {
            closeFormsExceptThis("MainPage");
            middleContentArea.Controls.Clear();
            MyVideos myvideos = new MyVideos(this, user);
            myvideos.FormBorderStyle = FormBorderStyle.None;
            myvideos.TopLevel = false;
            myvideos.AutoScroll = true;
            middleContentArea.Controls.Add(myvideos);
            myvideos.Show();
            /* VideoPlayer videoPlayer = new VideoPlayer(videoBean, user);
             videoPlayer.FormBorderStyle = FormBorderStyle.None;
             videoPlayer.TopLevel = false;
             videoPlayer.AutoScroll = true;
             middleContentArea.Controls.Add(videoPlayer);
             videoPlayer.Show();*/

        }

        private void PlaylistButton_Click(object sender, EventArgs e)
        {
            closeFormsExceptThis("MainPage");
            middleContentArea.Controls.Clear();
            Playlists playlists = new Playlists(this, user);
            playlists.FormBorderStyle = FormBorderStyle.None;
            playlists.TopLevel = false;
            playlists.AutoScroll = true;
            selectorPanel.Height = PlaylistButton.Height;
            selectorPanel.Top = PlaylistButton.Top;
            middleContentArea.Controls.Add(playlists);
            playlists.Show();
        }

        private void uploadVideoButton_Click(object sender, EventArgs e)
        {
            closeFormsExceptThis("MainPage");
            middleContentArea.Controls.Clear();
            VideoUploader videoUploader = new VideoUploader(user);
            videoUploader.FormBorderStyle = FormBorderStyle.None;
            videoUploader.TopLevel = false;
            videoUploader.AutoScroll = true;
            middleContentArea.Controls.Add(videoUploader);
            videoUploader.Show();
        }

        private void asd_Load(object sender, EventArgs e)
        {

        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            Form ident = new IdentificationFrame();
            this.Dispose();
            /*///*/
            ident.ShowDialog();
        }

        private void TopPanelForSearchBar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void videoPanel_Load(object sender, EventArgs e)
        {

        }

        private void closeFormsExceptThis(string formName)
        {
            for (int i = Application.OpenForms.Count - 1; i >= 0; i--)
            {
                if (Application.OpenForms[i].Name != formName)
                {
                    Application.OpenForms[i].Close();
                }
            }
        }

        private void MainPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            closeFormsExceptThis("MainPage");
        }

        private void editVideoTestButton_Click(object sender, EventArgs e)
        {
            VideoBean video = new VideoBean()
            {
                videoId = 4,
                videoTitle = "Idegesítő into",
                videoDescription = "fukoff",
                videoUploadDate = DateTime.ParseExact("2018. 05. 15. 21:06:05", "yyyy. MM. dd. HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                videoViewCount = 0,
                videoLikeCount = 0,
                isPublic = 1,
                isChildrenSafe = 1,
                areCommentsEnabled = 0,
                videoThumbnailImageURL = new Uri("ftp://127.0.0.1/thumbnail4.png"),
                videoURL = new Uri("ftp://127.0.0.1/video4.mkv"),
                userID = 3
            };

            closeFormsExceptThis("MainPage");
            middleContentArea.Controls.Clear();
            VideoEditor videoEditor = new VideoEditor(video);
            videoEditor.FormBorderStyle = FormBorderStyle.None;
            videoEditor.TopLevel = false;
            videoEditor.AutoScroll = true;
            middleContentArea.Controls.Add(videoEditor);
            videoEditor.Show();
        }
    }
}
