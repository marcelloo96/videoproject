﻿namespace Video.View
{
    partial class LoginPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.passwordTextBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.usernameTextBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.registerStaticLabel = new System.Windows.Forms.Label();
            this.passwordStaticLabel = new System.Windows.Forms.Label();
            this.usernameStaticLabel = new System.Windows.Forms.Label();
            this.loginLabel = new System.Windows.Forms.Label();
            this.registerLinkLabel = new System.Windows.Forms.LinkLabel();
            this.visitorButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.SuspendLayout();
            // 
            // loginButton
            // 
            this.loginButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.loginButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.loginButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.loginButton.BorderRadius = 0;
            this.loginButton.ButtonText = "Login";
            this.loginButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.loginButton.DisabledColor = System.Drawing.Color.Gray;
            this.loginButton.Iconcolor = System.Drawing.Color.Transparent;
            this.loginButton.Iconimage = null;
            this.loginButton.Iconimage_right = null;
            this.loginButton.Iconimage_right_Selected = null;
            this.loginButton.Iconimage_Selected = null;
            this.loginButton.IconMarginLeft = 0;
            this.loginButton.IconMarginRight = 0;
            this.loginButton.IconRightVisible = true;
            this.loginButton.IconRightZoom = 0D;
            this.loginButton.IconVisible = true;
            this.loginButton.IconZoom = 90D;
            this.loginButton.IsTab = false;
            this.loginButton.Location = new System.Drawing.Point(49, 363);
            this.loginButton.Name = "loginButton";
            this.loginButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.loginButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.loginButton.OnHoverTextColor = System.Drawing.Color.White;
            this.loginButton.selected = true;
            this.loginButton.Size = new System.Drawing.Size(182, 48);
            this.loginButton.TabIndex = 2;
            this.loginButton.Text = "Login";
            this.loginButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.loginButton.Textcolor = System.Drawing.Color.Black;
            this.loginButton.TextFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.passwordTextBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.passwordTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.passwordTextBox.HintForeColor = System.Drawing.Color.Empty;
            this.passwordTextBox.HintText = "";
            this.passwordTextBox.isPassword = true;
            this.passwordTextBox.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.passwordTextBox.LineIdleColor = System.Drawing.Color.Gray;
            this.passwordTextBox.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.passwordTextBox.LineThickness = 3;
            this.passwordTextBox.Location = new System.Drawing.Point(236, 275);
            this.passwordTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(230, 27);
            this.passwordTextBox.TabIndex = 1;
            this.passwordTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.usernameTextBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.usernameTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.usernameTextBox.HintForeColor = System.Drawing.Color.Empty;
            this.usernameTextBox.HintText = "";
            this.usernameTextBox.isPassword = false;
            this.usernameTextBox.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.usernameTextBox.LineIdleColor = System.Drawing.Color.Gray;
            this.usernameTextBox.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.usernameTextBox.LineThickness = 3;
            this.usernameTextBox.Location = new System.Drawing.Point(236, 163);
            this.usernameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(230, 27);
            this.usernameTextBox.TabIndex = 0;
            this.usernameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // registerStaticLabel
            // 
            this.registerStaticLabel.AutoSize = true;
            this.registerStaticLabel.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.registerStaticLabel.Location = new System.Drawing.Point(48, 517);
            this.registerStaticLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.registerStaticLabel.Name = "registerStaticLabel";
            this.registerStaticLabel.Size = new System.Drawing.Size(368, 25);
            this.registerStaticLabel.TabIndex = 9;
            this.registerStaticLabel.Text = "Don\'t have an account? Register ";
            // 
            // passwordStaticLabel
            // 
            this.passwordStaticLabel.AutoSize = true;
            this.passwordStaticLabel.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.passwordStaticLabel.Location = new System.Drawing.Point(44, 275);
            this.passwordStaticLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.passwordStaticLabel.Name = "passwordStaticLabel";
            this.passwordStaticLabel.Size = new System.Drawing.Size(122, 25);
            this.passwordStaticLabel.TabIndex = 10;
            this.passwordStaticLabel.Text = "password:";
            // 
            // usernameStaticLabel
            // 
            this.usernameStaticLabel.AutoSize = true;
            this.usernameStaticLabel.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usernameStaticLabel.Location = new System.Drawing.Point(44, 163);
            this.usernameStaticLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.usernameStaticLabel.Name = "usernameStaticLabel";
            this.usernameStaticLabel.Size = new System.Drawing.Size(125, 25);
            this.usernameStaticLabel.TabIndex = 11;
            this.usernameStaticLabel.Text = "username:";
            // 
            // loginLabel
            // 
            this.loginLabel.AutoSize = true;
            this.loginLabel.Font = new System.Drawing.Font("Century Gothic", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loginLabel.Location = new System.Drawing.Point(44, 37);
            this.loginLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.loginLabel.Name = "loginLabel";
            this.loginLabel.Size = new System.Drawing.Size(92, 32);
            this.loginLabel.TabIndex = 8;
            this.loginLabel.Text = "Log in";
            // 
            // registerLinkLabel
            // 
            this.registerLinkLabel.AutoSize = true;
            this.registerLinkLabel.Font = new System.Drawing.Font("Century Gothic", 16.2F);
            this.registerLinkLabel.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.registerLinkLabel.Location = new System.Drawing.Point(395, 517);
            this.registerLinkLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.registerLinkLabel.Name = "registerLinkLabel";
            this.registerLinkLabel.Size = new System.Drawing.Size(66, 25);
            this.registerLinkLabel.TabIndex = 4;
            this.registerLinkLabel.TabStop = true;
            this.registerLinkLabel.Text = "here!";
            this.registerLinkLabel.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.registerLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.registerLinkLabel_LinkClicked);
            // 
            // visitorButton
            // 
            this.visitorButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.visitorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.visitorButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.visitorButton.BorderRadius = 0;
            this.visitorButton.ButtonText = "I am a visitor";
            this.visitorButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.visitorButton.DisabledColor = System.Drawing.Color.Gray;
            this.visitorButton.Iconcolor = System.Drawing.Color.Transparent;
            this.visitorButton.Iconimage = null;
            this.visitorButton.Iconimage_right = null;
            this.visitorButton.Iconimage_right_Selected = null;
            this.visitorButton.Iconimage_Selected = null;
            this.visitorButton.IconMarginLeft = 0;
            this.visitorButton.IconMarginRight = 0;
            this.visitorButton.IconRightVisible = true;
            this.visitorButton.IconRightZoom = 0D;
            this.visitorButton.IconVisible = true;
            this.visitorButton.IconZoom = 90D;
            this.visitorButton.IsTab = false;
            this.visitorButton.Location = new System.Drawing.Point(284, 363);
            this.visitorButton.Name = "visitorButton";
            this.visitorButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.visitorButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.visitorButton.OnHoverTextColor = System.Drawing.Color.White;
            this.visitorButton.selected = false;
            this.visitorButton.Size = new System.Drawing.Size(182, 48);
            this.visitorButton.TabIndex = 3;
            this.visitorButton.Text = "I am a visitor";
            this.visitorButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.visitorButton.Textcolor = System.Drawing.Color.Black;
            this.visitorButton.TextFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.visitorButton.Click += new System.EventHandler(this.visitorButton_Click);
            // 
            // LoginPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.registerLinkLabel);
            this.Controls.Add(this.visitorButton);
            this.Controls.Add(this.loginButton);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(this.registerStaticLabel);
            this.Controls.Add(this.passwordStaticLabel);
            this.Controls.Add(this.usernameStaticLabel);
            this.Controls.Add(this.loginLabel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "LoginPage";
            this.Size = new System.Drawing.Size(690, 756);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuFlatButton loginButton;
        private Bunifu.Framework.UI.BunifuMaterialTextbox passwordTextBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox usernameTextBox;
        private System.Windows.Forms.Label registerStaticLabel;
        private System.Windows.Forms.Label passwordStaticLabel;
        private System.Windows.Forms.Label usernameStaticLabel;
        private System.Windows.Forms.Label loginLabel;
        private System.Windows.Forms.LinkLabel registerLinkLabel;
        private Bunifu.Framework.UI.BunifuFlatButton visitorButton;
    }
}
