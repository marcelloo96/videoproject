﻿namespace Video.View.IdentificationSubPages
{
    partial class RegistrationPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegistrationPage));
            this.birthDatePicker = new Bunifu.Framework.UI.BunifuDatepicker();
            this.registerButton = new Bunifu.Framework.UI.BunifuFlatButton();
            this.repasswordTextBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.passwordTextBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.emailTextBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.usernameTextBox = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.birthDateStaticLabel = new System.Windows.Forms.Label();
            this.repasswordStaticLabel = new System.Windows.Forms.Label();
            this.passwordStaticLabel = new System.Windows.Forms.Label();
            this.emailStaticLabel = new System.Windows.Forms.Label();
            this.usernameStaticLabel = new System.Windows.Forms.Label();
            this.RegistrationLabel = new System.Windows.Forms.Label();
            this.back_button = new Bunifu.Framework.UI.BunifuImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.back_button)).BeginInit();
            this.SuspendLayout();
            // 
            // birthDatePicker
            // 
            this.birthDatePicker.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.birthDatePicker.BorderRadius = 0;
            this.birthDatePicker.ForeColor = System.Drawing.Color.Black;
            this.birthDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.birthDatePicker.FormatCustom = null;
            this.birthDatePicker.Location = new System.Drawing.Point(236, 452);
            this.birthDatePicker.Name = "birthDatePicker";
            this.birthDatePicker.Size = new System.Drawing.Size(230, 36);
            this.birthDatePicker.TabIndex = 4;
            this.birthDatePicker.Value = new System.DateTime(2018, 5, 4, 17, 40, 20, 980);
            // 
            // registerButton
            // 
            this.registerButton.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.registerButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.registerButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.registerButton.BorderRadius = 0;
            this.registerButton.ButtonText = "Registration";
            this.registerButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.registerButton.DisabledColor = System.Drawing.Color.Gray;
            this.registerButton.Iconcolor = System.Drawing.Color.Transparent;
            this.registerButton.Iconimage = null;
            this.registerButton.Iconimage_right = null;
            this.registerButton.Iconimage_right_Selected = null;
            this.registerButton.Iconimage_Selected = null;
            this.registerButton.IconMarginLeft = 0;
            this.registerButton.IconMarginRight = 0;
            this.registerButton.IconRightVisible = true;
            this.registerButton.IconRightZoom = 0D;
            this.registerButton.IconVisible = true;
            this.registerButton.IconZoom = 90D;
            this.registerButton.IsTab = false;
            this.registerButton.Location = new System.Drawing.Point(51, 640);
            this.registerButton.Name = "registerButton";
            this.registerButton.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.registerButton.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.registerButton.OnHoverTextColor = System.Drawing.Color.White;
            this.registerButton.selected = false;
            this.registerButton.Size = new System.Drawing.Size(182, 48);
            this.registerButton.TabIndex = 5;
            this.registerButton.Text = "Registration";
            this.registerButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.registerButton.Textcolor = System.Drawing.Color.Black;
            this.registerButton.TextFont = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.registerButton.Click += new System.EventHandler(this.registerButton_Click);
            // 
            // repasswordTextBox
            // 
            this.repasswordTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.repasswordTextBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.repasswordTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.repasswordTextBox.HintForeColor = System.Drawing.Color.Empty;
            this.repasswordTextBox.HintText = "";
            this.repasswordTextBox.isPassword = true;
            this.repasswordTextBox.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.repasswordTextBox.LineIdleColor = System.Drawing.Color.Gray;
            this.repasswordTextBox.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.repasswordTextBox.LineThickness = 3;
            this.repasswordTextBox.Location = new System.Drawing.Point(236, 362);
            this.repasswordTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.repasswordTextBox.Name = "repasswordTextBox";
            this.repasswordTextBox.Size = new System.Drawing.Size(230, 27);
            this.repasswordTextBox.TabIndex = 3;
            this.repasswordTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.passwordTextBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.passwordTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.passwordTextBox.HintForeColor = System.Drawing.Color.Empty;
            this.passwordTextBox.HintText = "";
            this.passwordTextBox.isPassword = true;
            this.passwordTextBox.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.passwordTextBox.LineIdleColor = System.Drawing.Color.Gray;
            this.passwordTextBox.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.passwordTextBox.LineThickness = 3;
            this.passwordTextBox.Location = new System.Drawing.Point(236, 293);
            this.passwordTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(230, 27);
            this.passwordTextBox.TabIndex = 2;
            this.passwordTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // emailTextBox
            // 
            this.emailTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.emailTextBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.emailTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.emailTextBox.HintForeColor = System.Drawing.Color.Empty;
            this.emailTextBox.HintText = "";
            this.emailTextBox.isPassword = false;
            this.emailTextBox.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.emailTextBox.LineIdleColor = System.Drawing.Color.Gray;
            this.emailTextBox.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.emailTextBox.LineThickness = 3;
            this.emailTextBox.Location = new System.Drawing.Point(236, 219);
            this.emailTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(230, 27);
            this.emailTextBox.TabIndex = 1;
            this.emailTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.usernameTextBox.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.usernameTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.usernameTextBox.HintForeColor = System.Drawing.Color.Empty;
            this.usernameTextBox.HintText = "";
            this.usernameTextBox.isPassword = false;
            this.usernameTextBox.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.usernameTextBox.LineIdleColor = System.Drawing.Color.Gray;
            this.usernameTextBox.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.usernameTextBox.LineThickness = 3;
            this.usernameTextBox.Location = new System.Drawing.Point(236, 156);
            this.usernameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(230, 27);
            this.usernameTextBox.TabIndex = 0;
            this.usernameTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // birthDateStaticLabel
            // 
            this.birthDateStaticLabel.AutoSize = true;
            this.birthDateStaticLabel.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.birthDateStaticLabel.Location = new System.Drawing.Point(40, 452);
            this.birthDateStaticLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.birthDateStaticLabel.Name = "birthDateStaticLabel";
            this.birthDateStaticLabel.Size = new System.Drawing.Size(115, 25);
            this.birthDateStaticLabel.TabIndex = 25;
            this.birthDateStaticLabel.Text = "Birthdate:";
            // 
            // repasswordStaticLabel
            // 
            this.repasswordStaticLabel.AutoSize = true;
            this.repasswordStaticLabel.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.repasswordStaticLabel.Location = new System.Drawing.Point(40, 362);
            this.repasswordStaticLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.repasswordStaticLabel.Name = "repasswordStaticLabel";
            this.repasswordStaticLabel.Size = new System.Drawing.Size(190, 25);
            this.repasswordStaticLabel.TabIndex = 26;
            this.repasswordStaticLabel.Text = "password again:";
            // 
            // passwordStaticLabel
            // 
            this.passwordStaticLabel.AutoSize = true;
            this.passwordStaticLabel.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.passwordStaticLabel.Location = new System.Drawing.Point(40, 293);
            this.passwordStaticLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.passwordStaticLabel.Name = "passwordStaticLabel";
            this.passwordStaticLabel.Size = new System.Drawing.Size(122, 25);
            this.passwordStaticLabel.TabIndex = 27;
            this.passwordStaticLabel.Text = "password:";
            // 
            // emailStaticLabel
            // 
            this.emailStaticLabel.AutoSize = true;
            this.emailStaticLabel.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.emailStaticLabel.Location = new System.Drawing.Point(40, 219);
            this.emailStaticLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.emailStaticLabel.Name = "emailStaticLabel";
            this.emailStaticLabel.Size = new System.Drawing.Size(79, 25);
            this.emailStaticLabel.TabIndex = 28;
            this.emailStaticLabel.Text = "email:";
            // 
            // usernameStaticLabel
            // 
            this.usernameStaticLabel.AutoSize = true;
            this.usernameStaticLabel.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.usernameStaticLabel.Location = new System.Drawing.Point(40, 156);
            this.usernameStaticLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.usernameStaticLabel.Name = "usernameStaticLabel";
            this.usernameStaticLabel.Size = new System.Drawing.Size(125, 25);
            this.usernameStaticLabel.TabIndex = 29;
            this.usernameStaticLabel.Text = "username:";
            // 
            // RegistrationLabel
            // 
            this.RegistrationLabel.AutoSize = true;
            this.RegistrationLabel.Font = new System.Drawing.Font("Century Gothic", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RegistrationLabel.Location = new System.Drawing.Point(39, 46);
            this.RegistrationLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.RegistrationLabel.Name = "RegistrationLabel";
            this.RegistrationLabel.Size = new System.Drawing.Size(166, 32);
            this.RegistrationLabel.TabIndex = 24;
            this.RegistrationLabel.Text = "Registration";
            // 
            // back_button
            // 
            this.back_button.BackColor = System.Drawing.Color.Transparent;
            this.back_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.back_button.Image = ((System.Drawing.Image)(resources.GetObject("back_button.Image")));
            this.back_button.ImageActive = null;
            this.back_button.InitialImage = null;
            this.back_button.Location = new System.Drawing.Point(51, 695);
            this.back_button.Name = "back_button";
            this.back_button.Size = new System.Drawing.Size(34, 37);
            this.back_button.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.back_button.TabIndex = 36;
            this.back_button.TabStop = false;
            this.back_button.Zoom = 10;
            this.back_button.Click += new System.EventHandler(this.back_button_Click);
            // 
            // RegistrationPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.back_button);
            this.Controls.Add(this.birthDatePicker);
            this.Controls.Add(this.registerButton);
            this.Controls.Add(this.repasswordTextBox);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(this.birthDateStaticLabel);
            this.Controls.Add(this.repasswordStaticLabel);
            this.Controls.Add(this.passwordStaticLabel);
            this.Controls.Add(this.emailStaticLabel);
            this.Controls.Add(this.usernameStaticLabel);
            this.Controls.Add(this.RegistrationLabel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "RegistrationPage";
            this.Size = new System.Drawing.Size(698, 748);
            ((System.ComponentModel.ISupportInitialize)(this.back_button)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuDatepicker birthDatePicker;
        private Bunifu.Framework.UI.BunifuFlatButton registerButton;
        private Bunifu.Framework.UI.BunifuMaterialTextbox repasswordTextBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox passwordTextBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox emailTextBox;
        private Bunifu.Framework.UI.BunifuMaterialTextbox usernameTextBox;
        private System.Windows.Forms.Label birthDateStaticLabel;
        private System.Windows.Forms.Label repasswordStaticLabel;
        private System.Windows.Forms.Label passwordStaticLabel;
        private System.Windows.Forms.Label emailStaticLabel;
        private System.Windows.Forms.Label usernameStaticLabel;
        private System.Windows.Forms.Label RegistrationLabel;
        private Bunifu.Framework.UI.BunifuImageButton back_button;
    }
}
