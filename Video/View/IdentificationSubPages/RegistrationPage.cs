﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.Model;
using Oracle.ManagedDataAccess.Client;
using Video.Dao;

namespace Video.View.IdentificationSubPages
{
    public partial class RegistrationPage : UserControl
    {
        private UserControl loginPage;

        public void setLoginPage(UserControl log)
        {
            this.loginPage = log;
        }
        public RegistrationPage()
        {
            InitializeComponent();
        }



        private void back_button_Click(object sender, EventArgs e)
        {
            loginPage.BringToFront();

        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            if (usernameTextBox.Text.Equals(String.Empty)) {
                MessageBox.Show("Empty username");
            }
            else if (passwordTextBox.Text.Equals(String.Empty)) {
                MessageBox.Show("Empty password");
            }
            else if (emailTextBox.Text.Equals(String.Empty)){
                MessageBox.Show("Empty email");
            }
            else if (!passwordTextBox.Text.Equals(repasswordTextBox.Text))
            {
                MessageBox.Show("The passwords are not match");
            }
            else {
                String username = usernameTextBox.Text;
                String password = passwordTextBox.Text;
                String email = emailTextBox.Text;
                String birthdate = birthDatePicker.Value.ToString();


                Database dao = new Database();
                dao.insertUserIntoUserTable(new UserBean(username,password,email,birthdate));

                loginPage.BringToFront();
              
            }
        }
    }
}
