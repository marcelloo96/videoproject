﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.View;
using Video.Model;
using Video.Dao;

namespace Video.View
{
    public partial class LoginPage : UserControl
    {
        private UserControl registerPage;
        private Form identPage;

        public void setRegisterPage(UserControl regpage) {
            this.registerPage = regpage;
        }

        public void setIdentPage(Form ident) {
            this.identPage = ident;
        }

        public LoginPage()
        {
            InitializeComponent();
        }



        private void registerLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            registerPage.BringToFront();
        }


        private void visitorButton_Click(object sender, EventArgs e)
        {
            UserBean visitor = new UserBean() ;
            visitor.userid = 0;
            Form main = new MainPage(visitor);

            identPage.Dispose();
            main.ShowDialog();

        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (usernameTextBox.Text.Equals(String.Empty))
            {
                MessageBox.Show("Empty username filed");
            }
            else if (passwordTextBox.Text.Equals(String.Empty))
            {
                MessageBox.Show("Empty password field");
            }
            else {
                Database dao = new Database();
                UserBean bean = dao.getUserByUsernameAndPasswordInUserTable(usernameTextBox.Text, passwordTextBox.Text);
                if (bean == null)
                {
                    MessageBox.Show("Hiba a bejelentkezésnél!");
                }
                else {
                    Form main = new MainPage(bean);
                    identPage.Dispose();
                    main.ShowDialog();
                }
            }
        }
    }
}
