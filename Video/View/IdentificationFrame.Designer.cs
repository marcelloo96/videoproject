﻿namespace Video.View
{
    partial class IdentificationFrame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IdentificationFrame));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.topPanel = new System.Windows.Forms.Panel();
            this.TopPanelForWindowButtons = new System.Windows.Forms.Panel();
            this.red_button = new Bunifu.Framework.UI.BunifuImageButton();
            this.green_button = new Bunifu.Framework.UI.BunifuImageButton();
            this.yellow_button = new Bunifu.Framework.UI.BunifuImageButton();
            this.draggableTopPanel = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.middlePanel = new System.Windows.Forms.Panel();
            this.registrationPage = new Video.View.IdentificationSubPages.RegistrationPage();
            this.loginPage = new Video.View.LoginPage();
            this.middleLeftPanel = new System.Windows.Forms.Panel();
            this.middleRightPlaceHolder = new System.Windows.Forms.Panel();
            this.topPanel.SuspendLayout();
            this.TopPanelForWindowButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.red_button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.green_button)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yellow_button)).BeginInit();
            this.middlePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.topPanel.Controls.Add(this.TopPanelForWindowButtons);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(1920, 75);
            this.topPanel.TabIndex = 0;
            this.topPanel.DoubleClick += new System.EventHandler(this.topPanel_DoubleClick);
            // 
            // TopPanelForWindowButtons
            // 
            this.TopPanelForWindowButtons.Controls.Add(this.red_button);
            this.TopPanelForWindowButtons.Controls.Add(this.green_button);
            this.TopPanelForWindowButtons.Controls.Add(this.yellow_button);
            this.TopPanelForWindowButtons.Dock = System.Windows.Forms.DockStyle.Right;
            this.TopPanelForWindowButtons.Location = new System.Drawing.Point(1759, 0);
            this.TopPanelForWindowButtons.Margin = new System.Windows.Forms.Padding(4);
            this.TopPanelForWindowButtons.Name = "TopPanelForWindowButtons";
            this.TopPanelForWindowButtons.Size = new System.Drawing.Size(161, 75);
            this.TopPanelForWindowButtons.TabIndex = 4;
            // 
            // red_button
            // 
            this.red_button.BackColor = System.Drawing.Color.Transparent;
            this.red_button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("red_button.BackgroundImage")));
            this.red_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.red_button.Image = ((System.Drawing.Image)(resources.GetObject("red_button.Image")));
            this.red_button.ImageActive = null;
            this.red_button.InitialImage = null;
            this.red_button.Location = new System.Drawing.Point(105, 28);
            this.red_button.Margin = new System.Windows.Forms.Padding(4);
            this.red_button.Name = "red_button";
            this.red_button.Size = new System.Drawing.Size(27, 25);
            this.red_button.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.red_button.TabIndex = 0;
            this.red_button.TabStop = false;
            this.red_button.Zoom = 10;
            this.red_button.Click += new System.EventHandler(this.red_button_Click);
            // 
            // green_button
            // 
            this.green_button.BackColor = System.Drawing.Color.Transparent;
            this.green_button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("green_button.BackgroundImage")));
            this.green_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.green_button.Image = ((System.Drawing.Image)(resources.GetObject("green_button.Image")));
            this.green_button.ImageActive = null;
            this.green_button.InitialImage = null;
            this.green_button.Location = new System.Drawing.Point(36, 28);
            this.green_button.Margin = new System.Windows.Forms.Padding(4);
            this.green_button.Name = "green_button";
            this.green_button.Size = new System.Drawing.Size(27, 25);
            this.green_button.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.green_button.TabIndex = 0;
            this.green_button.TabStop = false;
            this.green_button.Zoom = 10;
            this.green_button.Click += new System.EventHandler(this.green_button_Click);
            // 
            // yellow_button
            // 
            this.yellow_button.BackColor = System.Drawing.Color.Transparent;
            this.yellow_button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("yellow_button.BackgroundImage")));
            this.yellow_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.yellow_button.Image = ((System.Drawing.Image)(resources.GetObject("yellow_button.Image")));
            this.yellow_button.ImageActive = null;
            this.yellow_button.InitialImage = null;
            this.yellow_button.Location = new System.Drawing.Point(71, 28);
            this.yellow_button.Margin = new System.Windows.Forms.Padding(4);
            this.yellow_button.Name = "yellow_button";
            this.yellow_button.Size = new System.Drawing.Size(27, 25);
            this.yellow_button.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.yellow_button.TabIndex = 0;
            this.yellow_button.TabStop = false;
            this.yellow_button.Zoom = 10;
            this.yellow_button.Click += new System.EventHandler(this.yellow_button_Click);
            // 
            // draggableTopPanel
            // 
            this.draggableTopPanel.Fixed = true;
            this.draggableTopPanel.Horizontal = true;
            this.draggableTopPanel.TargetControl = this.topPanel;
            this.draggableTopPanel.Vertical = true;
            // 
            // bottomPanel
            // 
            this.bottomPanel.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 1005);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(1920, 75);
            this.bottomPanel.TabIndex = 1;
            // 
            // middlePanel
            // 
            this.middlePanel.Controls.Add(this.registrationPage);
            this.middlePanel.Controls.Add(this.loginPage);
            this.middlePanel.Controls.Add(this.middleLeftPanel);
            this.middlePanel.Controls.Add(this.middleRightPlaceHolder);
            this.middlePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.middlePanel.Location = new System.Drawing.Point(0, 75);
            this.middlePanel.Name = "middlePanel";
            this.middlePanel.Size = new System.Drawing.Size(1920, 930);
            this.middlePanel.TabIndex = 2;
            // 
            // registrationPage
            // 
            this.registrationPage.AutoScroll = true;
            this.registrationPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.registrationPage.Location = new System.Drawing.Point(200, 0);
            this.registrationPage.Name = "registrationPage";
            this.registrationPage.Size = new System.Drawing.Size(1520, 930);
            this.registrationPage.TabIndex = 3;
            this.registrationPage.Load += new System.EventHandler(this.registrationPage_Load);
            // 
            // loginPage
            // 
            this.loginPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginPage.Location = new System.Drawing.Point(200, 0);
            this.loginPage.Name = "loginPage";
            this.loginPage.Size = new System.Drawing.Size(1520, 930);
            this.loginPage.TabIndex = 2;
            // 
            // middleLeftPanel
            // 
            this.middleLeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.middleLeftPanel.Location = new System.Drawing.Point(0, 0);
            this.middleLeftPanel.Name = "middleLeftPanel";
            this.middleLeftPanel.Size = new System.Drawing.Size(200, 930);
            this.middleLeftPanel.TabIndex = 1;
            // 
            // middleRightPlaceHolder
            // 
            this.middleRightPlaceHolder.Dock = System.Windows.Forms.DockStyle.Right;
            this.middleRightPlaceHolder.Location = new System.Drawing.Point(1720, 0);
            this.middleRightPlaceHolder.Name = "middleRightPlaceHolder";
            this.middleRightPlaceHolder.Size = new System.Drawing.Size(200, 930);
            this.middleRightPlaceHolder.TabIndex = 0;
            // 
            // IdentificationFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1920, 1080);
            this.Controls.Add(this.middlePanel);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.topPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "IdentificationFrame";
            this.Text = "LoginPage";
            this.topPanel.ResumeLayout(false);
            this.TopPanelForWindowButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.red_button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.green_button)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yellow_button)).EndInit();
            this.middlePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel topPanel;
        private Bunifu.Framework.UI.BunifuDragControl draggableTopPanel;
        private System.Windows.Forms.Panel TopPanelForWindowButtons;
        private Bunifu.Framework.UI.BunifuImageButton red_button;
        private Bunifu.Framework.UI.BunifuImageButton green_button;
        private Bunifu.Framework.UI.BunifuImageButton yellow_button;
        private System.Windows.Forms.Panel middlePanel;
        private System.Windows.Forms.Panel middleLeftPanel;
        private System.Windows.Forms.Panel middleRightPlaceHolder;
        private System.Windows.Forms.Panel bottomPanel;
        public LoginPage loginPage;
        private IdentificationSubPages.RegistrationPage registrationPage;
    }
}