﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Video.View
{
    public partial class IdentificationFrame : Form
    {

        public IdentificationFrame()
        {
            InitializeComponent();
            loginPage.BringToFront();
            loginPage.setRegisterPage(registrationPage);
            loginPage.setIdentPage(this);
            registrationPage.setLoginPage(loginPage);

        }

        private void green_button_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void yellow_button_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void red_button_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void registrationPage_Load(object sender, EventArgs e)
        {

        }

        private void topPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void topPanel_DoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
            }
        }


    }
}
