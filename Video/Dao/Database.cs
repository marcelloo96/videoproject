﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows;
using Video.HelperMethods;
using Video.Model;
using Video.Model.Beans.Helper;
using Video.View.MainPageSubPages.Components;

namespace Video.Dao
{
    class Database
    {
        UserBean user;
        MainPage main;
        VideoBean video;

        private const string connectionString = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521)) (CONNECT_DATA = (SID = xe))); User Id = system; Password = password; ";

        #region ID-k lekérdezése


        public int getNextUnusedIdOfVideoTable()
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                int id;
                connection.Open();
                command.CommandText = "getLastInsertedVideoId";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("lastInsertedVideoId", OracleDbType.Decimal);
                command.Parameters["lastInsertedVideoId"].Direction = ParameterDirection.ReturnValue;
                command.ExecuteNonQuery();
                int.TryParse(Convert.ToString(command.Parameters["lastInsertedVideoId"].Value), out id);
                return id += 1;
            }
        }

        public int getNextUnusedIdOfCategoryTable()
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                int id;
                connection.Open();
                command.CommandText = "getLastInsertedCategoryId";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("lastInsertedCategoryId", OracleDbType.Decimal);
                command.Parameters["lastInsertedCategoryId"].Direction = ParameterDirection.ReturnValue;
                command.ExecuteNonQuery();
                int.TryParse(Convert.ToString(command.Parameters["lastInsertedCategoryId"].Value), out id);
                return id += 1;
            }
        }

        public int getNextUnusedIdOfLabelTable()
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                int id;
                connection.Open();
                command.CommandText = "getLastInsertedLabelId";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("lastInsertedLabelId", OracleDbType.Decimal);
                command.Parameters["lastInsertedLabelId"].Direction = ParameterDirection.ReturnValue;
                command.ExecuteNonQuery();
                int.TryParse(Convert.ToString(command.Parameters["lastInsertedLabelId"].Value), out id);
                return id += 1;
            }
        }

        public int getNextUnusedIdOfCommentTable()
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                int id;
                connection.Open();
                command.CommandText = "getLastInsertedCommentId";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("lastInsertedCommentId", OracleDbType.Decimal);
                command.Parameters["lastInsertedCommentId"].Direction = ParameterDirection.ReturnValue;
                command.ExecuteNonQuery();
                int.TryParse(Convert.ToString(command.Parameters["lastInsertedCommentId"].Value), out id);
                return id += 1;
            }
        }

        public int getNextUnusedIdOfPlaylistTable()
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                int id;
                connection.Open();
                command.CommandText = "getLastInsertedPlaylistId";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("lastInsertedPlaylistId", OracleDbType.Decimal);
                command.Parameters["lastInsertedPlaylistId"].Direction = ParameterDirection.ReturnValue;
                command.ExecuteNonQuery();
                int.TryParse(Convert.ToString(command.Parameters["lastInsertedPlaylistId"].Value), out id);
                return id += 1;
            }
        }

        /*category, label*/
        #endregion

        #region UserTable
        public int getNextUnusedIdOfUserTable()
        {
            int id;
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = "getLastInsertedUserId";
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("lastInsertedUserId", OracleDbType.Decimal);
                command.Parameters["lastInsertedUserId"].Direction = ParameterDirection.ReturnValue;
                command.ExecuteNonQuery();
                int.TryParse(Convert.ToString(command.Parameters["lastInsertedUserId"].Value), out id);
                id++;
            }
            return id;
        }

        public bool insertUserIntoUserTable(UserBean user)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"INSERT INTO Usertable (userID, userUsername, userPassword, userEmail, userBirthdate, userIsAdmin) VALUES (:userID, :userUsername, :userPassword, :userEmail, :userBirthdate, :userIsAdmin)";
                command.Parameters.Add(":userID", getNextUnusedIdOfUserTable());
                command.Parameters.Add(":userUsername", user.username);
                command.Parameters.Add(":userPassword", user.password);
                command.Parameters.Add(":userEmail", user.email);
                command.Parameters.Add(":userBirthdate", user.birthdate);
                command.Parameters.Add(":userIsAdmin", 0);
                if (command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
            }
            return false;
        }

        public bool updateUserEmail(UserBean user, string newUserEmail)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"UPDATE USERTABLE SET userEmail = :newUserEmail WHERE userId = :thisUserId";
                command.Parameters.Add(":newUserEmail", OracleDbType.Varchar2).Value = newUserEmail;
                command.Parameters.Add(":thisUserId", OracleDbType.Varchar2).Value = user.userid;
                if (command.ExecuteNonQuery() == 1)
                {
                    user.email = newUserEmail;
                    return true;
                }
            }
            return false;
        }

        public bool updateUserPassword(UserBean user, string newUserPassword)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"UPDATE USERTABLE SET userPassword = :newUserPassword WHERE userId = :thisUserId";
                command.Parameters.Add(":newUserPassword", OracleDbType.Varchar2).Value = newUserPassword;
                command.Parameters.Add(":thisUserId", OracleDbType.Varchar2).Value = user.userid;
                if (command.ExecuteNonQuery() == 1)
                {
                    user.password = newUserPassword;
                    return true;
                }
            }
            return false;
        }

        public UserBean getUserByUsernameAndPasswordInUserTable(string userUsernameToCheck, string userPasswordToCheck)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"SELECT * FROM USERTABLE WHERE userUsername=:userUsernameToCheck AND userPassword=:userPasswordToCheck";
                command.Parameters.Add(":userUsernameToCheck", OracleDbType.Varchar2).Value = userUsernameToCheck;
                command.Parameters.Add(":userPasswordToCheck", OracleDbType.Varchar2).Value = userPasswordToCheck;
                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserBean bean = new UserBean()
                    {
                        userid = reader.GetInt32(reader.GetOrdinal("userId")),
                        username = reader.GetString(reader.GetOrdinal("userUsername")),
                        password = reader.GetString(reader.GetOrdinal("userPassword")),
                        email = reader.GetString(reader.GetOrdinal("userEmail")),
                        birthdate = reader.GetString(reader.GetOrdinal("userBirthdate")),
                        isAdmin = reader.GetInt32(reader.GetOrdinal("userIsAdmin"))
                    };
                    return bean;
                }
            }
            return null;
        }

        public UserBean selectUserFromUserTableByUserId(int userIdToFind)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"SELECT * FROM USERTABLE WHERE userId=:userIdToFind";
                command.Parameters.Add(":userIdToFind", OracleDbType.Int32).Value = userIdToFind;
                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    UserBean bean = new UserBean()
                    {
                        userid = reader.GetInt32(reader.GetOrdinal("userId")),
                        username = reader.GetString(reader.GetOrdinal("userUsername")),
                        password = reader.GetString(reader.GetOrdinal("userPassword")),
                        email = reader.GetString(reader.GetOrdinal("userEmail")),
                        birthdate = reader.GetString(reader.GetOrdinal("userBirthdate")),
                        isAdmin = reader.GetInt32(reader.GetOrdinal("userIsAdmin"))
                    };
                    return bean;
                }
            }
            return null;
        }
        #endregion

        #region VideoTable lekérdezései
        internal List<VideoBean> getListOfVideosFromVideoTableUploadedByThisUser(UserBean user)
        {
            List<VideoBean> videos = new List<VideoBean>();
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"SELECT * FROM VIDEOTABLE WHERE userId = :userIdToFind";
                command.Parameters.Add(":userIdToFind", OracleDbType.Int32).Value = user.userid;
                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    VideoBean video = new VideoBean()
                    {
                        videoId = reader.GetInt32(reader.GetOrdinal("videoId")),
                        videoTitle = reader.GetString(reader.GetOrdinal("videoTitle")),
                        videoDescription = reader.GetString(reader.GetOrdinal("videoDescription")),
                        videoUploadDate = DateTime.ParseExact(reader.GetString(reader.GetOrdinal("videoUploadDate")), "yyyy. MM. dd. HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                        videoViewCount = reader.GetInt32(reader.GetOrdinal("videoViewCount")),
                        videoLikeCount = reader.GetInt32(reader.GetOrdinal("videoLikeCount")),
                        videoThumbnailImageURL = new Uri(reader.GetString(reader.GetOrdinal("videoThumbnailImageURL"))),
                        videoURL = new Uri(reader.GetString(reader.GetOrdinal("videoURL"))),
                        isPublic = reader.GetInt32(reader.GetOrdinal("videoIsPublic")),
                        areCommentsEnabled = reader.GetInt32(reader.GetOrdinal("videoAreCommentsEnabled")),
                        isChildrenSafe = reader.GetInt32(reader.GetOrdinal("videoIsChildrenSafe")),
                        userID = reader.GetInt32(reader.GetOrdinal("userId"))
                    };
                    videos.Add(video);
                }
            }
            return videos;
        }

        public bool insertVideoIntoVideoTable(VideoBean video)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"INSERT INTO VIDEOTABLE (videoId, videoTitle, videoDescription, videoUploadDate, videoViewCount, videoLikeCount, videoThumbnailImageURL, videoURL, videoIsPublic, videoAreCommentsEnabled, videoIsChildrenSafe, userId) VALUES (:videoId, :videoTitle, :videoDescription, :videoUploadDate, :videoViewCount, :videoLikeCount, :videoThumbnailImageURL, :videoURL, :videoIsPublic, :videoAreCommentsEnabled, :videoIsChildrenSafe, :userId)";
                command.Parameters.Add(":videoId", OracleDbType.Decimal).Value = video.videoId;
                command.Parameters.Add(":videoTitle", OracleDbType.Varchar2).Value = video.videoTitle;
                command.Parameters.Add(":videoDescription", OracleDbType.Varchar2).Value = video.videoDescription;
                command.Parameters.Add(":videoUploadDate", OracleDbType.Varchar2).Value = video.videoUploadDate.ToString();
                command.Parameters.Add(":videoViewCount", OracleDbType.Decimal).Value = video.videoViewCount;
                command.Parameters.Add(":videoLikeCount", OracleDbType.Decimal).Value = video.videoLikeCount;
                command.Parameters.Add(":videoThumbnailImageURL", OracleDbType.Varchar2).Value = video.videoThumbnailImageURL.ToString();
                command.Parameters.Add(":videoURL", OracleDbType.Varchar2).Value = video.videoURL.ToString();
                command.Parameters.Add(":videoIsPublic", OracleDbType.Decimal).Value = video.isPublic;
                command.Parameters.Add(":videoAreCommentsEnabled", OracleDbType.Decimal).Value = video.areCommentsEnabled;
                command.Parameters.Add(":videoIsChildrenSafe", OracleDbType.Decimal).Value = video.isChildrenSafe;
                command.Parameters.Add(":userId", OracleDbType.Decimal).Value = video.userID;
                if (command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
            }
            return false;
        }

        public List<VideoBean> getVideosBySearch(string titleToSearch)
        {
            titleToSearch = "%" + titleToSearch + "%";
            List<VideoBean> videos = new List<VideoBean>();
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"SELECT * FROM VIDEOTABLE WHERE videoTitle LIKE :titleToSearch";
                command.Parameters.Add(":titleToSearch", OracleDbType.Varchar2).Value = titleToSearch;
                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    VideoBean video = new VideoBean()
                    {
                        videoId = reader.GetInt32(reader.GetOrdinal("videoId")),
                        videoTitle = reader.GetString(reader.GetOrdinal("videoTitle")),
                        videoDescription = reader.GetString(reader.GetOrdinal("videoDescription")),
                        videoUploadDate = DateTime.ParseExact(reader.GetString(reader.GetOrdinal("videoUploadDate")), "yyyy. MM. dd. HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                        videoViewCount = reader.GetInt32(reader.GetOrdinal("videoViewCount")),
                        videoLikeCount = reader.GetInt32(reader.GetOrdinal("videoLikeCount")),
                        videoThumbnailImageURL = new Uri(reader.GetString(reader.GetOrdinal("videoThumbnailImageURL"))),
                        videoURL = new Uri(reader.GetString(reader.GetOrdinal("videoURL"))),
                        isPublic = reader.GetInt32(reader.GetOrdinal("videoIsPublic")),
                        areCommentsEnabled = reader.GetInt32(reader.GetOrdinal("videoAreCommentsEnabled")),
                        isChildrenSafe = reader.GetInt32(reader.GetOrdinal("videoIsChildrenSafe")),
                        userID = reader.GetInt32(reader.GetOrdinal("userId"))
                    };
                    videos.Add(video);
                }
            }
            return videos;
        }

        public List<HomePageSingleUserForm> getListOfUsersWithMostUploadedVideos()
        {
            List<HomePageSingleUserForm> forms = new List<HomePageSingleUserForm>();
            List<int> userIds = new List<int>();
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"SELECT userId, COUNT(*) AS cnt FROM VIDEOTABLE GROUP BY userId ORDER BY cnt DESC";
                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    userIds.Add(reader.GetInt32(reader.GetOrdinal("userId")));

                }
            }
            int cnt = 0;
            foreach (int i in userIds)
            {
                if (cnt > 13)
                {
                    return forms;
                }

                UserBean user = selectUserFromUserTableByUserId(i);
                HomePageSingleUserForm a = new HomePageSingleUserForm();
                a.username.Text = user.username;
                forms.Add(a);
                cnt++;
            }
            return forms;
        }

        public List<VideoBean> listMostViewedVideosOfMonth()
        {
            List<VideoBean> videos = new List<VideoBean>();
            DateTime currentDate = DateTime.Now;
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT * FROM VIDEOTABLE WHERE videoUploadDate >:uplDate ORDER BY videoViewCount DESC";
                cmd.Parameters.Add(":uplDate", OracleDbType.Varchar2).Value = currentDate.ToString("yyyy. MM.");
                OracleDataReader reader = cmd.ExecuteReader();
                int db = 0;
                while (reader.Read())
                {
                    if (db > 7)
                    {
                        return videos;
                    }
                    VideoBean video = new VideoBean()
                    {
                        videoId = reader.GetInt32(reader.GetOrdinal("videoId")),
                        videoTitle = reader.GetString(reader.GetOrdinal("videoTitle")),
                        videoDescription = reader.GetString(reader.GetOrdinal("videoDescription")),
                        videoUploadDate = DateTime.ParseExact(reader.GetString(reader.GetOrdinal("videoUploadDate")), "yyyy. MM. dd. HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                        videoViewCount = reader.GetInt32(reader.GetOrdinal("videoViewCount")),
                        videoLikeCount = reader.GetInt32(reader.GetOrdinal("videoLikeCount")),
                        videoThumbnailImageURL = new Uri(reader.GetString(reader.GetOrdinal("videoThumbnailImageURL"))),
                        videoURL = new Uri(reader.GetString(reader.GetOrdinal("videoURL"))),
                        isPublic = reader.GetInt32(reader.GetOrdinal("videoIsPublic")),
                        areCommentsEnabled = reader.GetInt32(reader.GetOrdinal("videoAreCommentsEnabled")),
                        isChildrenSafe = reader.GetInt32(reader.GetOrdinal("videoIsChildrenSafe")),
                        userID = reader.GetInt32(reader.GetOrdinal("userId"))
                    };
                    videos.Add(video);
                    db++;
                }
            }
            return videos;
        }

        public List<VideoBean> listMostViewedVideosOfWeek()
        {
            List<VideoBean> videos = new List<VideoBean>();
            DateTime currentDate = DateTime.Now;
            String lastWeekDate;
            int day;
            int month;
            int year;
            Int32.TryParse(currentDate.ToString("dd"), out day);
            Int32.TryParse(currentDate.ToString("MM"), out month);
            Int32.TryParse(currentDate.ToString("yyyy"), out year);
            int dayOfMonth;
            if ((day - 7) < 0)
            {
                dayOfMonth = 30 - 7;

                if ((month - 1) < 0)
                {
                    month = 12;
                    lastWeekDate = year + ". " + month + ". " + dayOfMonth + ".";
                }
                else
                {
                    month -= 1;
                    lastWeekDate = currentDate.ToString("yyyy. ") + month + ". " + dayOfMonth + ".";
                }
            }
            else
            {
                dayOfMonth = day - 7;

                if (dayOfMonth < 10)
                {
                    lastWeekDate = currentDate.ToString("yyyy. MM. ") + "0" + dayOfMonth + ".";
                }
                else
                {
                    lastWeekDate = currentDate.ToString("yyyy. MM. ") + dayOfMonth + ".";
                }
            }
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT * FROM VideoTable Where videoUploadDate >:uplDate ORDER BY videoViewCount DESC";
                cmd.Parameters.Add(":uplDate", OracleDbType.Varchar2).Value = lastWeekDate;
                OracleDataReader reader = cmd.ExecuteReader();
                int db = 0;
                while (reader.Read())
                {
                    if (db > 7)
                    {
                        return videos;
                    }
                    VideoBean video = new VideoBean()
                    {
                        videoId = reader.GetInt32(reader.GetOrdinal("videoId")),
                        videoTitle = reader.GetString(reader.GetOrdinal("videoTitle")),
                        videoDescription = reader.GetString(reader.GetOrdinal("videoDescription")),
                        videoUploadDate = DateTime.ParseExact(reader.GetString(reader.GetOrdinal("videoUploadDate")), "yyyy. MM. dd. HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                        videoViewCount = reader.GetInt32(reader.GetOrdinal("videoViewCount")),
                        videoLikeCount = reader.GetInt32(reader.GetOrdinal("videoLikeCount")),
                        videoThumbnailImageURL = new Uri(reader.GetString(reader.GetOrdinal("videoThumbnailImageURL"))),
                        videoURL = new Uri(reader.GetString(reader.GetOrdinal("videoURL"))),
                        isPublic = reader.GetInt32(reader.GetOrdinal("videoIsPublic")),
                        areCommentsEnabled = reader.GetInt32(reader.GetOrdinal("videoAreCommentsEnabled")),
                        isChildrenSafe = reader.GetInt32(reader.GetOrdinal("videoIsChildrenSafe")),
                        userID = reader.GetInt32(reader.GetOrdinal("userId"))
                    };
                    videos.Add(video);
                    db++;
                }
            }
            return videos;
        }

        public List<VideoBean> listMostViewedVideosOfDay()
        {
            List<VideoBean> videos = new List<VideoBean>();
            DateTime currentDate = DateTime.Now;

            int db = 0;

            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT * FROM VideoTable Where videoUploadDate >:uplDate ORDER BY videoViewCount DESC";
                cmd.Parameters.Add(":uplDate", OracleDbType.Varchar2).Value = currentDate.ToString("yyyy. MM. dd");


                OracleDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    if (db > 7)
                    {
                        return videos;
                    }

                    VideoBean video = new VideoBean()
                    {
                        videoId = reader.GetInt32(reader.GetOrdinal("videoId")),
                        videoTitle = reader.GetString(reader.GetOrdinal("videoTitle")),
                        videoDescription = reader.GetString(reader.GetOrdinal("videoDescription")),
                        videoUploadDate = DateTime.ParseExact(reader.GetString(reader.GetOrdinal("videoUploadDate")), "yyyy. MM. dd. HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                        videoViewCount = reader.GetInt32(reader.GetOrdinal("videoViewCount")),
                        videoLikeCount = reader.GetInt32(reader.GetOrdinal("videoLikeCount")),
                        videoThumbnailImageURL = new Uri(reader.GetString(reader.GetOrdinal("videoThumbnailImageURL"))),
                        videoURL = new Uri(reader.GetString(reader.GetOrdinal("videoURL"))),
                        isPublic = reader.GetInt32(reader.GetOrdinal("videoIsPublic")),
                        areCommentsEnabled = reader.GetInt32(reader.GetOrdinal("videoAreCommentsEnabled")),
                        isChildrenSafe = reader.GetInt32(reader.GetOrdinal("videoIsChildrenSafe")),
                        userID = reader.GetInt32(reader.GetOrdinal("userId"))
                    };
                    videos.Add(video);
                    db++;
                }
            }
            return videos;
        }

        public bool deleteVideoFromVideoTable(VideoBean video)
        {
            using (OracleConnection connect = new OracleConnection(connectionString))
            using (OracleCommand command = connect.CreateCommand())
            {
                connect.Open();
                command.CommandText = @"DELETE FROM VideoTable WHERE videoId=:videoId";
                command.Parameters.Add(":videoId", OracleDbType.Int32).Value = video.videoId;
                if (command.ExecuteNonQuery() != 1)
                {
                    return false;
                }
            }
            using (OracleConnection connect = new OracleConnection(connectionString))
            using (OracleCommand command = connect.CreateCommand())
            {
                connect.Open();
                command.CommandText = @"DELETE FROM CONTAINSCATEGORYVIDEOTABLE WHERE videoId=:videoId";
                command.Parameters.Add(":videoId", OracleDbType.Int32).Value = video.videoId;
                if (command.ExecuteNonQuery() != 1)
                {
                    return false;
                }
            }
            using (OracleConnection connect = new OracleConnection(connectionString))
            using (OracleCommand command = connect.CreateCommand())
            {
                connect.Open();
                command.CommandText = @"DELETE FROM CONTAINSLABELVIDEOTABLE WHERE videoId=:videoId";
                command.Parameters.Add(":videoId", OracleDbType.Int32).Value = video.videoId;
                if (command.ExecuteNonQuery() != 1)
                {
                    return false;
                }
            }
            return true;
        }

        public bool updateVideoFromVideoTable(VideoBean video)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"UPDATE VIDEOTABLE SET videoTitle = :newVideoTitle, videoDescription = :newVideoDescription, videoIsPublic = :newVideoIsPublic, videoAreCommentsEnabled = :newVideoAreCommentsEnabled, videoIsChildrenSafe = :newVideoIsChildrenSafe WHERE videoId = :thisVideoId";
                command.Parameters.Add(":newVideoTitle", OracleDbType.Varchar2).Value = video.videoTitle;
                command.Parameters.Add(":newVideoDescription", OracleDbType.Varchar2).Value = video.videoDescription;
                command.Parameters.Add(":newVideoIsPublic", OracleDbType.Int32).Value = video.isPublic;
                command.Parameters.Add(":newVideoAreCommentsEnabled", OracleDbType.Int32).Value = video.areCommentsEnabled;
                command.Parameters.Add(":newVideoIsChildrenSafe", OracleDbType.Int32).Value = video.isChildrenSafe;
                command.Parameters.Add(":thisVideoId", OracleDbType.Varchar2).Value = video.videoId;
                if (command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
            }
            return false;
        }

        public bool deleteCategoriesFromVideo(VideoBean video)
        {
            using (OracleConnection connect = new OracleConnection(connectionString))
            using (OracleCommand command = connect.CreateCommand())
            {
                connect.Open();
                command.CommandText = @"DELETE FROM CONTAINSCATEGORYVIDEOTABLE WHERE videoId=:videoId";
                command.Parameters.Add(":videoId", OracleDbType.Int32).Value = video.videoId;
                if (command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
            }
            return false;
        }

        public bool deleteLabelsFromVideo(VideoBean video)
        {
            using (OracleConnection connect = new OracleConnection(connectionString))
            using (OracleCommand command = connect.CreateCommand())
            {
                connect.Open();
                command.CommandText = @"DELETE FROM CONTAINSLABELVIDEOTABLE WHERE videoId=:videoId";
                command.Parameters.Add(":videoId", OracleDbType.Int32).Value = video.videoId;
                if (command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
            }
            return false;
        }

        public VideoBean getVideoById(int videoID)
        {
            using (OracleConnection connect = new OracleConnection(connectionString))
            using (OracleCommand command = connect.CreateCommand())
            {
                connect.Open();
                command.CommandText = @"SELECT * FROM VIDEOTABLE WHERE VIDEOID = :vidID";
                command.Parameters.Add(":vidID", OracleDbType.Int32).Value = videoID;
                OracleDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    VideoBean video = new VideoBean()
                    {
                        videoId = reader.GetInt32(reader.GetOrdinal("videoId")),
                        videoTitle = reader.GetString(reader.GetOrdinal("videoTitle")),
                        videoDescription = reader.GetString(reader.GetOrdinal("videoDescription")),
                        videoUploadDate = DateTime.ParseExact(reader.GetString(reader.GetOrdinal("videoUploadDate")), "yyyy. MM. dd. HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                        videoViewCount = reader.GetInt32(reader.GetOrdinal("videoViewCount")),
                        videoLikeCount = reader.GetInt32(reader.GetOrdinal("videoLikeCount")),
                        videoThumbnailImageURL = new Uri(reader.GetString(reader.GetOrdinal("videoThumbnailImageURL"))),
                        videoURL = new Uri(reader.GetString(reader.GetOrdinal("videoURL"))),
                        isPublic = reader.GetInt32(reader.GetOrdinal("videoIsPublic")),
                        areCommentsEnabled = reader.GetInt32(reader.GetOrdinal("videoAreCommentsEnabled")),
                        isChildrenSafe = reader.GetInt32(reader.GetOrdinal("videoIsChildrenSafe")),
                        userID = reader.GetInt32(reader.GetOrdinal("userId"))
                    };
                    return video;
                }
            }
            return null;
        }
        #endregion

        #region CommentTable lekérdezései
        internal List<CommentBean> listCommentsOfVideo(VideoBean video, UserBean user)
        {
            List<CommentBean> result = new List<CommentBean>();
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT * FROM CommentTable WHERE videoID=:vidID";
                cmd.Parameters.Add(":vidID", OracleDbType.Decimal).Value = video.videoId;

                OracleDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    CommentBean c = new CommentBean();
                    c.commentId = reader.GetInt32(reader.GetOrdinal("commentID"));
                    c.commentText = reader.GetString(reader.GetOrdinal("commentText"));
                    c.commentWriteDate = DateTime.ParseExact(reader.GetString(reader.GetOrdinal("commentWriteDate")), "yyyy. MM. dd. HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    c.userId = reader.GetInt32(reader.GetOrdinal("userID"));
                    c.videoId = reader.GetInt32(reader.GetOrdinal("videoID"));

                    result.Add(c);

                }
            }
            return result;
        }

        public bool insertComment(CommentBean commentToSend)
        {
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand command = conn.CreateCommand())
            {
                conn.Open();
                command.CommandText = @"INSERT INTO CommentTable (commentID, commentText, commentWriteDate,userID,videoID) VALUES (:comID, :comText, :comWriteDate, :usID, :vidID)";
                command.Parameters.Add(":comID", OracleDbType.Decimal).Value = getNextUnusedIdOfCommentTable();
                command.Parameters.Add(":comText", OracleDbType.Varchar2).Value = commentToSend.commentText;
                command.Parameters.Add(":comWriteDate", OracleDbType.Varchar2).Value = commentToSend.commentWriteDate.ToString("yyyy. MM. dd. HH:mm:ss");
                command.Parameters.Add(":usID", OracleDbType.Decimal).Value = commentToSend.userId;
                command.Parameters.Add(":vidID", OracleDbType.Decimal).Value = commentToSend.videoId;

                if (command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
            }
            return false;
        }

        public List<HomePageSingleUserForm> listMostCommentedusers()
        {
            List<HomePageSingleUserForm> forms = new List<HomePageSingleUserForm>();
            List<int> userIds = new List<int>();

            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT userID, COUNT(*) as cnt FROM CommentTable GROUP BY userId ORDER BY cnt DESC";

                OracleDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    userIds.Add(reader.GetInt32(reader.GetOrdinal("userID")));
                }
            }

            int cnt = 0;
            foreach (int i in userIds)
            {
                if (cnt > 13)
                {
                    return forms;
                }

                UserBean user = selectUserFromUserTableByUserId(i);
                HomePageSingleUserForm a = new HomePageSingleUserForm();
                a.username.Text = user.username;
                forms.Add(a);
                cnt++;
            }

            return forms;
        }
        #endregion

        #region PlaylistTable lekérdezései
        public List<PlaylistBean> listPlaylists(UserBean bean)
        {
            List<PlaylistBean> playLists = new List<PlaylistBean>();

            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand command = conn.CreateCommand())
            {
                conn.Open();
                command.CommandText = @"SELECT * FROM PlaylistTable WHERE playlisttable.UserId =:userID";
                command.Parameters.Add(":userID", bean.userid);

                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {

                    PlaylistBean playlist = new PlaylistBean();
                    playlist.playlistName = reader.GetString(reader.GetOrdinal("playlistName"));
                    playlist.playlistId = reader.GetInt32(reader.GetOrdinal("playlistId"));
                    playLists.Add(playlist);
                }
            }
            return playLists;
        }

        public void deletePlaylist(PlaylistBean playlist)
        {
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"DELETE FROM PlaylistTable WHERE playlistId=:pId";
                cmd.Parameters.Add(":pId", OracleDbType.Int32).Value = playlist.playlistId;

                if (cmd.ExecuteNonQuery() == 1)
                {
                    MessageBox.Show("Sikeres törlés");
                }
                else
                {
                    MessageBox.Show("Nem sikerült törölni");
                }
            }
        }

        public bool insertPlaylist(PlaylistBean playlist)
        {
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand command = conn.CreateCommand())
            {
                conn.Open();
                command.CommandText = @"INSERT INTO PlaylistTable (playlistID, playlistName, userId) VALUES (:pId, :pName, :usId)";
                command.Parameters.Add(":pId", OracleDbType.Decimal).Value = getNextUnusedIdOfPlaylistTable();
                command.Parameters.Add(":pName", OracleDbType.Varchar2).Value = playlist.playlistName;
                command.Parameters.Add(":usID", OracleDbType.Decimal).Value = playlist.userId;

                if (command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region CategoryTable lekérdezései
        public List<CategoryBean> listCategories()
        {
            List<CategoryBean> categories = new List<CategoryBean>();
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT * FROM CategoryTable";
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CategoryBean category = new CategoryBean()
                    {
                        categoryId = reader.GetInt32(reader.GetOrdinal("categoryID")),
                        categoryName = reader.GetString(reader.GetOrdinal("categoryName"))
                    };
                    categories.Add(category);
                }
            }
            return categories;
        }

        public CategoryBean getCategoryById(int id)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"SELECT * FROM CategoryTable WHERE CATEGORYID=:catID";
                command.Parameters.Add(":catID", OracleDbType.Int32).Value = id;

                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CategoryBean category = new CategoryBean()
                    {
                        categoryId = reader.GetInt32(reader.GetOrdinal("categoryID")),
                        categoryName = reader.GetString(reader.GetOrdinal("categoryName"))
                    };
                    return category;
                }
            }


            return null;
        }

        #endregion

        #region LabelTable lekérdezései
        public List<LabelBean> listLabels()
        {
            List<LabelBean> labels = new List<LabelBean>();
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT * FROM labeltable";
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    LabelBean label = new LabelBean()
                    {
                        labelId = reader.GetInt32(reader.GetOrdinal("labelID")),
                        labelName = reader.GetString(reader.GetOrdinal("labelName"))
                    };
                    labels.Add(label);
                }
            }
            return labels;
        }

        #endregion

        #region Összekötő táblák
        public void incrementLikeCountOfVideo(VideoBean video, UserBean user)
        {
            var cnt = 0;
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT * FROM LIKEUSERVIDEOTABLE WHERE videoId=:vid2 AND userId=:uid2";
                cmd.Parameters.Add(":vid2", OracleDbType.Int32).Value = video.videoId;
                cmd.Parameters.Add(":uid2", OracleDbType.Int32).Value = user.userid;
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    cnt++;
                }
            }
            if (cnt == 0)
            {
                int prevCount = 0;
                using (OracleConnection conn = new OracleConnection(connectionString))
                using (OracleCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = @"SELECT videoLikeCount FROM VideoTable WHERE videoId=:vid2";
                    cmd.Parameters.Add(":vid2", OracleDbType.Int32).Value = video.videoId;
                    OracleDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        prevCount = reader.GetInt32(reader.GetOrdinal("videoLikeCount"));
                    }
                }
                prevCount++;
                using (OracleConnection conn = new OracleConnection(connectionString))
                using (OracleCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = @"UPDATE videotable SET videoLikeCount=:co2 WHERE videoId=:vid2";
                    cmd.Parameters.Add(":co2", OracleDbType.Int32).Value = prevCount;
                    cmd.Parameters.Add(":vid2", OracleDbType.Int32).Value = video.videoId;
                    OracleDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        prevCount = reader.GetInt32(reader.GetOrdinal("videoLikeCount"));
                    }
                }
                using (OracleConnection conn = new OracleConnection(connectionString))
                using (OracleCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandText = @"INSERT INTO LikeUserVideotable (USERID, VIDEOID) VALUES (:co2, :vid2)";
                    cmd.Parameters.Add(":co2", OracleDbType.Int32).Value = user.userid;
                    cmd.Parameters.Add(":vid2", OracleDbType.Int32).Value = video.videoId;
                    OracleDataReader reader = cmd.ExecuteReader();
                    cmd.ExecuteNonQuery();
                }
            }
            else
            {
                Debug.WriteLine("The user already like that video!");
            }
        }
        public List<VideoBean> listVideosOfPalylist(PlaylistBean playlist)
        {
            List<VideoBean> listOfVideos = new List<VideoBean>();
            List<PlaylistBean> playLists = new List<PlaylistBean>();
            List<int> videoIds = new List<int>();

            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand command = conn.CreateCommand())
            {
                conn.Open();
                command.CommandText = @"SELECT videoId FROM CONTAINSPLAYLISTVIDEOTABLE WHERE PlaylistId=:pId";
                command.Parameters.Add(":pId", playlist.playlistId);

                OracleDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    videoIds.Add(reader.GetInt32(reader.GetOrdinal("videoID")));
                }
            }

            foreach (int videoId in videoIds)
            {
                using (OracleConnection conn = new OracleConnection(connectionString))
                using (OracleCommand command = conn.CreateCommand())
                {
                    conn.Open();
                    command.CommandText = @"SELECT * FROM Videotable WHERE videoId=:vId";
                    command.Parameters.Add(":vId", videoIds.IndexOf(videoId));

                    OracleDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        VideoBean video = new VideoBean()
                        {
                            videoId = reader.GetInt32(reader.GetOrdinal("videoId")),
                            videoTitle = reader.GetString(reader.GetOrdinal("videoTitle")),
                            videoDescription = reader.GetString(reader.GetOrdinal("videoDescription")),
                            videoUploadDate = DateTime.ParseExact(reader.GetString(reader.GetOrdinal("videoUploadDate")), "yyyy. MM. dd. HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                            videoViewCount = reader.GetInt32(reader.GetOrdinal("videoViewCount")),
                            videoLikeCount = reader.GetInt32(reader.GetOrdinal("videoLikeCount")),
                            videoThumbnailImageURL = new Uri(reader.GetString(reader.GetOrdinal("videoThumbnailImageURL"))),
                            videoURL = new Uri(reader.GetString(reader.GetOrdinal("videoURL"))),
                            isPublic = reader.GetInt32(reader.GetOrdinal("videoIsPublic")),
                            areCommentsEnabled = reader.GetInt32(reader.GetOrdinal("videoAreCommentsEnabled")),
                            isChildrenSafe = reader.GetInt32(reader.GetOrdinal("videoIsChildrenSafe")),
                            userID = reader.GetInt32(reader.GetOrdinal("userId"))
                        };
                        listOfVideos.Add(video);
                    }
                }
            }
            return listOfVideos;
        }
        #endregion

        #region Nem triviális lekérdezések

        public VideoBean getSimilarVideoThatiLiked(UserBean user)
        {
            VideoBean video = new VideoBean();

            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();

                command.CommandText = @"SELECT Labeltable.LABELID , sum(VideoTable.VideoID) as total FROM (
                            ((FAVOURITEUSERVIDEOTABLE INNER JOIN VideoTable ON VideoTable.VideoId = FAVOURITEUSERVIDEOTABLE.VideoId) 
                              INNER JOIN CONTAINSLABELVIDEOTABLE On CONTAINSLABELVIDEOTABLE.VIDEOID = VideoTable.videoID) 
                              INNER JOIN Labeltable ON LABELTABLE.Labelid=ContainsLabelvideotable.labelid)
                              Where VideoTable.userid=:usID
                              Group by Labeltable.LABELID
                              ORDER BY total desc";

                command.Parameters.Add(":usID", OracleDbType.Varchar2).Value = user.userid;


                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int favouriteLabelID = reader.GetInt32(reader.GetOrdinal("labelid"));
                    Debug.WriteLine("L:" + favouriteLabelID);
                    video = getMostViewedVideoByLabel(favouriteLabelID);
                }
            }

            return video;
        }

        private VideoBean getMostViewedVideoByLabel(int favouriteLabelID)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();

                command.CommandText = @"SELECT MAX(VIDEOTABLE.VIDEOVIEWCOUNT)as total, videotable.videoid FROM (LABELTABLE inner join CONTAINSLABELVIDEOTABLE ON CONTAINSLABELVIDEOTABLE.LABELID = LABELTABLE.LABELID) 
                                INNER JOIN VIDEOTABLE ON VIDEOTABLE.VIDEOID=CONTAINSLABELVIDEOTABLE.VIDEOID
                                WHERE LABELTABLE.LABELID=:labID
                                group by VIDEOTABLE.videoid
                                ORDER BY total desc ";

                command.Parameters.Add(":labID", OracleDbType.Varchar2).Value = favouriteLabelID;

                Debug.WriteLine("F:" + favouriteLabelID);
                

                OracleDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Debug.WriteLine("ENNEK A VIDEONAK KEREM LE A TULAJDONSAGAIT" + reader.GetInt32(reader.GetOrdinal("videoID")));
                    return getVideoById(reader.GetInt32(reader.GetOrdinal("videoID")));
                }
            }
            return null;
        }

        public List<HomePageSingleCategoryForm> listMostViewedVideoByCategories()
        {
            List<HomePageSingleCategoryForm> forms = new List<HomePageSingleCategoryForm>();
            List<int> cIds = new List<int>();
            List<int> vIds = new List<int>();
            List<string> cNames = new List<string>();

            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT DISTINCT CATEGORYTABLE.CategoryId, CATEGORYTABLE.categoryName FROM ((CONTAINSCATEGORYVIDEOTABLE 
                                    INNER JOIN CATEGORYTABLE ON  CATEGORYTABLE.CategoryId = CONTAINSCATEGORYVIDEOTABLE.CategoryId) 
                                    INNER JOIN VIDEOTABLE On CONTAINSCATEGORYVIDEOTABLE.videoId = VIDEOTABLE.VideoId)";
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CategoryBean category = new CategoryBean()
                    {
                        categoryId = reader.GetInt32(reader.GetOrdinal("categoryId")),
                        categoryName = reader.GetString(reader.GetOrdinal("categoryName"))
                    };

                    string cName = reader.GetString(reader.GetOrdinal("categoryName"));
                    int cId = reader.GetInt32(reader.GetOrdinal("categoryId"));
                    Debug.WriteLine("C: " + cId);

                    using (OracleConnection conn2 = new OracleConnection(connectionString))
                    using (OracleCommand cmd2 = conn2.CreateCommand())
                    {
                        conn2.Open();
                        cmd2.CommandText = @"SELECT VIDEOTABLE.videoId FROM ((CONTAINSCATEGORYVIDEOTABLE 
                                            INNER JOIN VIDEOTABLE ON  VIDEOTABLE.VideoId = CONTAINSCATEGORYVIDEOTABLE.VideoId) 
                                            INNER JOIN CategoryTable On CONTAINSCATEGORYVIDEOTABLE.CategoryId = categoryTable.CategoryId ) 
                                            WHERE VideoTable.videoUploadDate = (SELECT MAX(videoUploadDate) FROM VideoTable WHERE categoryTable.CategoryId =:cId)";
                        cmd2.Parameters.Add(":cId", OracleDbType.Int32).Value = cId;

                        OracleDataReader reader2 = cmd2.ExecuteReader();

                        while (reader2.Read())
                        {
                            int videoID = reader2.GetInt32(reader2.GetOrdinal("videoId"));
                            Debug.WriteLine("V: " + videoID);

                            VideoBean video = getVideoById(videoID);
                            HomePageSingleCategoryForm form = new HomePageSingleCategoryForm(main, user, video, main);
                            Helper helper = new Helper();
                            form.category_name.Text = cName;

                            form.video_thumbnail.Image = helper.resizeImageWithPreservedAspectRatio(helper.getThumbnailFromFTP(video, "videoUploader", ""), 192, 108);
                            form.video_title.Text = video.videoTitle;
                            form.view_count.Text = video.videoViewCount.ToString();
                            form.like_count.Text = video.videoLikeCount.ToString();

                            forms.Add(form);
                        }
                    }
                }
            }
            return forms;
        }

        public List<CategoryLabel> listMostViewedCategories()
        {
            List<CategoryLabel> forms = new List<CategoryLabel>();

            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT Categorytable.categoryid, SUM(VideoTable.VideoViewCount) as total FROM ((CONTAINSCATEGORYVIDEOTABLE  
                                    INNER JOIN VideoTable ON VideoTable.VideoId = CONTAINSCATEGORYVIDEOTABLE.VideoId) 
                                    INNER JOIN CategoryTable On CONTAINSCATEGORYVIDEOTABLE.CategoryId = CategoryTable.CategoryId)
                                    Group by Categorytable.categoryid
                                    ORDER BY total DESC";

                OracleDataReader reader = cmd.ExecuteReader();
                int db = 0;
                while (reader.Read())
                {
                    if (db > 2)
                    {
                        return forms;
                    }

                    CategoryLabel catLabel = new CategoryLabel();
                    CategoryBean cmt = getCategoryById(reader.GetInt32(reader.GetOrdinal("CategoryID")));
                    catLabel.category_name.Text = cmt.categoryName;

                    forms.Add(catLabel);
                    db++;
                }
            }
            return forms;
        }

        public MostViewedVideosOfCategories listMostViewedVideosByCategories()
        {
            MostViewedVideosOfCategories ret = new MostViewedVideosOfCategories();

            using (OracleConnection connection = new OracleConnection(connectionString))
            using (OracleCommand command = connection.CreateCommand())
            {
                connection.Open();
                command.CommandText = @"SELECT Categorytable.categoryid, SUM(VideoTable.VideoViewCount) as total FROM ((CONTAINSCATEGORYVIDEOTABLE  
                                    INNER JOIN VideoTable ON VideoTable.VideoId = CONTAINSCATEGORYVIDEOTABLE.VideoId) 
                                    INNER JOIN CategoryTable On CONTAINSCATEGORYVIDEOTABLE.CategoryId = CategoryTable.CategoryId) WHERE rownum <= 3
                                    Group by Categorytable.categoryid
                                    ORDER BY total DESC";
                OracleDataReader reader = command.ExecuteReader();
                ret.cnt = 0;
                while (reader.Read())
                {
                    int categoryID = reader.GetInt32(reader.GetOrdinal("CategoryID"));
                    using (OracleConnection subconnection = new OracleConnection(connectionString))
                    using (OracleCommand subcommand = subconnection.CreateCommand())
                    {
                        subconnection.Open();

                        subcommand.CommandText = @"SELECT VideoTable.videoid, sum(VideoTable.VIDEOVIEWCOUNT) as total FROM((CONTAINSCATEGORYVIDEOTABLE 
                                                INNER JOIN VideoTable ON VideoTable.VideoId = CONTAINSCATEGORYVIDEOTABLE.VideoId) 
                                                INNER JOIN CategoryTable On CONTAINSCATEGORYVIDEOTABLE.CategoryId = CategoryTable.CategoryId) 
                                                Where categorytable.categoryid = :catID AND rownum <= 3 GROUP BY VideoTable.videoid Order BY total DESC";
                        subcommand.Parameters.Add(":catID", OracleDbType.Int32).Value = categoryID;

                        OracleDataReader subreader = subcommand.ExecuteReader();
                        while (subreader.Read())
                        {
                            int videoID = subreader.GetInt32(subreader.GetOrdinal("VideoID"));
                            VideoBean video = getVideoById(videoID);
                            ret.addVideoToList(video);
                        }
                    }
                    ret.cnt++;
                }
            }
            return ret;
        }

        public List<CategoryBean> listCategoriesOfVideo(VideoBean video)
        {
            List<CategoryBean> categories = new List<CategoryBean>();
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT categorytable.categoryId, categorytable.categoryName FROM CONTAINSCATEGORYVIDEOTABLE INNER JOIN CATEGORYTABLE ON CONTAINSCATEGORYVIDEOTABLE.categoryId = CATEGORYTABLE.CATEGORYID WHERE videoId = :vidID";
                cmd.Parameters.Add(":vidID", OracleDbType.Decimal).Value = video.videoId;
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CategoryBean category = new CategoryBean()
                    {
                        categoryId = reader.GetInt32(reader.GetOrdinal("categoryId")),
                        categoryName = reader.GetString(reader.GetOrdinal("categoryName"))
                    };
                    categories.Add(category);
                }
            }
            return categories;
        }

        public List<string> listLabelsOfVideo(VideoBean video)
        {
            List<string> labels = new List<string>();
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT labeltable.labelname FROM CONTAINSLABELVIDEOTABLE INNER JOIN LABELTABLE ON CONTAINSLABELVIDEOTABLE.labelId = LABELTABLE.labelid WHERE videoId = :vidID";
                cmd.Parameters.Add(":vidID", OracleDbType.Decimal).Value = video.videoId;
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    labels.Add(reader.GetString(reader.GetOrdinal("labelName")));
                }
            }
            return labels;
        }

        public void insertLabelAndAddItToVideo(string stringLabelName, VideoBean video)
        {
            Debug.WriteLine(stringLabelName);
            Database databaseAccess = new Database();
            var isAlreadyExists = false;
            var alreadyAddedLabels = listLabels();
            foreach (var actualElement in alreadyAddedLabels)
            {
                if (actualElement.labelName.Equals(stringLabelName))
                {
                    isAlreadyExists = true;
                }
            }
            if (!isAlreadyExists)
            {
                var nextLabelId = getNextUnusedIdOfLabelTable();
                using (OracleConnection conn = new OracleConnection(connectionString))
                using (OracleCommand command = conn.CreateCommand())
                {
                    conn.Open();
                    command.CommandText = @"INSERT INTO LabelTable (labelId, labelName) VALUES (:labID, :labName)";
                    command.Parameters.Add(":labID", OracleDbType.Decimal).Value = nextLabelId;
                    command.Parameters.Add(":labName", OracleDbType.Varchar2).Value = stringLabelName;
                    command.ExecuteNonQuery();
                }
                using (OracleConnection conn = new OracleConnection(connectionString))
                using (OracleCommand command = conn.CreateCommand())
                {
                    conn.Open();
                    command.CommandText = @"INSERT INTO CONTAINSLABELVIDEOTABLE (labelId, videoID) VALUES (:labID, :vidID)";
                    command.Parameters.Add(":catID", OracleDbType.Decimal).Value = nextLabelId;
                    command.Parameters.Add(":vidID", OracleDbType.Decimal).Value = video.videoId;
                    command.ExecuteNonQuery();
                }
            }
            else
            {
                int labelIdSearchRes = 0;
                using (OracleConnection conn = new OracleConnection(connectionString))
                using (OracleCommand command = conn.CreateCommand())
                {
                    conn.Open();
                    command.CommandText = @"SELECT labelID FROM LabelTable WHERE labelName = ':labName'";
                    command.Parameters.Add(":labName", OracleDbType.Decimal).Value = stringLabelName;
                    OracleDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        labelIdSearchRes = reader.GetInt32(reader.GetOrdinal("labelID"));
                    }
                }
                using (OracleConnection conn = new OracleConnection(connectionString))
                using (OracleCommand command = conn.CreateCommand())
                {
                    conn.Open();
                    command.CommandText = @"INSERT INTO CONTAINSLABELVIDEOTABLE (labelId, videoID) VALUES (:labID, :vidID)";
                    command.Parameters.Add(":catID", OracleDbType.Decimal).Value = labelIdSearchRes;
                    command.Parameters.Add(":vidID", OracleDbType.Decimal).Value = video.videoId;
                    command.ExecuteNonQuery();
                }
            }
        }

        public bool insertCategoryToVideo(CategoryBean category, VideoBean video)
        {
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand command = conn.CreateCommand())
            {
                conn.Open();
                command.CommandText = @"INSERT INTO CONTAINSCATEGORYVIDEOTABLE (categoryID, videoID) VALUES (:catID, :vidID)";
                command.Parameters.Add(":catID", OracleDbType.Decimal).Value = category.categoryId;
                command.Parameters.Add(":vidID", OracleDbType.Decimal).Value = video.videoId;

                if (command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
            }
            return false;
        }

        public List<HomePageSingleCategoryForm> listNewestVideosByCategories()
        {
            List<HomePageSingleCategoryForm> forms = new List<HomePageSingleCategoryForm>();
            List<int> cIds = new List<int>();
            List<int> vIds = new List<int>();
            List<string> cNames = new List<string>();

            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = @"SELECT DISTINCT CATEGORYTABLE.CategoryId, CATEGORYTABLE.categoryName FROM ((CONTAINSCATEGORYVIDEOTABLE 
                                    INNER JOIN CATEGORYTABLE ON  CATEGORYTABLE.CategoryId = CONTAINSCATEGORYVIDEOTABLE.CategoryId) 
                                    INNER JOIN VIDEOTABLE On CONTAINSCATEGORYVIDEOTABLE.videoId = VIDEOTABLE.VideoId)";
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    CategoryBean category = new CategoryBean()
                    {
                        categoryId = reader.GetInt32(reader.GetOrdinal("categoryId")),
                        categoryName = reader.GetString(reader.GetOrdinal("categoryName"))
                    };

                    string cName = reader.GetString(reader.GetOrdinal("categoryName"));
                    int cId = reader.GetInt32(reader.GetOrdinal("categoryId"));
                    Debug.WriteLine("C: " + cId);

                    using (OracleConnection conn2 = new OracleConnection(connectionString))
                    using (OracleCommand cmd2 = conn2.CreateCommand())
                    {
                        conn2.Open();
                        cmd2.CommandText = @"SELECT VIDEOTABLE.videoId, CONTAINSCATEGORYVIDEOTABLE.categoryid as catidproba FROM VIDEOTABLE 
                                        INNER JOIN CONTAINSCATEGORYVIDEOTABLE ON  VIDEOTABLE.VideoId = CONTAINSCATEGORYVIDEOTABLE.VideoId 
                                        WHERE VideoTable.videoUploadDate = (SELECT MAX(videoUploadDate) FROM VideoTable 
                                        INNER JOIN CONTAINSCATEGORYVIDEOTABLE ON  VIDEOTABLE.VideoId = CONTAINSCATEGORYVIDEOTABLE.VideoId 
                                        WHERE CONTAINSCATEGORYVIDEOTABLE.CategoryId =:cId) AND CONTAINSCATEGORYVIDEOTABLE.CategoryId =:cId";
                        cmd2.Parameters.Add(":cId", OracleDbType.Int32).Value = cId;

                        OracleDataReader reader2 = cmd2.ExecuteReader();

                        while (reader2.Read())
                        {
                            int videoID = reader2.GetInt32(reader2.GetOrdinal("videoId"));
                            Debug.WriteLine("V: " + videoID);

                            VideoBean video = getVideoById(videoID);
                            HomePageSingleCategoryForm form = new HomePageSingleCategoryForm(main, user, video, main);
                            Helper helper = new Helper();
                            form.category_name.Text = cName;

                            form.video_thumbnail.Image = helper.resizeImageWithPreservedAspectRatio(helper.getThumbnailFromFTP(video, "videoUploader", ""), 192, 108);
                            form.video_title.Text = video.videoTitle;
                            form.view_count.Text = video.videoViewCount.ToString();
                            form.like_count.Text = video.videoLikeCount.ToString();

                            forms.Add(form);
                        }
                    }
                }
                return forms;
            }

            #endregion
        }

        public bool addVideoToPlaylist(VideoBean video, PlaylistBean playlist)
        {
            using (OracleConnection conn = new OracleConnection(connectionString))
            using (OracleCommand command = conn.CreateCommand())
            {
                conn.Open();
                command.CommandText = @"INSERT INTO CONTAINSPLAYLISTVIDEOTABLE (playlistId, videoId) VALUES (:pId, :vId)";
                command.Parameters.Add(":pId", OracleDbType.Decimal).Value = playlist.playlistId;
                command.Parameters.Add(":vId", OracleDbType.Decimal).Value = video.videoId;

                if (command.ExecuteNonQuery() == 1)
                {
                    return true;
                }
            }
            return false;
        }
    }


}