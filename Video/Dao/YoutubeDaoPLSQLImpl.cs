﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Video.Model;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace Video.Dao
{
    class YoutubeDaoPLSQLImpl : YoutubeDao

    {
        #region konstansok
        private const string DATABASE_CONNECTION = "Data Source=127.0.0.1:1521 ;User Id=h662200;Password=h662200;";
        #endregion

        #region statikus tagok
        private readonly string s_connectionString;
        #endregion


        #region konstruktorok
        public YoutubeDaoPLSQLImpl(string databasePath) {
            s_connectionString = string.Format(DATABASE_CONNECTION_STRING_FORMAT_STRING,databasePath);
        }
        #endregion

        #region metodusimplementalasok
       

        public IEnumerable<VideoBean> selectMostViewedVideos()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<VideoBean> selectMostViewedVideosByCategory(string category)
        {
            throw new NotImplementedException();
        }

        public UserBean selectUserByName(string name)
        {
            throw new NotImplementedException();
        }

        public VideoBean selectVideoByName(string name)
        {
            throw new NotImplementedException();
        }

        public VideoBean selectVLatestVideos(string timeStamp = null)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region database connection

        public OracleConnection conn = new OracleConnection(DATABASE_CONNECTION);
        public void Connect()
        {
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;

            cmd.CommandType = "Select * from USERTABLE";
            cmd.CommandType = CommandType.Text;

            OracleDataReader dr = cmd.ExecuteReader();
            dr.Read();

        }
        public void Close()
        {
            conn.Close();
            conn.Dispose();
        }
        #endregion

    }
}
