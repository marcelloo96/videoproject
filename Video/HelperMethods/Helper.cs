﻿using NReco.VideoConverter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Video.Model;
using VideoPlayerProject;

namespace Video.HelperMethods
{
    public class Helper
    {
        public Helper()
        {

        }



        /// <summary>
        /// This method uploads one file from path to an ftp location as fileName.
        /// The upload progess is reported back to the backgroundWorker in percentage.
        /// </summary>
        /// <param name="filePath">The path of the uploadable file in string.</param>
        /// <param name="ftpPath">The path of the ftp location in string (i.e. "ftp://127.0.0.1").</param>
        /// <param name="fileName">The name of the uploaded file.</param>
        /// <param name="ftpUsername">The username of the ftp user.</param>
        /// <param name="ftpPassword">The password of the ftp user.</param>
        /// /// <returns>The url of the uploaded file.</returns>
        public string uploadFileToFTP(string filePath, string ftpPath, string fileName, string ftpUsername, string ftpPassword, BackgroundWorker backgroundWorker)
        {
            string ftpUrl = string.Format("{0}/{1}", ftpPath, fileName + Path.GetExtension(filePath));
            FtpWebRequest requestObj = FtpWebRequest.Create(ftpUrl) as FtpWebRequest;
            requestObj.Method = WebRequestMethods.Ftp.UploadFile;
            requestObj.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
            using (var inputStream = File.OpenRead(filePath))
            {
                using (var outputStream = requestObj.GetRequestStream())
                {
                    var buffer = new byte[1024 * 1024];
                    ulong totalReadBytesCount = 0;
                    ulong readBytesCount;
                    while ((readBytesCount = (ulong)inputStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        outputStream.Write(buffer, 0, (int)readBytesCount);
                        totalReadBytesCount += readBytesCount;
                        var progress = totalReadBytesCount * 100.0 / inputStream.Length;
                        backgroundWorker.ReportProgress((int)progress);
                    }
                }
            }
            return (ftpPath + "/" + fileName + Path.GetExtension(filePath));
        }

        /// <summary>
        /// This method uploads one file from stream to an ftp location as fileName.
        /// The upload progess is reported back to the backgroundWorker in percentage.
        /// </summary>
        /// <param name="stream">The stream of the uploadable file.</param>
        /// <param name="ftpPath">The path of the ftp location in string (i.e. "ftp://127.0.0.1").</param>
        /// <param name="fileName">The name of the uploaded file.</param>
        /// <param name="ftpUsername">The username of the ftp user.</param>
        /// <param name="ftpPassword">The password of the ftp user.</param>
        /// <returns>The url of the uploaded file.</returns>
        public string uploadStreamToFTP(Stream stream, string ftpPath, string fileName, string ftpUsername, string ftpPassword, BackgroundWorker backgroundWorker)
        {
            string ftpUrl = string.Format("{0}/{1}", ftpPath, fileName + ".png");
            FtpWebRequest requestObj = FtpWebRequest.Create(ftpUrl) as FtpWebRequest;
            requestObj.Method = WebRequestMethods.Ftp.UploadFile;
            requestObj.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
            using (var inputStream = stream)
            {
                using (var outputStream = requestObj.GetRequestStream())
                {
                    var buffer = new byte[1024 * 1024];
                    ulong totalReadBytesCount = 0;
                    ulong readBytesCount;
                    while ((readBytesCount = (ulong)inputStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        outputStream.Write(buffer, 0, (int)readBytesCount);
                        totalReadBytesCount += readBytesCount;
                        var progress = totalReadBytesCount * 100.0 / inputStream.Length;
                        backgroundWorker.ReportProgress((int)progress);
                    }
                }
            }
            stream.Close();
            return (ftpPath + "/" + fileName + ".png");
        }

        /// <summary>
        /// This method return a thumbnail image from the FTP server by the video's thumbnail URL property.
        /// </summary>
        /// <param name="video">The video which contains the thumbnail URL.</param>
        /// <param name="ftpUsername">The username of the ftp user.</param>
        /// <param name="ftpPassword">The password of the ftp user.</param>
        /// <returns>The thumbnail image.</returns>
        public Image getThumbnailFromFTP(VideoBean video, string ftpUsername, string ftpPassword)
        {
            if (video == null)
            {
                Debug.WriteLine("A VIDEO NULL");
            }
            if (video.videoThumbnailImageURL == null)
            {
                Debug.WriteLine("A VIDEO THUMBNAIL URL NULL");
            }
            Debug.WriteLine("Downloading thumbnail for video:" + video.videoId.ToString() + " " + video.videoTitle + " " + video.videoURL.ToString() + " " + video.videoThumbnailImageURL.ToString());
            
            WebClient wc = new WebClient();
            wc.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
            byte[] bytes = wc.DownloadData(video.videoThumbnailImageURL.ToString());
            MemoryStream ms = new MemoryStream(bytes);
            return Image.FromStream(ms);
        }

        /// <summary>
        /// This method returns a thumbnail image from a video in a specific time. The thumbnail image size is 
        /// fitted into a canvas specified by width and height while the original aspect ratio is preserved.
        /// </summary>
        /// <param name="videoPath">The path of the video.</param>
        /// <param name="second">The position of the video in seconds when the timestamp is created.</param>
        /// <returns>The thumbnail image.</returns>
        public Image getThumbnailFromVideo(string videoPath, int second, int width, int height)
        {
            using (var stream = new MemoryStream())
            {
                FFMpegConverter ffMpeg = new FFMpegConverter();
                ffMpeg.GetVideoThumbnail(videoPath, stream, second);
                Image thumbnail = Image.FromStream(stream);
                return resizeImageWithPreservedAspectRatio(thumbnail, width, height);
            }
        }

        /// <summary>
        /// This method returns an image which size is fitted into a canvas specified by width and height while the 
        /// original aspect ratio is preserved.
        /// </summary>
        /// <param name="originalImage">The resizeable image.</param>
        /// <param name="canvasWidth">The width of the canvas which the image will be fitted into.</param>
        /// <param name="canvasHeight">The height of the canvas which the image will be fitted into.</param>
        /// <returns>The resized image.</returns>
        public Image resizeImageWithPreservedAspectRatio(Image originalImage, int canvasWidth, int canvasHeight)
        {
            int originalWidth = originalImage.Width;
            int originalHeight = originalImage.Height;
            Image image = originalImage;
            Image thumbnail = new Bitmap(canvasWidth, canvasHeight);
            Graphics graphic = Graphics.FromImage(thumbnail);
            graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphic.SmoothingMode = SmoothingMode.HighQuality;
            graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
            graphic.CompositingQuality = CompositingQuality.HighQuality;
            double ratioX = (double)canvasWidth / (double)originalWidth;
            double ratioY = (double)canvasHeight / (double)originalHeight;
            double ratio = ratioX < ratioY ? ratioX : ratioY;
            int newHeight = Convert.ToInt32(originalHeight * ratio);
            int newWidth = Convert.ToInt32(originalWidth * ratio);
            int posX = Convert.ToInt32((canvasWidth - (originalWidth * ratio)) / 2);
            int posY = Convert.ToInt32((canvasHeight - (originalHeight * ratio)) / 2);
            graphic.Clear(Color.Black);
            graphic.DrawImage(image, posX, posY, newWidth, newHeight);
            ImageCodecInfo[] info = ImageCodecInfo.GetImageEncoders();
            EncoderParameters encoderParameters;
            encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            return thumbnail;
        }

        /// <summary>
        /// This method returns a stream which is converted from the specified image.
        /// </summary>
        /// <param name="image">The image to convert.</param>
        /// <returns>The stream of the image.</returns>
        public Stream imageToStream(Image image)
        {
            Stream stream = new MemoryStream();
            image.Save(stream, ImageFormat.Png);
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// This method will open a video player in the target panel with the desired video as the desired user
        /// while closing the form which contains the link to the video.
        /// </summary>
        /// <param name="actualForm">The form to close, which contains the link to the video.</param>
        /// <param name="targetPanel">The target panel where we want to open the video player.</param>
        /// <param name="video">The video we want to play in the video player.</param>
        /// <param name="user">The user who open the video.</param>
        public void openVideo(Form actualForm, Panel targetPanel, VideoBean video, UserBean user)
        {
            targetPanel.Controls.Clear();
            VideoPlayer videoPlayer = new VideoPlayer(video, user);
            videoPlayer.FormBorderStyle = FormBorderStyle.None;
            videoPlayer.TopLevel = false;
            videoPlayer.AutoScroll = true;
            targetPanel.Controls.Add(videoPlayer);
            videoPlayer.Show();
            actualForm.Close();
        }
    }
}
