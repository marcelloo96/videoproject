﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Video.Model
{
    public class CommentBean
    {
    /*Primary key*/ public int commentId { get; set; }
    public string commentText{ get; set; }
    public DateTime commentWriteDate{ get; set; } 
    /*Foreign key*/ public int userId{ get; set; } 
    /*Foreign key*/ public int videoId{ get; set; }
                  
    }
}
