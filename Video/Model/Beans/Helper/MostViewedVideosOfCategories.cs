﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Video.Model.Beans.Helper
{
    public class MostViewedVideosOfCategories
    {
        public int cnt = 0;


        public List<VideoBean> cat1 = new List<VideoBean>();
        public List<VideoBean> cat2 = new List<VideoBean>();
        public List<VideoBean> cat3 = new List<VideoBean>();

        public void addVideoToList(VideoBean video) {
            if (cnt == 0)
            {
                cat1.Add(video);
            }
            else if (cnt == 1)
            {
                cat2.Add(video);
            }
            else if (cnt == 2)
            {
                cat3.Add(video);
            }
        }
    }
}
