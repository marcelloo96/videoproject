﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Video.Model
{
    public class VideoBean
    {
        /*Primary key*/
        public int videoId { get; set; }
        public string videoTitle { get; set; }
        public  string videoDescription { get; set; }
        public DateTime videoUploadDate { get; set; }
        public int videoViewCount { get; set; }
        public int videoLikeCount { get; set; }
        public int isPublic { get; set; }
        public int areCommentsEnabled { get; set; }
        public int isChildrenSafe { get; set; }
        public Uri videoThumbnailImageURL { get; set; }
        public Uri videoURL { get; set; }
        public string videoLabelList { get; set; }

        /*Külső kulcs*/
        public  int userID { get; set; }
    }
}
