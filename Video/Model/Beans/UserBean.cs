﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Video.Model
{
    public class UserBean
    {
        public UserBean() { }
        public UserBean(string username, string password, string email, string birthdate){
            this.username = username;
            this.password = password;
            this.email = email;
            this.birthdate = birthdate;
        }
        /*Primary key*/
        public int userid { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string birthdate { get; set; }
        public int isAdmin { get; set; }

        public int getUserid() {
            return userid;
        }
        public string getUsername() {
            return username;
        }
        public string getPassword()
        {
            return password;
        }
        public string getEmail()
        {
            return email;
        }
        public string getBirthdate()
        {
            return birthdate;
        }


    }
}
