﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Video.Model
{
    public class LabelBean
    {
        public int labelId { get; set; }
        public string labelName { get; set; }
    }
}
