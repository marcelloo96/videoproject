﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Video.Model
{
    public class PlaylistBean
    {
        public PlaylistBean() { }

        public PlaylistBean(int playlistId, string playlistName, int userId){
            this.playlistId = playlistId;
            this.playlistName = playlistName;
            this.userId = userId;

        }
    /*Primary key*/ public int playlistId{ get; set; }
                    public string playlistName{ get; set; }
    /*Foreign key*/ public int userId{ get; set; }

        public int getplaylistId()
        {
            return playlistId;
        }
        public string getplaylistName()
        {
            return playlistName;
        }
        public int getuserIdd()
        {
            return userId;
        }
    }
}
