﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Video.Model
{
    public class CategoryBean
    {
        /*Primary key*/public int categoryId { get; set; }
        public string categoryName { get; set; }
    }
}
